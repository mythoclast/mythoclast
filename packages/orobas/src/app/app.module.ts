import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BungieNetModule } from '@mythoclast/ng-bungie-net';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AuthCodeComponent } from './auth-code/auth-code.component';
import { RequestAuthorizationComponent } from './request-authorization/request-authorization.component';
import { NoopComponent } from './noop/noop.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthCodeComponent,
    RequestAuthorizationComponent,
    NoopComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BungieNetModule.forRoot({
      apiKey: 'e8abd6fc6e6949c29cbc1ae4e53a7e9d',
      oauthClientId: '24712',
      oauthClientSecret: 'zFFjs0E8L1Jn1om-Lrv6bh.6qN3A0q1cq2VBKdQEFU0'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
