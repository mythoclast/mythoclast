import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UserService, Destiny2Service } from '@mythoclast/ng-bungie-net';
import { map, switchMap } from 'rxjs/operators';
import { BungieMembershipType, UserInfoCard, DestinyComponentType } from '@mythoclast/ng-bungie-net';
import { DestinyManifestService } from '@mythoclast/ng-bungie-net';

@Component({
  selector: 'app-auth-code',
  templateUrl: './auth-code.component.html',
  styleUrls: ['./auth-code.component.scss']
})
export class AuthCodeComponent implements OnInit {

  constructor(private userService: UserService, private destiny2Service: Destiny2Service, private manifest: DestinyManifestService) { }
  displayName$?: Observable<string>;

  ngOnInit() {
    this.userService.getMembershipDataForCurrentUser().pipe(
      map(user => {
        return user.Response.destinyMemberships.reduce((found: UserInfoCard, current: UserInfoCard) => {
          if (found) {
            return found;
          }
          if (current.membershipType === BungieMembershipType.TigerBlizzard) {
            return current;
          }
          return null;
        }, null);
      }),
      switchMap((userInfo: UserInfoCard) => this.destiny2Service.getProfile(
        userInfo.membershipType,
        userInfo.membershipId,
        [
          DestinyComponentType.Profiles,
          DestinyComponentType.ProfileInventories,
          DestinyComponentType.Characters,
          DestinyComponentType.CharacterInventories,
          DestinyComponentType.CharacterEquipment
        ]
      ))
    ).subscribe(response => console.log(response));
    //this.destiny2Service.getDestinyManifest().subscribe(response => console.log(response));
    //this.displayName$ = this.userService.getMembershipDataForCurrentUser().pipe(map(it => it.Response.bungieNetUser.blizzardDisplayName));
  }

}
