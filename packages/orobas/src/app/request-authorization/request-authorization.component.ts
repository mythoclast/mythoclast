import { Component, OnInit } from '@angular/core';
import { BungieNetAuthenticatorService } from '@mythoclast/ng-bungie-net';
import { take, map, switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-request-authorization',
  templateUrl: './request-authorization.component.html',
  styleUrls: ['./request-authorization.component.scss']
})
export class RequestAuthorizationComponent {

  constructor(private authorizationService: BungieNetAuthenticatorService) { }

  requestAuthorization() {
    const WIDTH = 800;
    const HEIGHT = 600;
    const TOP = window.top.outerHeight / 2 + window.top.screenY - ( HEIGHT / 2);
    const LEFT = window.top.outerWidth / 2 + window.top.screenX - ( WIDTH / 2);
    const win = window.open(
      '',
      'bnet-auth',
      `centerscreen,width=${WIDTH},height=${HEIGHT},top=${TOP},left=${LEFT},resizable,scrollbars,status`
    );
    this.authorizationService.authorizeAndGetToken(win, true)
    .subscribe((token) => console.log(token));
  }

  get authenticated(): Observable<boolean> {
    return this.authorizationService.getActiveToken().pipe(take(1), map((it: any): boolean => !!it));
  }

  get buttonText(): string {
    return 'Begin Bungie.net Authentication';
  }

}
