import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OAuthAuthorizationCodeGuard } from './guards/oauth-authorization-code.guard';
import { AuthCodeComponent } from './auth-code/auth-code.component';
import { RequestAuthorizationComponent } from './request-authorization/request-authorization.component';
import { NoopComponent } from './noop/noop.component';

const routes: Routes = [
  {
    path: 'acknowledge-oauth',
    canActivate: [OAuthAuthorizationCodeGuard],
    component: NoopComponent
  },
  {
    path: 'auth-info',
    component: AuthCodeComponent
  },
  {
    path: 'begin-auth-flow',
    component: RequestAuthorizationComponent
  },
  {
    path: '**',
    redirectTo: '/auth-info'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
