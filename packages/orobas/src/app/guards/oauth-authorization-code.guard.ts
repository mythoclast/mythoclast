import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable, of, forkJoin } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { BungieNetAuthenticatorService } from '@mythoclast/ng-bungie-net';
import { NgxBroadcastChannelService } from '@mythoclast/ngx-broadcast-channel';

@Injectable({
  providedIn: 'root'
})
export class OAuthAuthorizationCodeGuard implements CanActivate {
  constructor(
    private authenticator: BungieNetAuthenticatorService,
    private router: Router,
    private channel: NgxBroadcastChannelService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<UrlTree> {
    if (route.queryParams.code && route.queryParams.state) {
      return forkJoin({
        sent: this.channel.sendTo('BNET-AUTH', { authorizationCode: route.queryParams.code, state: route.queryParams.state }),
        received: this.channel.getObservableChannel<boolean>('NGX-AUTH-ACK')
      }).pipe(
        switchMap(() => this.authenticator.getActiveToken()),
        map(token => {
          if (null !== token) {
            return this.router.parseUrl('/auth-info');
          }
          return this.router.parseUrl('/');
        })
      );
    }
    return of(this.router.parseUrl('/'));
  }

}
