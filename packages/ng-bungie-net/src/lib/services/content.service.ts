/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { BungieNetConfiguration as __Configuration } from '../bungie-net-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { ContentTypeDescription } from '../models/content/models/content-type-description';
import { PlatformErrorCodes } from '../models/exceptions/platform-error-codes';
import { ContentItemPublicContract } from '../models/content/content-item-public-contract';
import { SearchResultOfContentItemPublicContract } from '../models/search-result-of-content-item-public-contract';

/**
 * content
 */
@Injectable({
  providedIn: 'root',
})
class ContentService extends __BaseService {
  static readonly getContentTypePath = '/Content/GetContentType/{type}/';
  static readonly getContentByIdPath = '/Content/GetContentById/{id}/{locale}/';
  static readonly getContentByTagAndTypePath = '/Content/GetContentByTagAndType/{tag}/{type}/{locale}/';
  static readonly searchContentWithTextPath = '/Content/Search/{locale}/';
  static readonly searchContentByTagAndTypePath = '/Content/SearchContentByTagAndType/{tag}/{type}/{locale}/';
  static readonly searchHelpArticlesPath = '/Content/SearchHelpArticles/{searchtext}/{size}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Gets an object describing a particular variant of content.
   * @param type
   * @return Look at the Response property for more information about the nature of this response
   */
  private getContentTypeResponse(type: string): __Observable<__StrictHttpResponse<{Response?: ContentTypeDescription, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Content/GetContentType/${type}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: ContentTypeDescription, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets an object describing a particular variant of content.
   * @param type
   * @return Look at the Response property for more information about the nature of this response
   */
  getContentType(type: string): __Observable<{Response?: ContentTypeDescription, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getContentTypeResponse(type).pipe(
      __map(_r => _r.body as {Response?: ContentTypeDescription, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Returns a content item referenced by id
   * @param locale
   * @param id
   * @param head false
   * @return Look at the Response property for more information about the nature of this response
   */
  private getContentByIdResponse(locale: string,
    id: number,
    head?: boolean): __Observable<__StrictHttpResponse<{Response?: ContentItemPublicContract, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    if (head != null) __params = __params.set('head', head.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Content/GetContentById/${id}/${locale}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: ContentItemPublicContract, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Returns a content item referenced by id
   * @param locale
   * @param id
   * @param head false
   * @return Look at the Response property for more information about the nature of this response
   */
  getContentById(locale: string,
    id: number,
    head?: boolean): __Observable<{Response?: ContentItemPublicContract, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getContentByIdResponse(locale, id, head).pipe(
      __map(_r => _r.body as {Response?: ContentItemPublicContract, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Returns the newest item that matches a given tag and Content Type.
   * @param type
   * @param tag
   * @param locale
   * @param head Not used.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getContentByTagAndTypeResponse(type: string,
    tag: string,
    locale: string,
    head?: boolean): __Observable<__StrictHttpResponse<{Response?: ContentItemPublicContract, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;



    if (head != null) __params = __params.set('head', head.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Content/GetContentByTagAndType/${tag}/${type}/${locale}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: ContentItemPublicContract, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Returns the newest item that matches a given tag and Content Type.
   * @param type
   * @param tag
   * @param locale
   * @param head Not used.
   * @return Look at the Response property for more information about the nature of this response
   */
  getContentByTagAndType(type: string,
    tag: string,
    locale: string,
    head?: boolean): __Observable<{Response?: ContentItemPublicContract, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getContentByTagAndTypeResponse(type, tag, locale, head).pipe(
      __map(_r => _r.body as {Response?: ContentItemPublicContract, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Gets content based on querystring information passed in. Provides basic search and text search capabilities.
   * @param locale
   * @param tag Tag used on the content to be searched.
   * @param source For analytics, hint at the part of the app that triggered the search. Optional.
   * @param searchtext Word or phrase for the search.
   * @param head Not used.
   * @param currentpage Page number for the search results, starting with page 1.
   * @param ctype Content type tag: Help, News, etc. Supply multiple ctypes separated by space.
   * @return Look at the Response property for more information about the nature of this response
   */
  private searchContentWithTextResponse(locale: string,
    tag?: string,
    source?: string,
    searchtext?: string,
    head?: boolean,
    currentpage?: number,
    ctype?: string): __Observable<__StrictHttpResponse<{Response?: SearchResultOfContentItemPublicContract, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (tag != null) __params = __params.set('tag', tag.toString());
    if (source != null) __params = __params.set('source', source.toString());
    if (searchtext != null) __params = __params.set('searchtext', searchtext.toString());
    if (head != null) __params = __params.set('head', head.toString());
    if (currentpage != null) __params = __params.set('currentpage', currentpage.toString());
    if (ctype != null) __params = __params.set('ctype', ctype.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Content/Search/${locale}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: SearchResultOfContentItemPublicContract, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets content based on querystring information passed in. Provides basic search and text search capabilities.
   * @param locale
   * @param tag Tag used on the content to be searched.
   * @param source For analytics, hint at the part of the app that triggered the search. Optional.
   * @param searchtext Word or phrase for the search.
   * @param head Not used.
   * @param currentpage Page number for the search results, starting with page 1.
   * @param ctype Content type tag: Help, News, etc. Supply multiple ctypes separated by space.
   * @return Look at the Response property for more information about the nature of this response
   */
  searchContentWithText(locale: string,
    tag?: string,
    source?: string,
    searchtext?: string,
    head?: boolean,
    currentpage?: number,
    ctype?: string): __Observable<{Response?: SearchResultOfContentItemPublicContract, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.searchContentWithTextResponse(locale, tag, source, searchtext, head, currentpage, ctype).pipe(
      __map(_r => _r.body as {Response?: SearchResultOfContentItemPublicContract, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Searches for Content Items that match the given Tag and Content Type.
   * @param type
   * @param tag
   * @param locale
   * @param itemsperpage Not used.
   * @param head Not used.
   * @param currentpage Page number for the search results starting with page 1.
   * @return Look at the Response property for more information about the nature of this response
   */
  private searchContentByTagAndTypeResponse(type: string,
    tag: string,
    locale: string,
    itemsperpage?: number,
    head?: boolean,
    currentpage?: number): __Observable<__StrictHttpResponse<{Response?: SearchResultOfContentItemPublicContract, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;



    if (itemsperpage != null) __params = __params.set('itemsperpage', itemsperpage.toString());
    if (head != null) __params = __params.set('head', head.toString());
    if (currentpage != null) __params = __params.set('currentpage', currentpage.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Content/SearchContentByTagAndType/${tag}/${type}/${locale}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: SearchResultOfContentItemPublicContract, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Searches for Content Items that match the given Tag and Content Type.
   * @param type
   * @param tag
   * @param locale
   * @param itemsperpage Not used.
   * @param head Not used.
   * @param currentpage Page number for the search results starting with page 1.
   * @return Look at the Response property for more information about the nature of this response
   */
  searchContentByTagAndType(type: string,
    tag: string,
    locale: string,
    itemsperpage?: number,
    head?: boolean,
    currentpage?: number): __Observable<{Response?: SearchResultOfContentItemPublicContract, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.searchContentByTagAndTypeResponse(type, tag, locale, itemsperpage, head, currentpage).pipe(
      __map(_r => _r.body as {Response?: SearchResultOfContentItemPublicContract, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Search for Help Articles.
   * @param size
   * @param searchtext
   * @return Look at the Response property for more information about the nature of this response
   */
  private searchHelpArticlesResponse(size: string,
    searchtext: string): __Observable<__StrictHttpResponse<{Response?: {}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Content/SearchHelpArticles/${searchtext}/${size}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: {}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Search for Help Articles.
   * @param size
   * @param searchtext
   * @return Look at the Response property for more information about the nature of this response
   */
  searchHelpArticles(size: string,
    searchtext: string): __Observable<{Response?: {}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.searchHelpArticlesResponse(size, searchtext).pipe(
      __map(_r => _r.body as {Response?: {}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }
}

module ContentService {
}

export { ContentService }
