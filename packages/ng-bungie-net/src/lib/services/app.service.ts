/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { BungieNetConfiguration as __Configuration } from '../bungie-net-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { ApiUsage } from '../models/applications/api-usage';
import { PlatformErrorCodes } from '../models/exceptions/platform-error-codes';
import { Application } from '../models/applications/application';

/**
 * application
 */
@Injectable({
  providedIn: 'root',
})
class AppService extends __BaseService {
  static readonly getApplicationApiUsagePath = '/App/ApiUsage/{applicationId}/';
  static readonly getBungieApplicationsPath = '/App/FirstParty/';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get API usage by application for time frame specified. You can go as far back as 30 days ago, and can ask for up to a 48 hour window of time in a single request. You must be authenticated with at least the ReadUserData permission to access this endpoint.
   * @param applicationId ID of the application to get usage statistics.
   * @param start Start time for query. Goes to 24 hours ago if not specified.
   * @param end End time for query. Goes to now if not specified.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getApplicationApiUsageResponse(applicationId: number,
    start?: string,
    end?: string): __Observable<__StrictHttpResponse<{Response?: ApiUsage, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (start != null) __params = __params.set('start', start.toString());
    if (end != null) __params = __params.set('end', end.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/App/ApiUsage/${applicationId}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: ApiUsage, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Get API usage by application for time frame specified. You can go as far back as 30 days ago, and can ask for up to a 48 hour window of time in a single request. You must be authenticated with at least the ReadUserData permission to access this endpoint.
   * @param applicationId ID of the application to get usage statistics.
   * @param start Start time for query. Goes to 24 hours ago if not specified.
   * @param end End time for query. Goes to now if not specified.
   * @return Look at the Response property for more information about the nature of this response
   */
  getApplicationApiUsage(applicationId: number,
    start?: string,
    end?: string): __Observable<{Response?: ApiUsage, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getApplicationApiUsageResponse(applicationId, start, end).pipe(
      __map(_r => _r.body as {Response?: ApiUsage, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Get list of applications created by Bungie.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getBungieApplicationsResponse(): __Observable<__StrictHttpResponse<{Response?: Array<Application>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/App/FirstParty/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: Array<Application>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Get list of applications created by Bungie.
   * @return Look at the Response property for more information about the nature of this response
   */
  getBungieApplications(): __Observable<{Response?: Array<Application>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getBungieApplicationsResponse().pipe(
      __map(_r => _r.body as {Response?: Array<Application>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }
}

module AppService {
}

export { AppService }
