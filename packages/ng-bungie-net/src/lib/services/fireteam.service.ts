/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { BungieNetConfiguration as __Configuration } from '../bungie-net-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { PlatformErrorCodes } from '../models/exceptions/platform-error-codes';
import { SearchResultOfFireteamSummary } from '../models/search-result-of-fireteam-summary';
import { SearchResultOfFireteamResponse } from '../models/search-result-of-fireteam-response';
import { FireteamResponse } from '../models/fireteam/fireteam-response';

/**
 * fireteam
 */
@Injectable({
  providedIn: 'root',
})
class FireteamService extends __BaseService {
  static readonly getActivePrivateClanFireteamCountPath = '/Fireteam/Clan/{groupId}/ActiveCount/';
  static readonly getAvailableClanFireteamsPath = '/Fireteam/Clan/{groupId}/Available/{platform}/{activityType}/{dateRange}/{slotFilter}/{publicOnly}/{page}/';
  static readonly searchPublicAvailableClanFireteamsPath = '/Fireteam/Search/Available/{platform}/{activityType}/{dateRange}/{slotFilter}/{page}/';
  static readonly getMyClanFireteamsPath = '/Fireteam/Clan/{groupId}/My/{platform}/{includeClosed}/{page}/';
  static readonly getClanFireteamPath = '/Fireteam/Clan/{groupId}/Summary/{fireteamId}/';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Gets a count of all active non-public fireteams for the specified clan. Maximum value returned is 25.
   * @param groupId The group id of the clan.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getActivePrivateClanFireteamCountResponse(groupId: number): __Observable<__StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Fireteam/Clan/${groupId}/ActiveCount/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets a count of all active non-public fireteams for the specified clan. Maximum value returned is 25.
   * @param groupId The group id of the clan.
   * @return Look at the Response property for more information about the nature of this response
   */
  getActivePrivateClanFireteamCount(groupId: number): __Observable<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getActivePrivateClanFireteamCountResponse(groupId).pipe(
      __map(_r => _r.body as {Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Gets a listing of all of this clan's fireteams that are have available slots. Caller is not checked for join criteria so caching is maximized.
   * @param slotFilter Filters based on available slots
   * @param publicOnly Determines public/private filtering.
   * @param platform The platform filter.
   * @param page Zero based page
   * @param groupId The group id of the clan.
   * @param dateRange The date range to grab available fireteams.
   * @param activityType The activity type to filter by.
   * @param langFilter An optional language filter.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getAvailableClanFireteamsResponse(slotFilter: number,
    publicOnly: number,
    platform: number,
    page: number,
    groupId: number,
    dateRange: number,
    activityType: number,
    langFilter?: string): __Observable<__StrictHttpResponse<{Response?: SearchResultOfFireteamSummary, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;







    if (langFilter != null) __params = __params.set('langFilter', langFilter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Fireteam/Clan/${groupId}/Available/${platform}/${activityType}/${dateRange}/${slotFilter}/${publicOnly}/${page}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: SearchResultOfFireteamSummary, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets a listing of all of this clan's fireteams that are have available slots. Caller is not checked for join criteria so caching is maximized.
   * @param slotFilter Filters based on available slots
   * @param publicOnly Determines public/private filtering.
   * @param platform The platform filter.
   * @param page Zero based page
   * @param groupId The group id of the clan.
   * @param dateRange The date range to grab available fireteams.
   * @param activityType The activity type to filter by.
   * @param langFilter An optional language filter.
   * @return Look at the Response property for more information about the nature of this response
   */
  getAvailableClanFireteams(slotFilter: number,
    publicOnly: number,
    platform: number,
    page: number,
    groupId: number,
    dateRange: number,
    activityType: number,
    langFilter?: string): __Observable<{Response?: SearchResultOfFireteamSummary, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getAvailableClanFireteamsResponse(slotFilter, publicOnly, platform, page, groupId, dateRange, activityType, langFilter).pipe(
      __map(_r => _r.body as {Response?: SearchResultOfFireteamSummary, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Gets a listing of all public fireteams starting now with open slots. Caller is not checked for join criteria so caching is maximized.
   * @param slotFilter Filters based on available slots
   * @param platform The platform filter.
   * @param page Zero based page
   * @param dateRange The date range to grab available fireteams.
   * @param activityType The activity type to filter by.
   * @param langFilter An optional language filter.
   * @return Look at the Response property for more information about the nature of this response
   */
  private searchPublicAvailableClanFireteamsResponse(slotFilter: number,
    platform: number,
    page: number,
    dateRange: number,
    activityType: number,
    langFilter?: string): __Observable<__StrictHttpResponse<{Response?: SearchResultOfFireteamSummary, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;





    if (langFilter != null) __params = __params.set('langFilter', langFilter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Fireteam/Search/Available/${platform}/${activityType}/${dateRange}/${slotFilter}/${page}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: SearchResultOfFireteamSummary, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets a listing of all public fireteams starting now with open slots. Caller is not checked for join criteria so caching is maximized.
   * @param slotFilter Filters based on available slots
   * @param platform The platform filter.
   * @param page Zero based page
   * @param dateRange The date range to grab available fireteams.
   * @param activityType The activity type to filter by.
   * @param langFilter An optional language filter.
   * @return Look at the Response property for more information about the nature of this response
   */
  searchPublicAvailableClanFireteams(slotFilter: number,
    platform: number,
    page: number,
    dateRange: number,
    activityType: number,
    langFilter?: string): __Observable<{Response?: SearchResultOfFireteamSummary, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.searchPublicAvailableClanFireteamsResponse(slotFilter, platform, page, dateRange, activityType, langFilter).pipe(
      __map(_r => _r.body as {Response?: SearchResultOfFireteamSummary, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Gets a listing of all clan fireteams that caller is an applicant, a member, or an alternate of.
   * @param platform The platform filter.
   * @param page Deprecated parameter, ignored.
   * @param includeClosed If true, return fireteams that have been closed.
   * @param groupId The group id of the clan. (This parameter is ignored unless the optional query parameter groupFilter is true).
   * @param langFilter An optional language filter.
   * @param groupFilter If true, filter by clan. Otherwise, ignore the clan and show all of the user's fireteams.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getMyClanFireteamsResponse(platform: number,
    page: number,
    includeClosed: boolean,
    groupId: number,
    langFilter?: string,
    groupFilter?: boolean): __Observable<__StrictHttpResponse<{Response?: SearchResultOfFireteamResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;




    if (langFilter != null) __params = __params.set('langFilter', langFilter.toString());
    if (groupFilter != null) __params = __params.set('groupFilter', groupFilter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Fireteam/Clan/${groupId}/My/${platform}/${includeClosed}/${page}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: SearchResultOfFireteamResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets a listing of all clan fireteams that caller is an applicant, a member, or an alternate of.
   * @param platform The platform filter.
   * @param page Deprecated parameter, ignored.
   * @param includeClosed If true, return fireteams that have been closed.
   * @param groupId The group id of the clan. (This parameter is ignored unless the optional query parameter groupFilter is true).
   * @param langFilter An optional language filter.
   * @param groupFilter If true, filter by clan. Otherwise, ignore the clan and show all of the user's fireteams.
   * @return Look at the Response property for more information about the nature of this response
   */
  getMyClanFireteams(platform: number,
    page: number,
    includeClosed: boolean,
    groupId: number,
    langFilter?: string,
    groupFilter?: boolean): __Observable<{Response?: SearchResultOfFireteamResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getMyClanFireteamsResponse(platform, page, includeClosed, groupId, langFilter, groupFilter).pipe(
      __map(_r => _r.body as {Response?: SearchResultOfFireteamResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Gets a specific clan fireteam.
   * @param groupId The group id of the clan.
   * @param fireteamId The unique id of the fireteam.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getClanFireteamResponse(groupId: number,
    fireteamId: number): __Observable<__StrictHttpResponse<{Response?: FireteamResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Fireteam/Clan/${groupId}/Summary/${fireteamId}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: FireteamResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets a specific clan fireteam.
   * @param groupId The group id of the clan.
   * @param fireteamId The unique id of the fireteam.
   * @return Look at the Response property for more information about the nature of this response
   */
  getClanFireteam(groupId: number,
    fireteamId: number): __Observable<{Response?: FireteamResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getClanFireteamResponse(groupId, fireteamId).pipe(
      __map(_r => _r.body as {Response?: FireteamResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }
}

module FireteamService {
}

export { FireteamService }
