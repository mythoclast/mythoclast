import { Injectable } from '@angular/core';
import { OAuthStateService } from './oauth-state.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, forkJoin, ReplaySubject, Observer, Subscription, pipe, ObservableInput } from 'rxjs';
import { map, switchMap, take, filter } from 'rxjs/operators';
import { BungieNetConfiguration } from '../bungie-net-configuration';
import { StorageMap } from '@ngx-pwa/local-storage';
import { NgxBroadcastChannelService } from '@mythoclast/ngx-broadcast-channel';

export const TOKEN_STORAGE = 'TOKEN_STORAGE';

export interface TokenStorage {
  currentToken: string;
  tokenExpirationEpoch: number;
  refreshToken: string;
  refreshTokenExpirationEpoch: number;
  lastState: string;
  membershipId: string;
}

class WindowObserver implements Observer<{authorizationCode: string, state: string}> {
  private result$: ReplaySubject<{ authorizationCode: string, state: string }>;
  closed?: boolean;

  constructor(private window: Window, private params: URLSearchParams) {
    this.result$ = new ReplaySubject(1);
    this.closed = false;
  }

  complete(): void {
    this.closed = true;
  }

  next(value: { authorizationCode: string; state: string; }): void {
    this.result$.next(value);
  }

  error(err: any): void {
    this.result$.error(err);
  }

  acceptSubscription(subscription: Subscription): Observable<{ authorizationCode: string; state: string; }> {
    if (null !== subscription && typeof(subscription) !== 'undefined') {
      this.window.location.href = 'https://www.bungie.net/en/oauth/authorize?' + this.params.toString();
      return this.result$;
    }
    return of(null);
  }
}


/**
 * This service provides authentication functionality for all Bungie API interactions
 *
 * You only need interest yourself with the 'getActiveToken' and 'authorizeAndGetToken' methods.
 */
@Injectable({
  providedIn: 'root'
})
export class BungieNetAuthenticatorService {

  constructor(
    private stateService: OAuthStateService,
    private config: BungieNetConfiguration,
    private http: HttpClient,
    private storage: StorageMap,
    private channel: NgxBroadcastChannelService
  ) {}

  /**
   * Returns an observable of the currently active OAuth token.
   * If authorization has not occurred you will observe null.
   *
   * When this happens, you must instead call authorizeAndGetToken
   *
   * If the current token is expired but a valid refresh token is still present, a token refresh will occur automatically.
   */
  getActiveToken(): Observable<string> {
    return this.getTokenStorage().pipe(
      switchMap(
        tokenStorage => {
          const now = (new Date()).getTime();
          if (tokenStorage) {
            const currentDelta = tokenStorage.tokenExpirationEpoch - now;
            const refreshDelta = tokenStorage.refreshTokenExpirationEpoch - now;
            if (currentDelta < 120 && refreshDelta > 60) {
              return this.getRefreshedToken(tokenStorage.refreshToken).pipe(
                take(1),
                switchMap(rawToken => this.bnetTokenToObservableStorage(rawToken)),
                switchMap(storage => this.updateTokenStorage(storage)),
                map(storage => storage.currentToken)
              );
            }
            return of(tokenStorage.currentToken);
          }
          return of(null);
        }
      ),
      switchMap(result => {
        if (!!result) {
          return this.channel.sendTo('NGX-AUTH-ACK', true).pipe(map(() => result));
        }
        return of(result);
      })
    );
  }

  /**
   * Performs BNet authorization and returns an Observable of the active OAuth token
   *
   * Accepts an open window which will be redirected to Bungie's authorization portal.
   * You must have your application configured such that the redirect URL points to a location
   * with the same origin as your application. The content at this URL must accept the 'authorizationCode'
   * and 'state' query parameters and send a BroadcastMessage with an object of the form:
   * {
   *     authorizationCode: string,
   *     state: string
   * }
   * to the auth code receipt channel ('BNET-AUTH' by default).
   * If this message is not sent then the returned Observable will never complete.
   *
   * This code is then used to fetch the tokens used to actually authorize requests.
   * @param window The Window to perform authorization in. It must already be open, but will be closed for you.
   * @param reauth Set this to true to begin the authorization flow over again (this forces the user to re-agree to authorization)
   */
  authorizeAndGetToken(window: Window, reauth: boolean = false): Observable<string> {
    return this.observeAuthorizationParams().pipe(
      switchMap(params => this.handleAuthorizationWindow(window, params, reauth)),
      switchMap(code => this.consumeAuthCode(code)),
      switchMap(storage => this.updateTokenStorage(storage)),
      map(storage => storage.currentToken)
    );
  }

  private observeAuthorizationParams(): Observable<URLSearchParams> {
    const params = new URLSearchParams();
    return this.config.oauthClientId.pipe(switchMap((clientId) => {
      params.set('client_id', clientId);
      return of(params);
    }));
  }

  private handleAuthorizationWindow(win: Window, params: URLSearchParams, reauth: boolean = false): Observable<string> {
    return this.storage.delete(TOKEN_STORAGE).pipe(
      switchMap(
        () => forkJoin([of(win), this.stateService.getState()])
      ),
      switchMap(
        ([window, originalState]) => {
          params.append('response_type', 'code');
          params.set('state', originalState);
          if (reauth) {
            params.set('reauth', 'true');
          }
          const observer = new WindowObserver(window, params);
          const subscription = this.channel
          .getObservableChannel<{authorizationCode: string, state: string}>('BNET-AUTH')
          .pipe(take(1))
          .subscribe(observer);
          return forkJoin(
            {
              items: of({window, originalState}),
              result: observer.acceptSubscription(subscription).pipe(take(1))
            }
          );
        }
      ),
      map(({
        items: { window, originalState },
        result: { authorizationCode, state }
      }) => {
        window.close();
        if (originalState === state) {
          return authorizationCode;
        }
        return null;
      })
    );
  }

  private bnetTokenToObservableStorage(token: BungieNetToken): Observable<TokenStorage> {
    return of({
      currentToken: token.access_token,
      tokenExpirationEpoch: token.expires_in + (new Date()).getTime(),
      refreshToken: token.refresh_token,
      refreshTokenExpirationEpoch: token.refresh_expires_in + (new Date()).getTime(),
      lastState: (null) as string,
      membershipId: token.membership_id
    });
  }

  private consumeAuthCode(authorizationCode: string): Observable<TokenStorage> {
    return this.getInitialToken(authorizationCode).pipe(
      take(1),
      switchMap(bnetToken => this.bnetTokenToObservableStorage(bnetToken)),
      switchMap<TokenStorage, Observable<TokenStorage>>((value: TokenStorage) => this.storage.set(TOKEN_STORAGE, value)),
      switchMap<any, Observable<TokenStorage>>(() => (this.storage.get<TokenStorage>(TOKEN_STORAGE) as Observable<TokenStorage>))
    );
  }

  private getInitialToken(code: string): Observable<BungieNetToken> {
    const body = new URLSearchParams();
    body.set('grant_type', 'authorization_code');
    body.set('code', code);
    return this.fetchToken(of(body), of({
      'Content-Type': 'application/x-www-form-urlencoded'
    }));
  }

  private getRefreshedToken(refreshToken: string): Observable<BungieNetToken> {
    const body = new URLSearchParams();
    body.set('grant_type', 'refresh_token');
    body.set('refresh_token', refreshToken);
    return this.fetchToken(of(body), of({
      'Content-Type': 'application/x-www-form-urlencoded'
    }));
  }

  private fetchToken(params$: Observable<URLSearchParams>, headers$: Observable<{ [index: string]: string }>): Observable<BungieNetToken> {
    return forkJoin({
      params: params$,
      headers: headers$,
      clientId: this.config.oauthClientId,
      clientSecret: this.config.oauthClientSecret
    }).pipe(
      map(({ params, headers, clientId, clientSecret }) => {
        params.set('client_id', clientId);
        params.set('client_secret', clientSecret);
        return { params, headers };
      }),
      switchMap(
        ({ params, headers }) => this.http.post<BungieNetToken>('https://www.bungie.net/platform/app/oauth/token/', params.toString(), {
          headers: new HttpHeaders(headers)
        })
      )
    );
  }

  private getTokenStorage(): Observable<TokenStorage> {
    return this.storage.get<TokenStorage>(TOKEN_STORAGE) as Observable<TokenStorage>;
  }

  private saveTokenStorage(tokenStorage: TokenStorage): Observable<boolean> {
    return this.storage.set(TOKEN_STORAGE, tokenStorage).pipe(
      switchMap(() => this.channel.sendTo('NGX-AUTH-ACK', true)),
      map(() => true)
    );
  }

  private updateTokenStorage(tokenStorage: TokenStorage): Observable<TokenStorage> {
    return this.saveTokenStorage(tokenStorage).pipe(switchMap((_) => this.getTokenStorage()));
  }
}

// tslint:disable: variable-name
export class BungieNetToken {
  access_token: string;
  token_type: string;
  expires_in: number;
  refresh_token: string;
  refresh_expires_in: number;
  membership_id: string;
}
