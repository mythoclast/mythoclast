import { Injectable } from '@angular/core';
import * as ShaNS from 'jssha';
import * as NonceNS from 'nonce-fast';
import { Observable, of } from 'rxjs';
import { map, switchMap, take } from 'rxjs/operators';
import { BungieNetConfiguration } from '../bungie-net-configuration';
import { TokenStorage, TOKEN_STORAGE } from './bungie-net-authenticator.service';
import { StorageMap } from '@ngx-pwa/local-storage';

const SHA = ShaNS;
const Nonce = NonceNS;

@Injectable({
  providedIn: 'root'
})
export class OAuthStateService {

  constructor(private config: BungieNetConfiguration, private storage: StorageMap) { }

  getState(): Observable<string> {
    return this.storage.get<TokenStorage>(TOKEN_STORAGE).pipe(
      take(1),
      switchMap((tokenStorage: TokenStorage) => {
        if (!tokenStorage) {
          tokenStorage = {
            currentToken: null,
            tokenExpirationEpoch: null,
            refreshToken: null,
            refreshTokenExpirationEpoch: null,
            lastState: null,
            membershipId: null
          };
        }
        if (tokenStorage.lastState) {
          return of(tokenStorage.lastState);
        }
        return this.config.oauthClientSecret.pipe(
          take(1),
          map(secret => {
            const sha = new SHA('SHA-512', 'TEXT');
            sha.update(Nonce(9) + (new Date()).getTime() + secret);
            tokenStorage.lastState = sha.getHash('HEX');
            return tokenStorage;
          }),
          switchMap(
            storage => this.storage.set(TOKEN_STORAGE, storage).pipe(map(() => storage.lastState))
          )
        );
      })
    );
  }
}
