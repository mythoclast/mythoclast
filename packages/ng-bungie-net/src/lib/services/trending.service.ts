/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { BungieNetConfiguration as __Configuration } from '../bungie-net-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { TrendingCategories } from '../models/trending/trending-categories';
import { PlatformErrorCodes } from '../models/exceptions/platform-error-codes';
import { SearchResultOfTrendingEntry } from '../models/search-result-of-trending-entry';
import { TrendingDetail } from '../models/trending/trending-detail';

/**
 * trending
 */
@Injectable({
  providedIn: 'root',
})
class TrendingService extends __BaseService {
  static readonly getTrendingCategoriesPath = '/Trending/Categories/';
  static readonly getTrendingCategoryPath = '/Trending/Categories/{categoryId}/{pageNumber}/';
  static readonly getTrendingEntryDetailPath = '/Trending/Details/{trendingEntryType}/{identifier}/';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Returns trending items for Bungie.net, collapsed into the first page of items per category. For pagination within a category, call GetTrendingCategory.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getTrendingCategoriesResponse(): __Observable<__StrictHttpResponse<{Response?: TrendingCategories, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Trending/Categories/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: TrendingCategories, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Returns trending items for Bungie.net, collapsed into the first page of items per category. For pagination within a category, call GetTrendingCategory.
   * @return Look at the Response property for more information about the nature of this response
   */
  getTrendingCategories(): __Observable<{Response?: TrendingCategories, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getTrendingCategoriesResponse().pipe(
      __map(_r => _r.body as {Response?: TrendingCategories, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Returns paginated lists of trending items for a category.
   * @param pageNumber The page # of results to return.
   * @param categoryId The ID of the category for whom you want additional results.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getTrendingCategoryResponse(pageNumber: number,
    categoryId: string): __Observable<__StrictHttpResponse<{Response?: SearchResultOfTrendingEntry, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Trending/Categories/${categoryId}/${pageNumber}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: SearchResultOfTrendingEntry, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Returns paginated lists of trending items for a category.
   * @param pageNumber The page # of results to return.
   * @param categoryId The ID of the category for whom you want additional results.
   * @return Look at the Response property for more information about the nature of this response
   */
  getTrendingCategory(pageNumber: number,
    categoryId: string): __Observable<{Response?: SearchResultOfTrendingEntry, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getTrendingCategoryResponse(pageNumber, categoryId).pipe(
      __map(_r => _r.body as {Response?: SearchResultOfTrendingEntry, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Returns the detailed results for a specific trending entry. Note that trending entries are uniquely identified by a combination of *both* the TrendingEntryType *and* the identifier: the identifier alone is not guaranteed to be globally unique.
   * @param trendingEntryType The type of entity to be returned.
   * @param identifier The identifier for the entity to be returned.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getTrendingEntryDetailResponse(trendingEntryType: number,
    identifier: string): __Observable<__StrictHttpResponse<{Response?: TrendingDetail, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Trending/Details/${trendingEntryType}/${identifier}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: TrendingDetail, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Returns the detailed results for a specific trending entry. Note that trending entries are uniquely identified by a combination of *both* the TrendingEntryType *and* the identifier: the identifier alone is not guaranteed to be globally unique.
   * @param trendingEntryType The type of entity to be returned.
   * @param identifier The identifier for the entity to be returned.
   * @return Look at the Response property for more information about the nature of this response
   */
  getTrendingEntryDetail(trendingEntryType: number,
    identifier: string): __Observable<{Response?: TrendingDetail, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getTrendingEntryDetailResponse(trendingEntryType, identifier).pipe(
      __map(_r => _r.body as {Response?: TrendingDetail, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }
}

module TrendingService {
}

export { TrendingService }
