/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { BungieNetConfiguration as __Configuration } from '../bungie-net-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { DestinyManifest } from '../models/destiny/config/destiny-manifest';
import { PlatformErrorCodes } from '../models/exceptions/platform-error-codes';
import { DestinyDefinition } from '../models/destiny/definitions/destiny-definition';
import { UserInfoCard } from '../models/user/user-info-card';
import { BungieMembershipType } from '../models/bungie-membership-type';
import { DestinyLinkedProfilesResponse } from '../models/destiny/responses/destiny-linked-profiles-response';
import { DestinyProfileResponse } from '../models/destiny/responses/destiny-profile-response';
import { DestinyComponentType } from '../models/destiny/destiny-component-type';
import { DestinyCharacterResponse } from '../models/destiny/responses/destiny-character-response';
import { DestinyMilestone } from '../models/destiny/milestones/destiny-milestone';
import { DestinyItemResponse } from '../models/destiny/responses/destiny-item-response';
import { DestinyVendorsResponse } from '../models/destiny/responses/destiny-vendors-response';
import { DestinyVendorResponse } from '../models/destiny/responses/destiny-vendor-response';
import { DestinyPublicVendorsResponse } from '../models/destiny/responses/destiny-public-vendors-response';
import { DestinyCollectibleNodeDetailResponse } from '../models/destiny/responses/destiny-collectible-node-detail-response';
import { DestinyEquipItemResults } from '../models/destiny/destiny-equip-item-results';
import { DestinyItemChangeResponse } from '../models/destiny/responses/destiny-item-change-response';
import { DestinyPostGameCarnageReportData } from '../models/destiny/historical-stats/destiny-post-game-carnage-report-data';
import { DestinyHistoricalStatsDefinition } from '../models/destiny/historical-stats/definitions/destiny-historical-stats-definition';
import { DestinyLeaderboard } from '../models/destiny/historical-stats/destiny-leaderboard';
import { DestinyClanAggregateStat } from '../models/destiny/historical-stats/destiny-clan-aggregate-stat';
import { DestinyEntitySearchResult } from '../models/destiny/definitions/destiny-entity-search-result';
import { DestinyHistoricalStatsByPeriod } from '../models/destiny/historical-stats/destiny-historical-stats-by-period';
import { DestinyActivityModeType } from '../models/destiny/historical-stats/definitions/destiny-activity-mode-type';
import { DestinyStatsGroupType } from '../models/destiny/historical-stats/definitions/destiny-stats-group-type';
import { DestinyHistoricalStatsAccountResult } from '../models/destiny/historical-stats/destiny-historical-stats-account-result';
import { DestinyActivityHistoryResults } from '../models/destiny/historical-stats/destiny-activity-history-results';
import { DestinyHistoricalWeaponStatsData } from '../models/destiny/historical-stats/destiny-historical-weapon-stats-data';
import { DestinyAggregateActivityResults } from '../models/destiny/historical-stats/destiny-aggregate-activity-results';
import { DestinyMilestoneContent } from '../models/destiny/milestones/destiny-milestone-content';
import { DestinyPublicMilestone } from '../models/destiny/milestones/destiny-public-milestone';
import { AwaInitializeResponse } from '../models/destiny/advanced/awa-initialize-response';
import { AwaAuthorizationResult } from '../models/destiny/advanced/awa-authorization-result';

/**
 * destiny2
 */
@Injectable({
  providedIn: 'root',
})
class Destiny2Service extends __BaseService {
  static readonly getDestinyManifestPath = '/Destiny2/Manifest/';
  static readonly getDestinyEntityDefinitionPath = '/Destiny2/Manifest/{entityType}/{hashIdentifier}/';
  static readonly searchDestinyPlayerPath = '/Destiny2/SearchDestinyPlayer/{membershipType}/{displayName}/';
  static readonly getLinkedProfilesPath = '/Destiny2/{membershipType}/Profile/{membershipId}/LinkedProfiles/';
  static readonly getProfilePath = '/Destiny2/{membershipType}/Profile/{destinyMembershipId}/';
  static readonly getCharacterPath = '/Destiny2/{membershipType}/Profile/{destinyMembershipId}/Character/{characterId}/';
  static readonly getClanWeeklyRewardStatePath = '/Destiny2/Clan/{groupId}/WeeklyRewardState/';
  static readonly getItemPath = '/Destiny2/{membershipType}/Profile/{destinyMembershipId}/Item/{itemInstanceId}/';
  static readonly getVendorsPath = '/Destiny2/{membershipType}/Profile/{destinyMembershipId}/Character/{characterId}/Vendors/';
  static readonly getVendorPath = '/Destiny2/{membershipType}/Profile/{destinyMembershipId}/Character/{characterId}/Vendors/{vendorHash}/';
  static readonly getPublicVendorsPath = '/Destiny2//Vendors/';
  static readonly getCollectibleNodeDetailsPath = '/Destiny2/{membershipType}/Profile/{destinyMembershipId}/Character/{characterId}/Collectibles/{collectiblePresentationNodeHash}/';
  static readonly transferItemPath = '/Destiny2/Actions/Items/TransferItem/';
  static readonly pullFromPostmasterPath = '/Destiny2/Actions/Items/PullFromPostmaster/';
  static readonly equipItemPath = '/Destiny2/Actions/Items/EquipItem/';
  static readonly equipItemsPath = '/Destiny2/Actions/Items/EquipItems/';
  static readonly setItemLockStatePath = '/Destiny2/Actions/Items/SetLockState/';
  static readonly insertSocketPlugPath = '/Destiny2/Actions/Items/InsertSocketPlug/';
  static readonly getPostGameCarnageReportPath = '/Destiny2/Stats/PostGameCarnageReport/{activityId}/';
  static readonly reportOffensivePostGameCarnageReportPlayerPath = '/Destiny2/Stats/PostGameCarnageReport/{activityId}/Report/';
  static readonly getHistoricalStatsDefinitionPath = '/Destiny2/Stats/Definition/';
  static readonly getClanLeaderboardsPath = '/Destiny2/Stats/Leaderboards/Clans/{groupId}/';
  static readonly getClanAggregateStatsPath = '/Destiny2/Stats/AggregateClanStats/{groupId}/';
  static readonly getLeaderboardsPath = '/Destiny2/{membershipType}/Account/{destinyMembershipId}/Stats/Leaderboards/';
  static readonly getLeaderboardsForCharacterPath = '/Destiny2/Stats/Leaderboards/{membershipType}/{destinyMembershipId}/{characterId}/';
  static readonly searchDestinyEntitiesPath = '/Destiny2/Armory/Search/{type}/{searchTerm}/';
  static readonly getHistoricalStatsPath = '/Destiny2/{membershipType}/Account/{destinyMembershipId}/Character/{characterId}/Stats/';
  static readonly getHistoricalStatsForAccountPath = '/Destiny2/{membershipType}/Account/{destinyMembershipId}/Stats/';
  static readonly getActivityHistoryPath = '/Destiny2/{membershipType}/Account/{destinyMembershipId}/Character/{characterId}/Stats/Activities/';
  static readonly getUniqueWeaponHistoryPath = '/Destiny2/{membershipType}/Account/{destinyMembershipId}/Character/{characterId}/Stats/UniqueWeapons/';
  static readonly getDestinyAggregateActivityStatsPath = '/Destiny2/{membershipType}/Account/{destinyMembershipId}/Character/{characterId}/Stats/AggregateActivityStats/';
  static readonly getPublicMilestoneContentPath = '/Destiny2/Milestones/{milestoneHash}/Content/';
  static readonly getPublicMilestonesPath = '/Destiny2/Milestones/';
  static readonly awaInitializeRequestPath = '/Destiny2/Awa/Initialize/';
  static readonly awaProvideAuthorizationResultPath = '/Destiny2/Awa/AwaProvideAuthorizationResult/';
  static readonly awaGetActionTokenPath = '/Destiny2/Awa/GetActionToken/{correlationId}/';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Returns the current version of the manifest as a json object.
   * @return DestinyManifest is the external-facing contract for just the properties needed by those calling the Destiny Platform.
   */
  private getDestinyManifestResponse(): __Observable<__StrictHttpResponse<{Response?: DestinyManifest, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Destiny2/Manifest/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: DestinyManifest, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Returns the current version of the manifest as a json object.
   * @return DestinyManifest is the external-facing contract for just the properties needed by those calling the Destiny Platform.
   */
  getDestinyManifest(): __Observable<{Response?: DestinyManifest, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getDestinyManifestResponse().pipe(
      __map(_r => _r.body as {Response?: DestinyManifest, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Returns the static definition of an entity of the given Type and hash identifier. Examine the API Documentation for the Type Names of entities that have their own definitions. Note that the return type will always *inherit from* DestinyDefinition, but the specific type returned will be the requested entity type if it can be found. Please don't use this as a chatty alternative to the Manifest database if you require large sets of data, but for simple and one-off accesses this should be handy.
   * @param hashIdentifier The hash identifier for the specific Entity you want returned.
   * @param entityType The type of entity for whom you would like results. These correspond to the entity's definition contract name. For instance, if you are looking for items, this property should be 'DestinyInventoryItemDefinition'. PREVIEW: This endpoint is still in beta, and may experience rough edges. The schema is tentatively in final form, but there may be bugs that prevent desirable operation.
   * @return Provides common properties for destiny definitions.
   */
  private getDestinyEntityDefinitionResponse(hashIdentifier: number,
    entityType: string): __Observable<__StrictHttpResponse<{Response?: DestinyDefinition, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Destiny2/Manifest/${entityType}/${hashIdentifier}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: DestinyDefinition, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Returns the static definition of an entity of the given Type and hash identifier. Examine the API Documentation for the Type Names of entities that have their own definitions. Note that the return type will always *inherit from* DestinyDefinition, but the specific type returned will be the requested entity type if it can be found. Please don't use this as a chatty alternative to the Manifest database if you require large sets of data, but for simple and one-off accesses this should be handy.
   * @param hashIdentifier The hash identifier for the specific Entity you want returned.
   * @param entityType The type of entity for whom you would like results. These correspond to the entity's definition contract name. For instance, if you are looking for items, this property should be 'DestinyInventoryItemDefinition'. PREVIEW: This endpoint is still in beta, and may experience rough edges. The schema is tentatively in final form, but there may be bugs that prevent desirable operation.
   * @return Provides common properties for destiny definitions.
   */
  getDestinyEntityDefinition(hashIdentifier: number,
    entityType: string): __Observable<{Response?: DestinyDefinition, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getDestinyEntityDefinitionResponse(hashIdentifier, entityType).pipe(
      __map(_r => _r.body as {Response?: DestinyDefinition, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Returns a list of Destiny memberships given a full Gamertag or PSN ID.
   * @param membershipType A valid non-BungieNet membership type, or All.
   * @param displayName The full gamertag or PSN id of the player. Spaces and case are ignored.
   * @return Look at the Response property for more information about the nature of this response
   */
  private searchDestinyPlayerResponse(membershipType: BungieMembershipType,
    displayName: string): __Observable<__StrictHttpResponse<{Response?: Array<UserInfoCard>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Destiny2/SearchDestinyPlayer/${membershipType}/${displayName}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: Array<UserInfoCard>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Returns a list of Destiny memberships given a full Gamertag or PSN ID.
   * @param membershipType A valid non-BungieNet membership type, or All.
   * @param displayName The full gamertag or PSN id of the player. Spaces and case are ignored.
   * @return Look at the Response property for more information about the nature of this response
   */
  searchDestinyPlayer(membershipType: BungieMembershipType,
    displayName: string): __Observable<{Response?: Array<UserInfoCard>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.searchDestinyPlayerResponse(membershipType, displayName).pipe(
      __map(_r => _r.body as {Response?: Array<UserInfoCard>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Returns a summary information about all profiles linked to the requesting membership type/membership ID that have valid Destiny information. The passed-in Membership Type/Membership ID may be a Bungie.Net membership or a Destiny membership. It only returns the minimal amount of data to begin making more substantive requests, but will hopefully serve as a useful alternative to UserServices for people who just care about Destiny data. Note that it will only return linked accounts whose linkages you are allowed to view.
   * @param membershipType The type for the membership whose linked Destiny accounts you want returned.
   * @param membershipId The ID of the membership whose linked Destiny accounts you want returned. Make sure your membership ID matches its Membership Type: don't pass us a PSN membership ID and the XBox membership type, it's not going to work!
   * @param getAllMemberships (optional) if set to 'true', all memberships regardless of whether they're obscured by overrides will be returned. Normal privacy restrictions on account linking will still apply no matter what.
   * @return I know what you seek. You seek linked accounts. Found them, you have.
   * This contract returns a minimal amount of data about Destiny Accounts that are linked through your Bungie.Net account. We will not return accounts in this response whose
   */
  private getLinkedProfilesResponse(membershipType: BungieMembershipType,
    membershipId: number,
    getAllMemberships?: boolean): __Observable<__StrictHttpResponse<{Response?: DestinyLinkedProfilesResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    if (getAllMemberships != null) __params = __params.set('getAllMemberships', getAllMemberships.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Destiny2/${membershipType}/Profile/${membershipId}/LinkedProfiles/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: DestinyLinkedProfilesResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Returns a summary information about all profiles linked to the requesting membership type/membership ID that have valid Destiny information. The passed-in Membership Type/Membership ID may be a Bungie.Net membership or a Destiny membership. It only returns the minimal amount of data to begin making more substantive requests, but will hopefully serve as a useful alternative to UserServices for people who just care about Destiny data. Note that it will only return linked accounts whose linkages you are allowed to view.
   * @param membershipType The type for the membership whose linked Destiny accounts you want returned.
   * @param membershipId The ID of the membership whose linked Destiny accounts you want returned. Make sure your membership ID matches its Membership Type: don't pass us a PSN membership ID and the XBox membership type, it's not going to work!
   * @param getAllMemberships (optional) if set to 'true', all memberships regardless of whether they're obscured by overrides will be returned. Normal privacy restrictions on account linking will still apply no matter what.
   * @return I know what you seek. You seek linked accounts. Found them, you have.
   * This contract returns a minimal amount of data about Destiny Accounts that are linked through your Bungie.Net account. We will not return accounts in this response whose
   */
  getLinkedProfiles(membershipType: BungieMembershipType,
    membershipId: number,
    getAllMemberships?: boolean): __Observable<{Response?: DestinyLinkedProfilesResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getLinkedProfilesResponse(membershipType, membershipId, getAllMemberships).pipe(
      __map(_r => _r.body as {Response?: DestinyLinkedProfilesResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Returns Destiny Profile information for the supplied membership.
   * @param membershipType A valid non-BungieNet membership type.
   * @param destinyMembershipId Destiny membership ID.
   * @param components A comma separated list of components to return (as strings or numeric values). See the DestinyComponentType enum for valid components to request. You must request at least one component to receive results.
   * @return The response for GetDestinyProfile, with components for character and item-level data.
   */
  private getProfileResponse(membershipType: BungieMembershipType,
    destinyMembershipId: number,
    components?: Array<DestinyComponentType>): __Observable<__StrictHttpResponse<{Response?: DestinyProfileResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    (components || []).forEach(val => {if (val != null) __params = __params.append('components', val.toString())});
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Destiny2/${membershipType}/Profile/${destinyMembershipId}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: DestinyProfileResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Returns Destiny Profile information for the supplied membership.
   * @param membershipType A valid non-BungieNet membership type.
   * @param destinyMembershipId Destiny membership ID.
   * @param components A comma separated list of components to return (as strings or numeric values). See the DestinyComponentType enum for valid components to request. You must request at least one component to receive results.
   * @return The response for GetDestinyProfile, with components for character and item-level data.
   */
  getProfile(membershipType: BungieMembershipType,
    destinyMembershipId: number,
    components?: Array<DestinyComponentType>): __Observable<{Response?: DestinyProfileResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getProfileResponse(membershipType, destinyMembershipId, components).pipe(
      __map(_r => _r.body as {Response?: DestinyProfileResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Returns character information for the supplied character.
   * @param membershipType A valid non-BungieNet membership type.
   * @param destinyMembershipId Destiny membership ID.
   * @param characterId ID of the character.
   * @param components A comma separated list of components to return (as strings or numeric values). See the DestinyComponentType enum for valid components to request. You must request at least one component to receive results.
   * @return The response contract for GetDestinyCharacter, with components that can be returned for character and item-level data.
   */
  private getCharacterResponse(membershipType: BungieMembershipType,
    destinyMembershipId: number,
    characterId: number,
    components?: Array<DestinyComponentType>): __Observable<__StrictHttpResponse<{Response?: DestinyCharacterResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;



    (components || []).forEach(val => {if (val != null) __params = __params.append('components', val.toString())});
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Destiny2/${membershipType}/Profile/${destinyMembershipId}/Character/${characterId}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: DestinyCharacterResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Returns character information for the supplied character.
   * @param membershipType A valid non-BungieNet membership type.
   * @param destinyMembershipId Destiny membership ID.
   * @param characterId ID of the character.
   * @param components A comma separated list of components to return (as strings or numeric values). See the DestinyComponentType enum for valid components to request. You must request at least one component to receive results.
   * @return The response contract for GetDestinyCharacter, with components that can be returned for character and item-level data.
   */
  getCharacter(membershipType: BungieMembershipType,
    destinyMembershipId: number,
    characterId: number,
    components?: Array<DestinyComponentType>): __Observable<{Response?: DestinyCharacterResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getCharacterResponse(membershipType, destinyMembershipId, characterId, components).pipe(
      __map(_r => _r.body as {Response?: DestinyCharacterResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Returns information on the weekly clan rewards and if the clan has earned them or not. Note that this will always report rewards as not redeemed.
   * @param groupId A valid group id of clan.
   * @return Represents a runtime instance of a user's milestone status. Live Milestone data should be combined with DestinyMilestoneDefinition data to show the user a picture of what is available for them to do in the game, and their status in regards to said "things to do." Consider it a big, wonky to-do list, or Advisors 3.0 for those who remember the Destiny 1 API.
   */
  private getClanWeeklyRewardStateResponse(groupId: number): __Observable<__StrictHttpResponse<{Response?: DestinyMilestone, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Destiny2/Clan/${groupId}/WeeklyRewardState/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: DestinyMilestone, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Returns information on the weekly clan rewards and if the clan has earned them or not. Note that this will always report rewards as not redeemed.
   * @param groupId A valid group id of clan.
   * @return Represents a runtime instance of a user's milestone status. Live Milestone data should be combined with DestinyMilestoneDefinition data to show the user a picture of what is available for them to do in the game, and their status in regards to said "things to do." Consider it a big, wonky to-do list, or Advisors 3.0 for those who remember the Destiny 1 API.
   */
  getClanWeeklyRewardState(groupId: number): __Observable<{Response?: DestinyMilestone, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getClanWeeklyRewardStateResponse(groupId).pipe(
      __map(_r => _r.body as {Response?: DestinyMilestone, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Retrieve the details of an instanced Destiny Item. An instanced Destiny item is one with an ItemInstanceId. Non-instanced items, such as materials, have no useful instance-specific details and thus are not queryable here.
   * @param membershipType A valid non-BungieNet membership type.
   * @param itemInstanceId The Instance ID of the destiny item.
   * @param destinyMembershipId The membership ID of the destiny profile.
   * @param components A comma separated list of components to return (as strings or numeric values). See the DestinyComponentType enum for valid components to request. You must request at least one component to receive results.
   * @return The response object for retrieving an individual instanced item. None of these components are relevant for an item that doesn't have an "itemInstanceId": for those, get your information from the DestinyInventoryDefinition.
   */
  private getItemResponse(membershipType: BungieMembershipType,
    itemInstanceId: number,
    destinyMembershipId: number,
    components?: Array<DestinyComponentType>): __Observable<__StrictHttpResponse<{Response?: DestinyItemResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;



    (components || []).forEach(val => {if (val != null) __params = __params.append('components', val.toString())});
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Destiny2/${membershipType}/Profile/${destinyMembershipId}/Item/${itemInstanceId}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: DestinyItemResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Retrieve the details of an instanced Destiny Item. An instanced Destiny item is one with an ItemInstanceId. Non-instanced items, such as materials, have no useful instance-specific details and thus are not queryable here.
   * @param membershipType A valid non-BungieNet membership type.
   * @param itemInstanceId The Instance ID of the destiny item.
   * @param destinyMembershipId The membership ID of the destiny profile.
   * @param components A comma separated list of components to return (as strings or numeric values). See the DestinyComponentType enum for valid components to request. You must request at least one component to receive results.
   * @return The response object for retrieving an individual instanced item. None of these components are relevant for an item that doesn't have an "itemInstanceId": for those, get your information from the DestinyInventoryDefinition.
   */
  getItem(membershipType: BungieMembershipType,
    itemInstanceId: number,
    destinyMembershipId: number,
    components?: Array<DestinyComponentType>): __Observable<{Response?: DestinyItemResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getItemResponse(membershipType, itemInstanceId, destinyMembershipId, components).pipe(
      __map(_r => _r.body as {Response?: DestinyItemResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Get currently available vendors from the list of vendors that can possibly have rotating inventory. Note that this does not include things like preview vendors and vendors-as-kiosks, neither of whom have rotating/dynamic inventories. Use their definitions as-is for those.
   * @param membershipType A valid non-BungieNet membership type.
   * @param destinyMembershipId Destiny membership ID of another user. You may be denied.
   * @param characterId The Destiny Character ID of the character for whom we're getting vendor info.
   * @param components A comma separated list of components to return (as strings or numeric values). See the DestinyComponentType enum for valid components to request. You must request at least one component to receive results.
   * @return A response containing all of the components for all requested vendors.
   */
  private getVendorsResponse(membershipType: BungieMembershipType,
    destinyMembershipId: number,
    characterId: number,
    components?: Array<DestinyComponentType>): __Observable<__StrictHttpResponse<{Response?: DestinyVendorsResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;



    (components || []).forEach(val => {if (val != null) __params = __params.append('components', val.toString())});
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Destiny2/${membershipType}/Profile/${destinyMembershipId}/Character/${characterId}/Vendors/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: DestinyVendorsResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Get currently available vendors from the list of vendors that can possibly have rotating inventory. Note that this does not include things like preview vendors and vendors-as-kiosks, neither of whom have rotating/dynamic inventories. Use their definitions as-is for those.
   * @param membershipType A valid non-BungieNet membership type.
   * @param destinyMembershipId Destiny membership ID of another user. You may be denied.
   * @param characterId The Destiny Character ID of the character for whom we're getting vendor info.
   * @param components A comma separated list of components to return (as strings or numeric values). See the DestinyComponentType enum for valid components to request. You must request at least one component to receive results.
   * @return A response containing all of the components for all requested vendors.
   */
  getVendors(membershipType: BungieMembershipType,
    destinyMembershipId: number,
    characterId: number,
    components?: Array<DestinyComponentType>): __Observable<{Response?: DestinyVendorsResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getVendorsResponse(membershipType, destinyMembershipId, characterId, components).pipe(
      __map(_r => _r.body as {Response?: DestinyVendorsResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Get the details of a specific Vendor.
   * @param vendorHash The Hash identifier of the Vendor to be returned.
   * @param membershipType A valid non-BungieNet membership type.
   * @param destinyMembershipId Destiny membership ID of another user. You may be denied.
   * @param characterId The Destiny Character ID of the character for whom we're getting vendor info.
   * @param components A comma separated list of components to return (as strings or numeric values). See the DestinyComponentType enum for valid components to request. You must request at least one component to receive results.
   * @return A response containing all of the components for a vendor.
   */
  private getVendorResponse(vendorHash: number,
    membershipType: BungieMembershipType,
    destinyMembershipId: number,
    characterId: number,
    components?: Array<DestinyComponentType>): __Observable<__StrictHttpResponse<{Response?: DestinyVendorResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;




    (components || []).forEach(val => {if (val != null) __params = __params.append('components', val.toString())});
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Destiny2/${membershipType}/Profile/${destinyMembershipId}/Character/${characterId}/Vendors/${vendorHash}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: DestinyVendorResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Get the details of a specific Vendor.
   * @param vendorHash The Hash identifier of the Vendor to be returned.
   * @param membershipType A valid non-BungieNet membership type.
   * @param destinyMembershipId Destiny membership ID of another user. You may be denied.
   * @param characterId The Destiny Character ID of the character for whom we're getting vendor info.
   * @param components A comma separated list of components to return (as strings or numeric values). See the DestinyComponentType enum for valid components to request. You must request at least one component to receive results.
   * @return A response containing all of the components for a vendor.
   */
  getVendor(vendorHash: number,
    membershipType: BungieMembershipType,
    destinyMembershipId: number,
    characterId: number,
    components?: Array<DestinyComponentType>): __Observable<{Response?: DestinyVendorResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getVendorResponse(vendorHash, membershipType, destinyMembershipId, characterId, components).pipe(
      __map(_r => _r.body as {Response?: DestinyVendorResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Get items available from vendors where the vendors have items for sale that are common for everyone. If any portion of the Vendor's available inventory is character or account specific, we will be unable to return their data from this endpoint due to the way that available inventory is computed. As I am often guilty of saying: 'It's a long story...'
   * @param components A comma separated list of components to return (as strings or numeric values). See the DestinyComponentType enum for valid components to request. You must request at least one component to receive results.
   * @return A response containing all valid components for the public Vendors endpoint.
   *  It is a decisively smaller subset of data compared to what we can get when we know the specific user making the request.
   *  If you want any of the other data - item details, whether or not you can buy it, etc... you'll have to call in the context of a character. I know, sad but true.
   */
  private getPublicVendorsResponse(components?: Array<DestinyComponentType>): __Observable<__StrictHttpResponse<{Response?: DestinyPublicVendorsResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    (components || []).forEach(val => {if (val != null) __params = __params.append('components', val.toString())});
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Destiny2//Vendors/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: DestinyPublicVendorsResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Get items available from vendors where the vendors have items for sale that are common for everyone. If any portion of the Vendor's available inventory is character or account specific, we will be unable to return their data from this endpoint due to the way that available inventory is computed. As I am often guilty of saying: 'It's a long story...'
   * @param components A comma separated list of components to return (as strings or numeric values). See the DestinyComponentType enum for valid components to request. You must request at least one component to receive results.
   * @return A response containing all valid components for the public Vendors endpoint.
   *  It is a decisively smaller subset of data compared to what we can get when we know the specific user making the request.
   *  If you want any of the other data - item details, whether or not you can buy it, etc... you'll have to call in the context of a character. I know, sad but true.
   */
  getPublicVendors(components?: Array<DestinyComponentType>): __Observable<{Response?: DestinyPublicVendorsResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getPublicVendorsResponse(components).pipe(
      __map(_r => _r.body as {Response?: DestinyPublicVendorsResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Given a Presentation Node that has Collectibles as direct descendants, this will return item details about those descendants in the context of the requesting character.
   * @param membershipType A valid non-BungieNet membership type.
   * @param destinyMembershipId Destiny membership ID of another user. You may be denied.
   * @param collectiblePresentationNodeHash The hash identifier of the Presentation Node for whom we should return collectible details. Details will only be returned for collectibles that are direct descendants of this node.
   * @param characterId The Destiny Character ID of the character for whom we're getting collectible detail info.
   * @param components A comma separated list of components to return (as strings or numeric values). See the DestinyComponentType enum for valid components to request. You must request at least one component to receive results.
   * @return Returns the detailed information about a Collectible Presentation Node and any Collectibles that are direct descendants.
   */
  private getCollectibleNodeDetailsResponse(membershipType: BungieMembershipType,
    destinyMembershipId: number,
    collectiblePresentationNodeHash: number,
    characterId: number,
    components?: Array<DestinyComponentType>): __Observable<__StrictHttpResponse<{Response?: DestinyCollectibleNodeDetailResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;




    (components || []).forEach(val => {if (val != null) __params = __params.append('components', val.toString())});
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Destiny2/${membershipType}/Profile/${destinyMembershipId}/Character/${characterId}/Collectibles/${collectiblePresentationNodeHash}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: DestinyCollectibleNodeDetailResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Given a Presentation Node that has Collectibles as direct descendants, this will return item details about those descendants in the context of the requesting character.
   * @param membershipType A valid non-BungieNet membership type.
   * @param destinyMembershipId Destiny membership ID of another user. You may be denied.
   * @param collectiblePresentationNodeHash The hash identifier of the Presentation Node for whom we should return collectible details. Details will only be returned for collectibles that are direct descendants of this node.
   * @param characterId The Destiny Character ID of the character for whom we're getting collectible detail info.
   * @param components A comma separated list of components to return (as strings or numeric values). See the DestinyComponentType enum for valid components to request. You must request at least one component to receive results.
   * @return Returns the detailed information about a Collectible Presentation Node and any Collectibles that are direct descendants.
   */
  getCollectibleNodeDetails(membershipType: BungieMembershipType,
    destinyMembershipId: number,
    collectiblePresentationNodeHash: number,
    characterId: number,
    components?: Array<DestinyComponentType>): __Observable<{Response?: DestinyCollectibleNodeDetailResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getCollectibleNodeDetailsResponse(membershipType, destinyMembershipId, collectiblePresentationNodeHash, characterId, components).pipe(
      __map(_r => _r.body as {Response?: DestinyCollectibleNodeDetailResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Transfer an item to/from your vault. You must have a valid Destiny account. You must also pass BOTH a reference AND an instance ID if it's an instanced item. itshappening.gif
   * @return Look at the Response property for more information about the nature of this response
   */
  private transferItemResponse(): __Observable<__StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/Destiny2/Actions/Items/TransferItem/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Transfer an item to/from your vault. You must have a valid Destiny account. You must also pass BOTH a reference AND an instance ID if it's an instanced item. itshappening.gif
   * @return Look at the Response property for more information about the nature of this response
   */
  transferItem(): __Observable<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.transferItemResponse().pipe(
      __map(_r => _r.body as {Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Extract an item from the Postmaster, with whatever implications that may entail. You must have a valid Destiny account. You must also pass BOTH a reference AND an instance ID if it's an instanced item.
   * @return Look at the Response property for more information about the nature of this response
   */
  private pullFromPostmasterResponse(): __Observable<__StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/Destiny2/Actions/Items/PullFromPostmaster/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Extract an item from the Postmaster, with whatever implications that may entail. You must have a valid Destiny account. You must also pass BOTH a reference AND an instance ID if it's an instanced item.
   * @return Look at the Response property for more information about the nature of this response
   */
  pullFromPostmaster(): __Observable<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.pullFromPostmasterResponse().pipe(
      __map(_r => _r.body as {Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Equip an item. You must have a valid Destiny Account, and either be in a social space, in orbit, or offline.
   * @return Look at the Response property for more information about the nature of this response
   */
  private equipItemResponse(): __Observable<__StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/Destiny2/Actions/Items/EquipItem/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Equip an item. You must have a valid Destiny Account, and either be in a social space, in orbit, or offline.
   * @return Look at the Response property for more information about the nature of this response
   */
  equipItem(): __Observable<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.equipItemResponse().pipe(
      __map(_r => _r.body as {Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Equip a list of items by itemInstanceIds. You must have a valid Destiny Account, and either be in a social space, in orbit, or offline. Any items not found on your character will be ignored.
   * @return The results of a bulk Equipping operation performed through the Destiny API.
   */
  private equipItemsResponse(): __Observable<__StrictHttpResponse<{Response?: DestinyEquipItemResults, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/Destiny2/Actions/Items/EquipItems/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: DestinyEquipItemResults, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Equip a list of items by itemInstanceIds. You must have a valid Destiny Account, and either be in a social space, in orbit, or offline. Any items not found on your character will be ignored.
   * @return The results of a bulk Equipping operation performed through the Destiny API.
   */
  equipItems(): __Observable<{Response?: DestinyEquipItemResults, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.equipItemsResponse().pipe(
      __map(_r => _r.body as {Response?: DestinyEquipItemResults, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Set the Lock State for an instanced item. You must have a valid Destiny Account.
   * @return Look at the Response property for more information about the nature of this response
   */
  private setItemLockStateResponse(): __Observable<__StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/Destiny2/Actions/Items/SetLockState/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Set the Lock State for an instanced item. You must have a valid Destiny Account.
   * @return Look at the Response property for more information about the nature of this response
   */
  setItemLockState(): __Observable<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.setItemLockStateResponse().pipe(
      __map(_r => _r.body as {Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Insert a plug into a socketed item. I know how it sounds, but I assure you it's much more G-rated than you might be guessing. We haven't decided yet whether this will be able to insert plugs that have side effects, but if we do it will require special scope permission for an application attempting to do so. You must have a valid Destiny Account, and either be in a social space, in orbit, or offline. Request must include proof of permission for 'InsertPlugs' from the account owner.
   * @return Look at the Response property for more information about the nature of this response
   */
  private insertSocketPlugResponse(): __Observable<__StrictHttpResponse<{Response?: DestinyItemChangeResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/Destiny2/Actions/Items/InsertSocketPlug/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: DestinyItemChangeResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Insert a plug into a socketed item. I know how it sounds, but I assure you it's much more G-rated than you might be guessing. We haven't decided yet whether this will be able to insert plugs that have side effects, but if we do it will require special scope permission for an application attempting to do so. You must have a valid Destiny Account, and either be in a social space, in orbit, or offline. Request must include proof of permission for 'InsertPlugs' from the account owner.
   * @return Look at the Response property for more information about the nature of this response
   */
  insertSocketPlug(): __Observable<{Response?: DestinyItemChangeResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.insertSocketPlugResponse().pipe(
      __map(_r => _r.body as {Response?: DestinyItemChangeResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Gets the available post game carnage report for the activity ID.
   * @param activityId The ID of the activity whose PGCR is requested.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getPostGameCarnageReportResponse(activityId: number): __Observable<__StrictHttpResponse<{Response?: DestinyPostGameCarnageReportData, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Destiny2/Stats/PostGameCarnageReport/${activityId}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: DestinyPostGameCarnageReportData, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets the available post game carnage report for the activity ID.
   * @param activityId The ID of the activity whose PGCR is requested.
   * @return Look at the Response property for more information about the nature of this response
   */
  getPostGameCarnageReport(activityId: number): __Observable<{Response?: DestinyPostGameCarnageReportData, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getPostGameCarnageReportResponse(activityId).pipe(
      __map(_r => _r.body as {Response?: DestinyPostGameCarnageReportData, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Report a player that you met in an activity that was engaging in ToS-violating activities. Both you and the offending player must have played in the activityId passed in. Please use this judiciously and only when you have strong suspicions of violation, pretty please.
   * @param activityId The ID of the activity where you ran into the brigand that you're reporting.
   * @return Look at the Response property for more information about the nature of this response
   */
  private reportOffensivePostGameCarnageReportPlayerResponse(activityId: number): __Observable<__StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/Destiny2/Stats/PostGameCarnageReport/${activityId}/Report/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Report a player that you met in an activity that was engaging in ToS-violating activities. Both you and the offending player must have played in the activityId passed in. Please use this judiciously and only when you have strong suspicions of violation, pretty please.
   * @param activityId The ID of the activity where you ran into the brigand that you're reporting.
   * @return Look at the Response property for more information about the nature of this response
   */
  reportOffensivePostGameCarnageReportPlayer(activityId: number): __Observable<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.reportOffensivePostGameCarnageReportPlayerResponse(activityId).pipe(
      __map(_r => _r.body as {Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Gets historical stats definitions.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getHistoricalStatsDefinitionResponse(): __Observable<__StrictHttpResponse<{Response?: {[key: string]: DestinyHistoricalStatsDefinition}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Destiny2/Stats/Definition/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: {[key: string]: DestinyHistoricalStatsDefinition}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets historical stats definitions.
   * @return Look at the Response property for more information about the nature of this response
   */
  getHistoricalStatsDefinition(): __Observable<{Response?: {[key: string]: DestinyHistoricalStatsDefinition}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getHistoricalStatsDefinitionResponse().pipe(
      __map(_r => _r.body as {Response?: {[key: string]: DestinyHistoricalStatsDefinition}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Gets leaderboards with the signed in user's friends and the supplied destinyMembershipId as the focus. PREVIEW: This endpoint is still in beta, and may experience rough edges. The schema is in final form, but there may be bugs that prevent desirable operation.
   * @param groupId Group ID of the clan whose leaderboards you wish to fetch.
   * @param statid ID of stat to return rather than returning all Leaderboard stats.
   * @param modes List of game modes for which to get leaderboards. See the documentation for DestinyActivityModeType for valid values, and pass in string representation, comma delimited.
   * @param maxtop Maximum number of top players to return. Use a large number to get entire leaderboard.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getClanLeaderboardsResponse(groupId: number,
    statid?: string,
    modes?: string,
    maxtop?: number): __Observable<__StrictHttpResponse<{Response?: {[key: string]: {[key: string]: DestinyLeaderboard}}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (statid != null) __params = __params.set('statid', statid.toString());
    if (modes != null) __params = __params.set('modes', modes.toString());
    if (maxtop != null) __params = __params.set('maxtop', maxtop.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Destiny2/Stats/Leaderboards/Clans/${groupId}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: {[key: string]: {[key: string]: DestinyLeaderboard}}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets leaderboards with the signed in user's friends and the supplied destinyMembershipId as the focus. PREVIEW: This endpoint is still in beta, and may experience rough edges. The schema is in final form, but there may be bugs that prevent desirable operation.
   * @param groupId Group ID of the clan whose leaderboards you wish to fetch.
   * @param statid ID of stat to return rather than returning all Leaderboard stats.
   * @param modes List of game modes for which to get leaderboards. See the documentation for DestinyActivityModeType for valid values, and pass in string representation, comma delimited.
   * @param maxtop Maximum number of top players to return. Use a large number to get entire leaderboard.
   * @return Look at the Response property for more information about the nature of this response
   */
  getClanLeaderboards(groupId: number,
    statid?: string,
    modes?: string,
    maxtop?: number): __Observable<{Response?: {[key: string]: {[key: string]: DestinyLeaderboard}}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getClanLeaderboardsResponse(groupId, statid, modes, maxtop).pipe(
      __map(_r => _r.body as {Response?: {[key: string]: {[key: string]: DestinyLeaderboard}}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Gets aggregated stats for a clan using the same categories as the clan leaderboards. PREVIEW: This endpoint is still in beta, and may experience rough edges. The schema is in final form, but there may be bugs that prevent desirable operation.
   * @param groupId Group ID of the clan whose leaderboards you wish to fetch.
   * @param modes List of game modes for which to get leaderboards. See the documentation for DestinyActivityModeType for valid values, and pass in string representation, comma delimited.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getClanAggregateStatsResponse(groupId: number,
    modes?: string): __Observable<__StrictHttpResponse<{Response?: Array<DestinyClanAggregateStat>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (modes != null) __params = __params.set('modes', modes.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Destiny2/Stats/AggregateClanStats/${groupId}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: Array<DestinyClanAggregateStat>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets aggregated stats for a clan using the same categories as the clan leaderboards. PREVIEW: This endpoint is still in beta, and may experience rough edges. The schema is in final form, but there may be bugs that prevent desirable operation.
   * @param groupId Group ID of the clan whose leaderboards you wish to fetch.
   * @param modes List of game modes for which to get leaderboards. See the documentation for DestinyActivityModeType for valid values, and pass in string representation, comma delimited.
   * @return Look at the Response property for more information about the nature of this response
   */
  getClanAggregateStats(groupId: number,
    modes?: string): __Observable<{Response?: Array<DestinyClanAggregateStat>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getClanAggregateStatsResponse(groupId, modes).pipe(
      __map(_r => _r.body as {Response?: Array<DestinyClanAggregateStat>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Gets leaderboards with the signed in user's friends and the supplied destinyMembershipId as the focus. PREVIEW: This endpoint has not yet been implemented. It is being returned for a preview of future functionality, and for public comment/suggestion/preparation.
   * @param membershipType A valid non-BungieNet membership type.
   * @param destinyMembershipId The Destiny membershipId of the user to retrieve.
   * @param statid ID of stat to return rather than returning all Leaderboard stats.
   * @param modes List of game modes for which to get leaderboards. See the documentation for DestinyActivityModeType for valid values, and pass in string representation, comma delimited.
   * @param maxtop Maximum number of top players to return. Use a large number to get entire leaderboard.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getLeaderboardsResponse(membershipType: BungieMembershipType,
    destinyMembershipId: number,
    statid?: string,
    modes?: string,
    maxtop?: number): __Observable<__StrictHttpResponse<{Response?: {[key: string]: {[key: string]: DestinyLeaderboard}}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    if (statid != null) __params = __params.set('statid', statid.toString());
    if (modes != null) __params = __params.set('modes', modes.toString());
    if (maxtop != null) __params = __params.set('maxtop', maxtop.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Destiny2/${membershipType}/Account/${destinyMembershipId}/Stats/Leaderboards/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: {[key: string]: {[key: string]: DestinyLeaderboard}}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets leaderboards with the signed in user's friends and the supplied destinyMembershipId as the focus. PREVIEW: This endpoint has not yet been implemented. It is being returned for a preview of future functionality, and for public comment/suggestion/preparation.
   * @param membershipType A valid non-BungieNet membership type.
   * @param destinyMembershipId The Destiny membershipId of the user to retrieve.
   * @param statid ID of stat to return rather than returning all Leaderboard stats.
   * @param modes List of game modes for which to get leaderboards. See the documentation for DestinyActivityModeType for valid values, and pass in string representation, comma delimited.
   * @param maxtop Maximum number of top players to return. Use a large number to get entire leaderboard.
   * @return Look at the Response property for more information about the nature of this response
   */
  getLeaderboards(membershipType: BungieMembershipType,
    destinyMembershipId: number,
    statid?: string,
    modes?: string,
    maxtop?: number): __Observable<{Response?: {[key: string]: {[key: string]: DestinyLeaderboard}}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getLeaderboardsResponse(membershipType, destinyMembershipId, statid, modes, maxtop).pipe(
      __map(_r => _r.body as {Response?: {[key: string]: {[key: string]: DestinyLeaderboard}}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Gets leaderboards with the signed in user's friends and the supplied destinyMembershipId as the focus. PREVIEW: This endpoint is still in beta, and may experience rough edges. The schema is in final form, but there may be bugs that prevent desirable operation.
   * @param membershipType A valid non-BungieNet membership type.
   * @param destinyMembershipId The Destiny membershipId of the user to retrieve.
   * @param characterId The specific character to build the leaderboard around for the provided Destiny Membership.
   * @param statid ID of stat to return rather than returning all Leaderboard stats.
   * @param modes List of game modes for which to get leaderboards. See the documentation for DestinyActivityModeType for valid values, and pass in string representation, comma delimited.
   * @param maxtop Maximum number of top players to return. Use a large number to get entire leaderboard.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getLeaderboardsForCharacterResponse(membershipType: BungieMembershipType,
    destinyMembershipId: number,
    characterId: number,
    statid?: string,
    modes?: string,
    maxtop?: number): __Observable<__StrictHttpResponse<{Response?: {[key: string]: {[key: string]: DestinyLeaderboard}}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;



    if (statid != null) __params = __params.set('statid', statid.toString());
    if (modes != null) __params = __params.set('modes', modes.toString());
    if (maxtop != null) __params = __params.set('maxtop', maxtop.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Destiny2/Stats/Leaderboards/${membershipType}/${destinyMembershipId}/${characterId}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: {[key: string]: {[key: string]: DestinyLeaderboard}}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets leaderboards with the signed in user's friends and the supplied destinyMembershipId as the focus. PREVIEW: This endpoint is still in beta, and may experience rough edges. The schema is in final form, but there may be bugs that prevent desirable operation.
   * @param membershipType A valid non-BungieNet membership type.
   * @param destinyMembershipId The Destiny membershipId of the user to retrieve.
   * @param characterId The specific character to build the leaderboard around for the provided Destiny Membership.
   * @param statid ID of stat to return rather than returning all Leaderboard stats.
   * @param modes List of game modes for which to get leaderboards. See the documentation for DestinyActivityModeType for valid values, and pass in string representation, comma delimited.
   * @param maxtop Maximum number of top players to return. Use a large number to get entire leaderboard.
   * @return Look at the Response property for more information about the nature of this response
   */
  getLeaderboardsForCharacter(membershipType: BungieMembershipType,
    destinyMembershipId: number,
    characterId: number,
    statid?: string,
    modes?: string,
    maxtop?: number): __Observable<{Response?: {[key: string]: {[key: string]: DestinyLeaderboard}}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getLeaderboardsForCharacterResponse(membershipType, destinyMembershipId, characterId, statid, modes, maxtop).pipe(
      __map(_r => _r.body as {Response?: {[key: string]: {[key: string]: DestinyLeaderboard}}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Gets a page list of Destiny items.
   * @param type The type of entity for whom you would like results. These correspond to the entity's definition contract name. For instance, if you are looking for items, this property should be 'DestinyInventoryItemDefinition'.
   * @param searchTerm The string to use when searching for Destiny entities.
   * @param page Page number to return, starting with 0.
   * @return The results of a search for Destiny content. This will be improved on over time, I've been doing some experimenting to see what might be useful.
   */
  private searchDestinyEntitiesResponse(type: string,
    searchTerm: string,
    page?: number): __Observable<__StrictHttpResponse<{Response?: DestinyEntitySearchResult, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    if (page != null) __params = __params.set('page', page.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Destiny2/Armory/Search/${type}/${searchTerm}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: DestinyEntitySearchResult, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets a page list of Destiny items.
   * @param type The type of entity for whom you would like results. These correspond to the entity's definition contract name. For instance, if you are looking for items, this property should be 'DestinyInventoryItemDefinition'.
   * @param searchTerm The string to use when searching for Destiny entities.
   * @param page Page number to return, starting with 0.
   * @return The results of a search for Destiny content. This will be improved on over time, I've been doing some experimenting to see what might be useful.
   */
  searchDestinyEntities(type: string,
    searchTerm: string,
    page?: number): __Observable<{Response?: DestinyEntitySearchResult, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.searchDestinyEntitiesResponse(type, searchTerm, page).pipe(
      __map(_r => _r.body as {Response?: DestinyEntitySearchResult, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Gets historical stats for indicated character.
   * @param membershipType A valid non-BungieNet membership type.
   * @param destinyMembershipId The Destiny membershipId of the user to retrieve.
   * @param characterId The id of the character to retrieve. You can omit this character ID or set it to 0 to get aggregate stats across all characters.
   * @param periodType Indicates a specific period type to return. Optional. May be: Daily, AllTime, or Activity
   * @param modes Game modes to return. See the documentation for DestinyActivityModeType for valid values, and pass in string representation, comma delimited.
   * @param groups Group of stats to include, otherwise only general stats are returned. Comma separated list is allowed. Values: General, Weapons, Medals
   * @param daystart First day to return when daily stats are requested. Use the format YYYY-MM-DD. Currently, we cannot allow more than 31 days of daily data to be requested in a single request.
   * @param dayend Last day to return when daily stats are requested. Use the format YYYY-MM-DD. Currently, we cannot allow more than 31 days of daily data to be requested in a single request.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getHistoricalStatsResponse(membershipType: BungieMembershipType,
    destinyMembershipId: number,
    characterId: number,
    periodType?: number,
    modes?: Array<DestinyActivityModeType>,
    groups?: Array<DestinyStatsGroupType>,
    daystart?: string,
    dayend?: string): __Observable<__StrictHttpResponse<{Response?: {[key: string]: DestinyHistoricalStatsByPeriod}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;



    if (periodType != null) __params = __params.set('periodType', periodType.toString());
    (modes || []).forEach(val => {if (val != null) __params = __params.append('modes', val.toString())});
    (groups || []).forEach(val => {if (val != null) __params = __params.append('groups', val.toString())});
    if (daystart != null) __params = __params.set('daystart', daystart.toString());
    if (dayend != null) __params = __params.set('dayend', dayend.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Destiny2/${membershipType}/Account/${destinyMembershipId}/Character/${characterId}/Stats/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: {[key: string]: DestinyHistoricalStatsByPeriod}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets historical stats for indicated character.
   * @param membershipType A valid non-BungieNet membership type.
   * @param destinyMembershipId The Destiny membershipId of the user to retrieve.
   * @param characterId The id of the character to retrieve. You can omit this character ID or set it to 0 to get aggregate stats across all characters.
   * @param periodType Indicates a specific period type to return. Optional. May be: Daily, AllTime, or Activity
   * @param modes Game modes to return. See the documentation for DestinyActivityModeType for valid values, and pass in string representation, comma delimited.
   * @param groups Group of stats to include, otherwise only general stats are returned. Comma separated list is allowed. Values: General, Weapons, Medals
   * @param daystart First day to return when daily stats are requested. Use the format YYYY-MM-DD. Currently, we cannot allow more than 31 days of daily data to be requested in a single request.
   * @param dayend Last day to return when daily stats are requested. Use the format YYYY-MM-DD. Currently, we cannot allow more than 31 days of daily data to be requested in a single request.
   * @return Look at the Response property for more information about the nature of this response
   */
  getHistoricalStats(membershipType: BungieMembershipType,
    destinyMembershipId: number,
    characterId: number,
    periodType?: number,
    modes?: Array<DestinyActivityModeType>,
    groups?: Array<DestinyStatsGroupType>,
    daystart?: string,
    dayend?: string): __Observable<{Response?: {[key: string]: DestinyHistoricalStatsByPeriod}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getHistoricalStatsResponse(membershipType, destinyMembershipId, characterId, periodType, modes, groups, daystart, dayend).pipe(
      __map(_r => _r.body as {Response?: {[key: string]: DestinyHistoricalStatsByPeriod}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Gets aggregate historical stats organized around each character for a given account.
   * @param membershipType A valid non-BungieNet membership type.
   * @param destinyMembershipId The Destiny membershipId of the user to retrieve.
   * @param groups Groups of stats to include, otherwise only general stats are returned. Comma separated list is allowed. Values: General, Weapons, Medals.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getHistoricalStatsForAccountResponse(membershipType: BungieMembershipType,
    destinyMembershipId: number,
    groups?: Array<DestinyStatsGroupType>): __Observable<__StrictHttpResponse<{Response?: DestinyHistoricalStatsAccountResult, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    (groups || []).forEach(val => {if (val != null) __params = __params.append('groups', val.toString())});
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Destiny2/${membershipType}/Account/${destinyMembershipId}/Stats/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: DestinyHistoricalStatsAccountResult, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets aggregate historical stats organized around each character for a given account.
   * @param membershipType A valid non-BungieNet membership type.
   * @param destinyMembershipId The Destiny membershipId of the user to retrieve.
   * @param groups Groups of stats to include, otherwise only general stats are returned. Comma separated list is allowed. Values: General, Weapons, Medals.
   * @return Look at the Response property for more information about the nature of this response
   */
  getHistoricalStatsForAccount(membershipType: BungieMembershipType,
    destinyMembershipId: number,
    groups?: Array<DestinyStatsGroupType>): __Observable<{Response?: DestinyHistoricalStatsAccountResult, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getHistoricalStatsForAccountResponse(membershipType, destinyMembershipId, groups).pipe(
      __map(_r => _r.body as {Response?: DestinyHistoricalStatsAccountResult, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Gets activity history stats for indicated character.
   * @param membershipType A valid non-BungieNet membership type.
   * @param destinyMembershipId The Destiny membershipId of the user to retrieve.
   * @param characterId The id of the character to retrieve.
   * @param page Page number to return, starting with 0.
   * @param mode A filter for the activity mode to be returned. None returns all activities. See the documentation for DestinyActivityModeType for valid values, and pass in string representation.
   * @param count Number of rows to return
   * @return Look at the Response property for more information about the nature of this response
   */
  private getActivityHistoryResponse(membershipType: BungieMembershipType,
    destinyMembershipId: number,
    characterId: number,
    page?: number,
    mode?: number,
    count?: number): __Observable<__StrictHttpResponse<{Response?: DestinyActivityHistoryResults, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;



    if (page != null) __params = __params.set('page', page.toString());
    if (mode != null) __params = __params.set('mode', mode.toString());
    if (count != null) __params = __params.set('count', count.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Destiny2/${membershipType}/Account/${destinyMembershipId}/Character/${characterId}/Stats/Activities/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: DestinyActivityHistoryResults, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets activity history stats for indicated character.
   * @param membershipType A valid non-BungieNet membership type.
   * @param destinyMembershipId The Destiny membershipId of the user to retrieve.
   * @param characterId The id of the character to retrieve.
   * @param page Page number to return, starting with 0.
   * @param mode A filter for the activity mode to be returned. None returns all activities. See the documentation for DestinyActivityModeType for valid values, and pass in string representation.
   * @param count Number of rows to return
   * @return Look at the Response property for more information about the nature of this response
   */
  getActivityHistory(membershipType: BungieMembershipType,
    destinyMembershipId: number,
    characterId: number,
    page?: number,
    mode?: number,
    count?: number): __Observable<{Response?: DestinyActivityHistoryResults, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getActivityHistoryResponse(membershipType, destinyMembershipId, characterId, page, mode, count).pipe(
      __map(_r => _r.body as {Response?: DestinyActivityHistoryResults, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Gets details about unique weapon usage, including all exotic weapons.
   * @param membershipType A valid non-BungieNet membership type.
   * @param destinyMembershipId The Destiny membershipId of the user to retrieve.
   * @param characterId The id of the character to retrieve.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getUniqueWeaponHistoryResponse(membershipType: BungieMembershipType,
    destinyMembershipId: number,
    characterId: number): __Observable<__StrictHttpResponse<{Response?: DestinyHistoricalWeaponStatsData, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;



    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Destiny2/${membershipType}/Account/${destinyMembershipId}/Character/${characterId}/Stats/UniqueWeapons/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: DestinyHistoricalWeaponStatsData, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets details about unique weapon usage, including all exotic weapons.
   * @param membershipType A valid non-BungieNet membership type.
   * @param destinyMembershipId The Destiny membershipId of the user to retrieve.
   * @param characterId The id of the character to retrieve.
   * @return Look at the Response property for more information about the nature of this response
   */
  getUniqueWeaponHistory(membershipType: BungieMembershipType,
    destinyMembershipId: number,
    characterId: number): __Observable<{Response?: DestinyHistoricalWeaponStatsData, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getUniqueWeaponHistoryResponse(membershipType, destinyMembershipId, characterId).pipe(
      __map(_r => _r.body as {Response?: DestinyHistoricalWeaponStatsData, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Gets all activities the character has participated in together with aggregate statistics for those activities.
   * @param membershipType A valid non-BungieNet membership type.
   * @param destinyMembershipId The Destiny membershipId of the user to retrieve.
   * @param characterId The specific character whose activities should be returned.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getDestinyAggregateActivityStatsResponse(membershipType: BungieMembershipType,
    destinyMembershipId: number,
    characterId: number): __Observable<__StrictHttpResponse<{Response?: DestinyAggregateActivityResults, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;



    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Destiny2/${membershipType}/Account/${destinyMembershipId}/Character/${characterId}/Stats/AggregateActivityStats/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: DestinyAggregateActivityResults, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets all activities the character has participated in together with aggregate statistics for those activities.
   * @param membershipType A valid non-BungieNet membership type.
   * @param destinyMembershipId The Destiny membershipId of the user to retrieve.
   * @param characterId The specific character whose activities should be returned.
   * @return Look at the Response property for more information about the nature of this response
   */
  getDestinyAggregateActivityStats(membershipType: BungieMembershipType,
    destinyMembershipId: number,
    characterId: number): __Observable<{Response?: DestinyAggregateActivityResults, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getDestinyAggregateActivityStatsResponse(membershipType, destinyMembershipId, characterId).pipe(
      __map(_r => _r.body as {Response?: DestinyAggregateActivityResults, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Gets custom localized content for the milestone of the given hash, if it exists.
   * @param milestoneHash The identifier for the milestone to be returned.
   * @return Represents localized, extended content related to Milestones. This is intentionally returned by a separate endpoint and not with Character-level Milestone data because we do not put localized data into standard Destiny responses, both for brevity of response and for caching purposes. If you really need this data, hit the Milestone Content endpoint.
   */
  private getPublicMilestoneContentResponse(milestoneHash: number): __Observable<__StrictHttpResponse<{Response?: DestinyMilestoneContent, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Destiny2/Milestones/${milestoneHash}/Content/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: DestinyMilestoneContent, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets custom localized content for the milestone of the given hash, if it exists.
   * @param milestoneHash The identifier for the milestone to be returned.
   * @return Represents localized, extended content related to Milestones. This is intentionally returned by a separate endpoint and not with Character-level Milestone data because we do not put localized data into standard Destiny responses, both for brevity of response and for caching purposes. If you really need this data, hit the Milestone Content endpoint.
   */
  getPublicMilestoneContent(milestoneHash: number): __Observable<{Response?: DestinyMilestoneContent, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getPublicMilestoneContentResponse(milestoneHash).pipe(
      __map(_r => _r.body as {Response?: DestinyMilestoneContent, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Gets public information about currently available Milestones.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getPublicMilestonesResponse(): __Observable<__StrictHttpResponse<{Response?: {[key: string]: DestinyPublicMilestone}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Destiny2/Milestones/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: {[key: string]: DestinyPublicMilestone}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets public information about currently available Milestones.
   * @return Look at the Response property for more information about the nature of this response
   */
  getPublicMilestones(): __Observable<{Response?: {[key: string]: DestinyPublicMilestone}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getPublicMilestonesResponse().pipe(
      __map(_r => _r.body as {Response?: {[key: string]: DestinyPublicMilestone}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Initialize a request to perform an advanced write action.
   * @return Look at the Response property for more information about the nature of this response
   */
  private awaInitializeRequestResponse(): __Observable<__StrictHttpResponse<{Response?: AwaInitializeResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/Destiny2/Awa/Initialize/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: AwaInitializeResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Initialize a request to perform an advanced write action.
   * @return Look at the Response property for more information about the nature of this response
   */
  awaInitializeRequest(): __Observable<{Response?: AwaInitializeResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.awaInitializeRequestResponse().pipe(
      __map(_r => _r.body as {Response?: AwaInitializeResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Provide the result of the user interaction. Called by the Bungie Destiny App to approve or reject a request.
   * @return Look at the Response property for more information about the nature of this response
   */
  private awaProvideAuthorizationResultResponse(): __Observable<__StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/Destiny2/Awa/AwaProvideAuthorizationResult/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Provide the result of the user interaction. Called by the Bungie Destiny App to approve or reject a request.
   * @return Look at the Response property for more information about the nature of this response
   */
  awaProvideAuthorizationResult(): __Observable<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.awaProvideAuthorizationResultResponse().pipe(
      __map(_r => _r.body as {Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Returns the action token if user approves the request.
   * @param correlationId The identifier for the advanced write action request.
   * @return Look at the Response property for more information about the nature of this response
   */
  private awaGetActionTokenResponse(correlationId: string): __Observable<__StrictHttpResponse<{Response?: AwaAuthorizationResult, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Destiny2/Awa/GetActionToken/${correlationId}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: AwaAuthorizationResult, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Returns the action token if user approves the request.
   * @param correlationId The identifier for the advanced write action request.
   * @return Look at the Response property for more information about the nature of this response
   */
  awaGetActionToken(correlationId: string): __Observable<{Response?: AwaAuthorizationResult, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.awaGetActionTokenResponse(correlationId).pipe(
      __map(_r => _r.body as {Response?: AwaAuthorizationResult, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }
}

module Destiny2Service {
}

export { Destiny2Service }
