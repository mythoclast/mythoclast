/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { BungieNetConfiguration as __Configuration } from '../bungie-net-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { GeneralUser } from '../models/user/general-user';
import { PlatformErrorCodes } from '../models/exceptions/platform-error-codes';
import { UserTheme } from '../models/config/user-theme';
import { UserMembershipData } from '../models/user/user-membership-data';
import { BungieMembershipType } from '../models/bungie-membership-type';
import { PublicPartnershipDetail } from '../models/partnerships/public-partnership-detail';

/**
 * user
 */
@Injectable({
  providedIn: 'root',
})
class UserService extends __BaseService {
  static readonly getBungieNetUserByIdPath = '/User/GetBungieNetUserById/{id}/';
  static readonly searchUsersPath = '/User/SearchUsers/';
  static readonly getAvailableThemesPath = '/User/GetAvailableThemes/';
  static readonly getMembershipDataByIdPath = '/User/GetMembershipsById/{membershipId}/{membershipType}/';
  static readonly getMembershipDataForCurrentUserPath = '/User/GetMembershipsForCurrentUser/';
  static readonly getPartnershipsPath = '/User/{membershipId}/Partnerships/';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Loads a bungienet user by membership id.
   * @param id The requested Bungie.net membership id.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getBungieNetUserByIdResponse(id: number): __Observable<__StrictHttpResponse<{Response?: GeneralUser, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/User/GetBungieNetUserById/${id}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: GeneralUser, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Loads a bungienet user by membership id.
   * @param id The requested Bungie.net membership id.
   * @return Look at the Response property for more information about the nature of this response
   */
  getBungieNetUserById(id: number): __Observable<{Response?: GeneralUser, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getBungieNetUserByIdResponse(id).pipe(
      __map(_r => _r.body as {Response?: GeneralUser, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Returns a list of possible users based on the search string
   * @param q The search string.
   * @return Look at the Response property for more information about the nature of this response
   */
  private searchUsersResponse(q?: string): __Observable<__StrictHttpResponse<{Response?: Array<GeneralUser>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (q != null) __params = __params.set('q', q.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/User/SearchUsers/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: Array<GeneralUser>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Returns a list of possible users based on the search string
   * @param q The search string.
   * @return Look at the Response property for more information about the nature of this response
   */
  searchUsers(q?: string): __Observable<{Response?: Array<GeneralUser>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.searchUsersResponse(q).pipe(
      __map(_r => _r.body as {Response?: Array<GeneralUser>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Returns a list of all available user themes.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getAvailableThemesResponse(): __Observable<__StrictHttpResponse<{Response?: Array<UserTheme>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/User/GetAvailableThemes/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: Array<UserTheme>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Returns a list of all available user themes.
   * @return Look at the Response property for more information about the nature of this response
   */
  getAvailableThemes(): __Observable<{Response?: Array<UserTheme>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getAvailableThemesResponse().pipe(
      __map(_r => _r.body as {Response?: Array<UserTheme>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Returns a list of accounts associated with the supplied membership ID and membership type. This will include all linked accounts (even when hidden) if supplied credentials permit it.
   * @param membershipType Type of the supplied membership ID.
   * @param membershipId The membership ID of the target user.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getMembershipDataByIdResponse(membershipType: BungieMembershipType,
    membershipId: number): __Observable<__StrictHttpResponse<{Response?: UserMembershipData, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/User/GetMembershipsById/${membershipId}/${membershipType}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: UserMembershipData, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Returns a list of accounts associated with the supplied membership ID and membership type. This will include all linked accounts (even when hidden) if supplied credentials permit it.
   * @param membershipType Type of the supplied membership ID.
   * @param membershipId The membership ID of the target user.
   * @return Look at the Response property for more information about the nature of this response
   */
  getMembershipDataById(membershipType: BungieMembershipType,
    membershipId: number): __Observable<{Response?: UserMembershipData, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getMembershipDataByIdResponse(membershipType, membershipId).pipe(
      __map(_r => _r.body as {Response?: UserMembershipData, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Returns a list of accounts associated with signed in user. This is useful for OAuth implementations that do not give you access to the token response.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getMembershipDataForCurrentUserResponse(): __Observable<__StrictHttpResponse<{Response?: UserMembershipData, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/User/GetMembershipsForCurrentUser/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: UserMembershipData, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Returns a list of accounts associated with signed in user. This is useful for OAuth implementations that do not give you access to the token response.
   * @return Look at the Response property for more information about the nature of this response
   */
  getMembershipDataForCurrentUser(): __Observable<{Response?: UserMembershipData, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getMembershipDataForCurrentUserResponse().pipe(
      __map(_r => _r.body as {Response?: UserMembershipData, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Returns a user's linked Partnerships.
   * @param membershipId The ID of the member for whom partnerships should be returned.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getPartnershipsResponse(membershipId: number): __Observable<__StrictHttpResponse<{Response?: Array<PublicPartnershipDetail>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/User/${membershipId}/Partnerships/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: Array<PublicPartnershipDetail>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Returns a user's linked Partnerships.
   * @param membershipId The ID of the member for whom partnerships should be returned.
   * @return Look at the Response property for more information about the nature of this response
   */
  getPartnerships(membershipId: number): __Observable<{Response?: Array<PublicPartnershipDetail>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getPartnershipsResponse(membershipId).pipe(
      __map(_r => _r.body as {Response?: Array<PublicPartnershipDetail>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }
}

module UserService {
}

export { UserService }
