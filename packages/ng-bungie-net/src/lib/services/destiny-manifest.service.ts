import { Injectable } from '@angular/core';
import { Destiny2Service } from '../services/destiny-2.service';
import { HttpClient } from '@angular/common/http';
import { Observable, forkJoin, of, from } from 'rxjs';
import { switchMap, map, tap } from 'rxjs/operators';
import * as localforagens from 'localforage';
import { DestinyManifestDatabase } from '../models/destiny/config/destiny-manifest-db.interface';

const localforage = localforagens;

@Injectable({
  providedIn: 'root'
})
export class DestinyManifestService {

  private manifest: DestinyManifestDatabase;

  constructor(private d2Service: Destiny2Service, private http: HttpClient) {
    localforage.config({
      name: 'Destiny 2 Manifest Data'
    });
  }

  public getManifest(): Observable<DestinyManifestDatabase> {
    if (this.manifest) {
      return of(this.manifest);
    }
    return from(localforage.ready()).pipe(
      switchMap(
        () => forkJoin([
          localforage.getItem('DESTINY_MANIFEST_VERSION'),
          localforage.getItem('DESTINY_MANIFEST') as Promise<DestinyManifestDatabase>,
          this.d2Service.getDestinyManifest()
        ])
      ),
      switchMap(([localVersion, localManifest, remoteManifest]) => {
        if (localVersion === remoteManifest.Response.version && null !== localManifest) {
          return of(localManifest);
        }
        return this.updateManifestFromRemote(remoteManifest.Response.jsonWorldContentPaths.en, remoteManifest.Response.version);
      })
    );
  }

  private updateManifestFromRemote(jsonWorldContentPath: string, version: string): Observable<DestinyManifestDatabase> {
    return this.http.get<DestinyManifestDatabase>(`https://www.bungie.net${jsonWorldContentPath}`).pipe(
      switchMap(manifest => forkJoin([
        localforage.setItem('DESTINY_MANIFEST', manifest),
        localforage.setItem('DESTINY_MANIFEST_VERSION', version)
      ])),
      map(([manifest, _]) => manifest),
      tap(manifest => this.manifest = manifest)
    );
  }
}
