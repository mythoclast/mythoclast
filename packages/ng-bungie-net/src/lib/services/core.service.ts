/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { BungieNetConfiguration as __Configuration } from '../bungie-net-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { PlatformErrorCodes } from '../models/exceptions/platform-error-codes';
import { CoreSettingsConfiguration } from '../models/common/models/core-settings-configuration';
import { GlobalAlert } from '../models/global-alert';

/**
 * core
 */
@Injectable({
  providedIn: 'root',
})
class CoreService extends __BaseService {
  static readonly getAvailableLocalesPath = '/GetAvailableLocales/';
  static readonly getCommonSettingsPath = '/Settings/';
  static readonly getGlobalAlertsPath = '/GlobalAlerts/';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * List of available localization cultures
   * @return Look at the Response property for more information about the nature of this response
   */
  private getAvailableLocalesResponse(): __Observable<__StrictHttpResponse<{Response?: {[key: string]: string}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/GetAvailableLocales/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: {[key: string]: string}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * List of available localization cultures
   * @return Look at the Response property for more information about the nature of this response
   */
  getAvailableLocales(): __Observable<{Response?: {[key: string]: string}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getAvailableLocalesResponse().pipe(
      __map(_r => _r.body as {Response?: {[key: string]: string}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Get the common settings used by the Bungie.Net environment.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getCommonSettingsResponse(): __Observable<__StrictHttpResponse<{Response?: CoreSettingsConfiguration, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Settings/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: CoreSettingsConfiguration, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Get the common settings used by the Bungie.Net environment.
   * @return Look at the Response property for more information about the nature of this response
   */
  getCommonSettings(): __Observable<{Response?: CoreSettingsConfiguration, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getCommonSettingsResponse().pipe(
      __map(_r => _r.body as {Response?: CoreSettingsConfiguration, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Gets any active global alert for display in the forum banners, help pages, etc. Usually used for DOC alerts.
   * @param includestreaming Determines whether Streaming Alerts are included in results
   * @return Look at the Response property for more information about the nature of this response
   */
  private getGlobalAlertsResponse(includestreaming?: boolean): __Observable<__StrictHttpResponse<{Response?: Array<GlobalAlert>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (includestreaming != null) __params = __params.set('includestreaming', includestreaming.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/GlobalAlerts/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: Array<GlobalAlert>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets any active global alert for display in the forum banners, help pages, etc. Usually used for DOC alerts.
   * @param includestreaming Determines whether Streaming Alerts are included in results
   * @return Look at the Response property for more information about the nature of this response
   */
  getGlobalAlerts(includestreaming?: boolean): __Observable<{Response?: Array<GlobalAlert>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getGlobalAlertsResponse(includestreaming).pipe(
      __map(_r => _r.body as {Response?: Array<GlobalAlert>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }
}

module CoreService {
}

export { CoreService }
