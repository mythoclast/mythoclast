/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { BungieNetConfiguration as __Configuration } from '../bungie-net-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { PostSearchResponse } from '../models/forum/post-search-response';
import { PlatformErrorCodes } from '../models/exceptions/platform-error-codes';
import { SearchResultOfCommunityLiveStatus } from '../models/search-result-of-community-live-status';
import { CommunityLiveStatus } from '../models/community/community-live-status';
import { BungieMembershipType } from '../models/bungie-membership-type';

/**
 * communitycontent
 */
@Injectable({
  providedIn: 'root',
})
class CommunityContentService extends __BaseService {
  static readonly getCommunityContentPath = '/CommunityContent/Get/{sort}/{mediaFilter}/{page}/';
  static readonly getCommunityLiveStatusesPath = '/CommunityContent/Live/All/{partnershipType}/{sort}/{page}/';
  static readonly getCommunityLiveStatusesForClanmatesPath = '/CommunityContent/Live/Clan/{partnershipType}/{sort}/{page}/';
  static readonly getCommunityLiveStatusesForFriendsPath = '/CommunityContent/Live/Friends/{partnershipType}/{sort}/{page}/';
  static readonly getFeaturedCommunityLiveStatusesPath = '/CommunityContent/Live/Featured/{partnershipType}/{sort}/{page}/';
  static readonly getStreamingStatusForMemberPath = '/CommunityContent/Live/Users/{partnershipType}/{membershipType}/{membershipId}/';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Returns community content.
   * @param sort The sort mode.
   * @param page Zero based page
   * @param mediaFilter The type of media to get
   * @return Look at the Response property for more information about the nature of this response
   */
  private getCommunityContentResponse(sort: number,
    page: number,
    mediaFilter: number): __Observable<__StrictHttpResponse<{Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;



    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/CommunityContent/Get/${sort}/${mediaFilter}/${page}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Returns community content.
   * @param sort The sort mode.
   * @param page Zero based page
   * @param mediaFilter The type of media to get
   * @return Look at the Response property for more information about the nature of this response
   */
  getCommunityContent(sort: number,
    page: number,
    mediaFilter: number): __Observable<{Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getCommunityContentResponse(sort, page, mediaFilter).pipe(
      __map(_r => _r.body as {Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Returns info about community members who are live streaming.
   * @param sort The sort mode.
   * @param partnershipType The type of partnership for which the status should be returned.
   * @param page Zero based page.
   * @param streamLocale The locale for streams you'd like to see. Don't pass this to fall back on your BNet locale. Pass 'ALL' to not filter by locale.
   * @param modeHash The hash of the Activity Mode for which streams should be retrieved. Don't pass it in or pass 0 to not filter by mode.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getCommunityLiveStatusesResponse(sort: number,
    partnershipType: number,
    page: number,
    streamLocale?: string,
    modeHash?: number): __Observable<__StrictHttpResponse<{Response?: SearchResultOfCommunityLiveStatus, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;



    if (streamLocale != null) __params = __params.set('streamLocale', streamLocale.toString());
    if (modeHash != null) __params = __params.set('modeHash', modeHash.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/CommunityContent/Live/All/${partnershipType}/${sort}/${page}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: SearchResultOfCommunityLiveStatus, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Returns info about community members who are live streaming.
   * @param sort The sort mode.
   * @param partnershipType The type of partnership for which the status should be returned.
   * @param page Zero based page.
   * @param streamLocale The locale for streams you'd like to see. Don't pass this to fall back on your BNet locale. Pass 'ALL' to not filter by locale.
   * @param modeHash The hash of the Activity Mode for which streams should be retrieved. Don't pass it in or pass 0 to not filter by mode.
   * @return Look at the Response property for more information about the nature of this response
   */
  getCommunityLiveStatuses(sort: number,
    partnershipType: number,
    page: number,
    streamLocale?: string,
    modeHash?: number): __Observable<{Response?: SearchResultOfCommunityLiveStatus, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getCommunityLiveStatusesResponse(sort, partnershipType, page, streamLocale, modeHash).pipe(
      __map(_r => _r.body as {Response?: SearchResultOfCommunityLiveStatus, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Returns info about community members who are live streaming in your clans.
   * @param sort The sort mode.
   * @param partnershipType The type of partnership for which the status should be returned.
   * @param page Zero based page.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getCommunityLiveStatusesForClanmatesResponse(sort: number,
    partnershipType: number,
    page: number): __Observable<__StrictHttpResponse<{Response?: SearchResultOfCommunityLiveStatus, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;



    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/CommunityContent/Live/Clan/${partnershipType}/${sort}/${page}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: SearchResultOfCommunityLiveStatus, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Returns info about community members who are live streaming in your clans.
   * @param sort The sort mode.
   * @param partnershipType The type of partnership for which the status should be returned.
   * @param page Zero based page.
   * @return Look at the Response property for more information about the nature of this response
   */
  getCommunityLiveStatusesForClanmates(sort: number,
    partnershipType: number,
    page: number): __Observable<{Response?: SearchResultOfCommunityLiveStatus, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getCommunityLiveStatusesForClanmatesResponse(sort, partnershipType, page).pipe(
      __map(_r => _r.body as {Response?: SearchResultOfCommunityLiveStatus, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Returns info about community members who are live streaming among your friends.
   * @param sort The sort mode.
   * @param partnershipType The type of partnership for which the status should be returned.
   * @param page Zero based page.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getCommunityLiveStatusesForFriendsResponse(sort: number,
    partnershipType: number,
    page: number): __Observable<__StrictHttpResponse<{Response?: SearchResultOfCommunityLiveStatus, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;



    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/CommunityContent/Live/Friends/${partnershipType}/${sort}/${page}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: SearchResultOfCommunityLiveStatus, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Returns info about community members who are live streaming among your friends.
   * @param sort The sort mode.
   * @param partnershipType The type of partnership for which the status should be returned.
   * @param page Zero based page.
   * @return Look at the Response property for more information about the nature of this response
   */
  getCommunityLiveStatusesForFriends(sort: number,
    partnershipType: number,
    page: number): __Observable<{Response?: SearchResultOfCommunityLiveStatus, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getCommunityLiveStatusesForFriendsResponse(sort, partnershipType, page).pipe(
      __map(_r => _r.body as {Response?: SearchResultOfCommunityLiveStatus, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Returns info about Featured live streams.
   * @param sort The sort mode.
   * @param partnershipType The type of partnership for which the status should be returned.
   * @param page Zero based page.
   * @param streamLocale The locale for streams you'd like to see. Don't pass this to fall back on your BNet locale. Pass 'ALL' to not filter by locale.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getFeaturedCommunityLiveStatusesResponse(sort: number,
    partnershipType: number,
    page: number,
    streamLocale?: string): __Observable<__StrictHttpResponse<{Response?: SearchResultOfCommunityLiveStatus, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;



    if (streamLocale != null) __params = __params.set('streamLocale', streamLocale.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/CommunityContent/Live/Featured/${partnershipType}/${sort}/${page}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: SearchResultOfCommunityLiveStatus, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Returns info about Featured live streams.
   * @param sort The sort mode.
   * @param partnershipType The type of partnership for which the status should be returned.
   * @param page Zero based page.
   * @param streamLocale The locale for streams you'd like to see. Don't pass this to fall back on your BNet locale. Pass 'ALL' to not filter by locale.
   * @return Look at the Response property for more information about the nature of this response
   */
  getFeaturedCommunityLiveStatuses(sort: number,
    partnershipType: number,
    page: number,
    streamLocale?: string): __Observable<{Response?: SearchResultOfCommunityLiveStatus, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getFeaturedCommunityLiveStatusesResponse(sort, partnershipType, page, streamLocale).pipe(
      __map(_r => _r.body as {Response?: SearchResultOfCommunityLiveStatus, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Gets the Live Streaming status of a particular Account and Membership Type.
   * @param partnershipType The type of partnership for which info will be extracted.
   * @param membershipType The type of account for which info will be extracted.
   * @param membershipId The membershipId related to that type.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getStreamingStatusForMemberResponse(partnershipType: number,
    membershipType: BungieMembershipType,
    membershipId: number): __Observable<__StrictHttpResponse<{Response?: CommunityLiveStatus, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;



    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/CommunityContent/Live/Users/${partnershipType}/${membershipType}/${membershipId}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: CommunityLiveStatus, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets the Live Streaming status of a particular Account and Membership Type.
   * @param partnershipType The type of partnership for which info will be extracted.
   * @param membershipType The type of account for which info will be extracted.
   * @param membershipId The membershipId related to that type.
   * @return Look at the Response property for more information about the nature of this response
   */
  getStreamingStatusForMember(partnershipType: number,
    membershipType: BungieMembershipType,
    membershipId: number): __Observable<{Response?: CommunityLiveStatus, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getStreamingStatusForMemberResponse(partnershipType, membershipType, membershipId).pipe(
      __map(_r => _r.body as {Response?: CommunityLiveStatus, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }
}

module CommunityContentService {
}

export { CommunityContentService }
