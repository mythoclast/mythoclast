/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { BungieNetConfiguration as __Configuration } from '../bungie-net-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { PlatformErrorCodes } from '../models/exceptions/platform-error-codes';
import { GroupTheme } from '../models/config/group-theme';
import { GroupV2Card } from '../models/groups-v2/group-v2card';
import { GroupSearchResponse } from '../models/groups-v2/group-search-response';
import { GroupResponse } from '../models/groups-v2/group-response';
import { GroupOptionalConversation } from '../models/groups-v2/group-optional-conversation';
import { SearchResultOfGroupMember } from '../models/search-result-of-group-member';
import { BungieMembershipType } from '../models/bungie-membership-type';
import { GroupMemberLeaveResult } from '../models/groups-v2/group-member-leave-result';
import { SearchResultOfGroupBan } from '../models/search-result-of-group-ban';
import { SearchResultOfGroupMemberApplication } from '../models/search-result-of-group-member-application';
import { EntityActionResult } from '../models/entities/entity-action-result';
import { GroupMembershipSearchResponse } from '../models/groups-v2/group-membership-search-response';
import { GroupPotentialMembershipSearchResponse } from '../models/groups-v2/group-potential-membership-search-response';
import { GroupApplicationResponse } from '../models/groups-v2/group-application-response';

/**
 * groupV2
 */
@Injectable({
  providedIn: 'root',
})
class GroupV2Service extends __BaseService {
  static readonly getAvailableAvatarsPath = '/GroupV2/GetAvailableAvatars/';
  static readonly getAvailableThemesPath = '/GroupV2/GetAvailableThemes/';
  static readonly getUserClanInviteSettingPath = '/GroupV2/GetUserClanInviteSetting/{mType}/';
  static readonly getRecommendedGroupsPath = '/GroupV2/Recommended/{groupType}/{createDateRange}/';
  static readonly groupSearchPath = '/GroupV2/Search/';
  static readonly getGroupPath = '/GroupV2/{groupId}/';
  static readonly getGroupByNamePath = '/GroupV2/Name/{groupName}/{groupType}/';
  static readonly getGroupByNameV2Path = '/GroupV2/NameV2/';
  static readonly getGroupOptionalConversationsPath = '/GroupV2/{groupId}/OptionalConversations/';
  static readonly editGroupPath = '/GroupV2/{groupId}/Edit/';
  static readonly editClanBannerPath = '/GroupV2/{groupId}/EditClanBanner/';
  static readonly editFounderOptionsPath = '/GroupV2/{groupId}/EditFounderOptions/';
  static readonly addOptionalConversationPath = '/GroupV2/{groupId}/OptionalConversations/Add/';
  static readonly editOptionalConversationPath = '/GroupV2/{groupId}/OptionalConversations/Edit/{conversationId}/';
  static readonly getMembersOfGroupPath = '/GroupV2/{groupId}/Members/';
  static readonly getAdminsAndFounderOfGroupPath = '/GroupV2/{groupId}/AdminsAndFounder/';
  static readonly editGroupMembershipPath = '/GroupV2/{groupId}/Members/{membershipType}/{membershipId}/SetMembershipType/{memberType}/';
  static readonly kickMemberPath = '/GroupV2/{groupId}/Members/{membershipType}/{membershipId}/Kick/';
  static readonly banMemberPath = '/GroupV2/{groupId}/Members/{membershipType}/{membershipId}/Ban/';
  static readonly unbanMemberPath = '/GroupV2/{groupId}/Members/{membershipType}/{membershipId}/Unban/';
  static readonly getBannedMembersOfGroupPath = '/GroupV2/{groupId}/Banned/';
  static readonly abdicateFoundershipPath = '/GroupV2/{groupId}/Admin/AbdicateFoundership/{membershipType}/{founderIdNew}/';
  static readonly getPendingMembershipsPath = '/GroupV2/{groupId}/Members/Pending/';
  static readonly getInvitedIndividualsPath = '/GroupV2/{groupId}/Members/InvitedIndividuals/';
  static readonly approveAllPendingPath = '/GroupV2/{groupId}/Members/ApproveAll/';
  static readonly denyAllPendingPath = '/GroupV2/{groupId}/Members/DenyAll/';
  static readonly approvePendingForListPath = '/GroupV2/{groupId}/Members/ApproveList/';
  static readonly approvePendingPath = '/GroupV2/{groupId}/Members/Approve/{membershipType}/{membershipId}/';
  static readonly denyPendingForListPath = '/GroupV2/{groupId}/Members/DenyList/';
  static readonly getGroupsForMemberPath = '/GroupV2/User/{membershipType}/{membershipId}/{filter}/{groupType}/';
  static readonly recoverGroupForFounderPath = '/GroupV2/Recover/{membershipType}/{membershipId}/{groupType}/';
  static readonly getPotentialGroupsForMemberPath = '/GroupV2/User/Potential/{membershipType}/{membershipId}/{filter}/{groupType}/';
  static readonly individualGroupInvitePath = '/GroupV2/{groupId}/Members/IndividualInvite/{membershipType}/{membershipId}/';
  static readonly individualGroupInviteCancelPath = '/GroupV2/{groupId}/Members/IndividualInviteCancel/{membershipType}/{membershipId}/';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Returns a list of all available group avatars for the signed-in user.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getAvailableAvatarsResponse(): __Observable<__StrictHttpResponse<{Response?: {[key: string]: string}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/GroupV2/GetAvailableAvatars/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: {[key: string]: string}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Returns a list of all available group avatars for the signed-in user.
   * @return Look at the Response property for more information about the nature of this response
   */
  getAvailableAvatars(): __Observable<{Response?: {[key: string]: string}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getAvailableAvatarsResponse().pipe(
      __map(_r => _r.body as {Response?: {[key: string]: string}, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Returns a list of all available group themes.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getAvailableThemesResponse(): __Observable<__StrictHttpResponse<{Response?: Array<GroupTheme>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/GroupV2/GetAvailableThemes/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: Array<GroupTheme>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Returns a list of all available group themes.
   * @return Look at the Response property for more information about the nature of this response
   */
  getAvailableThemes(): __Observable<{Response?: Array<GroupTheme>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getAvailableThemesResponse().pipe(
      __map(_r => _r.body as {Response?: Array<GroupTheme>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Gets the state of the user's clan invite preferences for a particular membership type - true if they wish to be invited to clans, false otherwise.
   * @param mType The Destiny membership type of the account we wish to access settings.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getUserClanInviteSettingResponse(mType: number): __Observable<__StrictHttpResponse<{Response?: boolean, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/GroupV2/GetUserClanInviteSetting/${mType}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: boolean, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets the state of the user's clan invite preferences for a particular membership type - true if they wish to be invited to clans, false otherwise.
   * @param mType The Destiny membership type of the account we wish to access settings.
   * @return Look at the Response property for more information about the nature of this response
   */
  getUserClanInviteSetting(mType: number): __Observable<{Response?: boolean, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getUserClanInviteSettingResponse(mType).pipe(
      __map(_r => _r.body as {Response?: boolean, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Gets groups recommended for you based on the groups to whom those you follow belong.
   * @param groupType Type of groups requested
   * @param createDateRange Requested range in which to pull recommended groups
   * @return Look at the Response property for more information about the nature of this response
   */
  private getRecommendedGroupsResponse(groupType: number,
    createDateRange: number): __Observable<__StrictHttpResponse<{Response?: Array<GroupV2Card>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/GroupV2/Recommended/${groupType}/${createDateRange}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: Array<GroupV2Card>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets groups recommended for you based on the groups to whom those you follow belong.
   * @param groupType Type of groups requested
   * @param createDateRange Requested range in which to pull recommended groups
   * @return Look at the Response property for more information about the nature of this response
   */
  getRecommendedGroups(groupType: number,
    createDateRange: number): __Observable<{Response?: Array<GroupV2Card>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getRecommendedGroupsResponse(groupType, createDateRange).pipe(
      __map(_r => _r.body as {Response?: Array<GroupV2Card>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Search for Groups.
   * @return Look at the Response property for more information about the nature of this response
   */
  private groupSearchResponse(): __Observable<__StrictHttpResponse<{Response?: GroupSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/GroupV2/Search/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: GroupSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Search for Groups.
   * @return Look at the Response property for more information about the nature of this response
   */
  groupSearch(): __Observable<{Response?: GroupSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.groupSearchResponse().pipe(
      __map(_r => _r.body as {Response?: GroupSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Get information about a specific group of the given ID.
   * @param groupId Requested group's id.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getGroupResponse(groupId: number): __Observable<__StrictHttpResponse<{Response?: GroupResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/GroupV2/${groupId}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: GroupResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Get information about a specific group of the given ID.
   * @param groupId Requested group's id.
   * @return Look at the Response property for more information about the nature of this response
   */
  getGroup(groupId: number): __Observable<{Response?: GroupResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getGroupResponse(groupId).pipe(
      __map(_r => _r.body as {Response?: GroupResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Get information about a specific group with the given name and type.
   * @param groupType Type of group to find.
   * @param groupName Exact name of the group to find.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getGroupByNameResponse(groupType: number,
    groupName: string): __Observable<__StrictHttpResponse<{Response?: GroupResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/GroupV2/Name/${groupName}/${groupType}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: GroupResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Get information about a specific group with the given name and type.
   * @param groupType Type of group to find.
   * @param groupName Exact name of the group to find.
   * @return Look at the Response property for more information about the nature of this response
   */
  getGroupByName(groupType: number,
    groupName: string): __Observable<{Response?: GroupResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getGroupByNameResponse(groupType, groupName).pipe(
      __map(_r => _r.body as {Response?: GroupResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Get information about a specific group with the given name and type. The POST version.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getGroupByNameV2Response(): __Observable<__StrictHttpResponse<{Response?: GroupResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/GroupV2/NameV2/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: GroupResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Get information about a specific group with the given name and type. The POST version.
   * @return Look at the Response property for more information about the nature of this response
   */
  getGroupByNameV2(): __Observable<{Response?: GroupResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getGroupByNameV2Response().pipe(
      __map(_r => _r.body as {Response?: GroupResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Gets a list of available optional conversation channels and their settings.
   * @param groupId Requested group's id.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getGroupOptionalConversationsResponse(groupId: number): __Observable<__StrictHttpResponse<{Response?: Array<GroupOptionalConversation>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/GroupV2/${groupId}/OptionalConversations/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: Array<GroupOptionalConversation>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets a list of available optional conversation channels and their settings.
   * @param groupId Requested group's id.
   * @return Look at the Response property for more information about the nature of this response
   */
  getGroupOptionalConversations(groupId: number): __Observable<{Response?: Array<GroupOptionalConversation>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getGroupOptionalConversationsResponse(groupId).pipe(
      __map(_r => _r.body as {Response?: Array<GroupOptionalConversation>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Edit an existing group. You must have suitable permissions in the group to perform this operation. This latest revision will only edit the fields you pass in - pass null for properties you want to leave unaltered.
   * @param groupId Group ID of the group to edit.
   * @return Look at the Response property for more information about the nature of this response
   */
  private editGroupResponse(groupId: number): __Observable<__StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/GroupV2/${groupId}/Edit/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Edit an existing group. You must have suitable permissions in the group to perform this operation. This latest revision will only edit the fields you pass in - pass null for properties you want to leave unaltered.
   * @param groupId Group ID of the group to edit.
   * @return Look at the Response property for more information about the nature of this response
   */
  editGroup(groupId: number): __Observable<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.editGroupResponse(groupId).pipe(
      __map(_r => _r.body as {Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Edit an existing group's clan banner. You must have suitable permissions in the group to perform this operation. All fields are required.
   * @param groupId Group ID of the group to edit.
   * @return Look at the Response property for more information about the nature of this response
   */
  private editClanBannerResponse(groupId: number): __Observable<__StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/GroupV2/${groupId}/EditClanBanner/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Edit an existing group's clan banner. You must have suitable permissions in the group to perform this operation. All fields are required.
   * @param groupId Group ID of the group to edit.
   * @return Look at the Response property for more information about the nature of this response
   */
  editClanBanner(groupId: number): __Observable<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.editClanBannerResponse(groupId).pipe(
      __map(_r => _r.body as {Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Edit group options only available to a founder. You must have suitable permissions in the group to perform this operation.
   * @param groupId Group ID of the group to edit.
   * @return Look at the Response property for more information about the nature of this response
   */
  private editFounderOptionsResponse(groupId: number): __Observable<__StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/GroupV2/${groupId}/EditFounderOptions/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Edit group options only available to a founder. You must have suitable permissions in the group to perform this operation.
   * @param groupId Group ID of the group to edit.
   * @return Look at the Response property for more information about the nature of this response
   */
  editFounderOptions(groupId: number): __Observable<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.editFounderOptionsResponse(groupId).pipe(
      __map(_r => _r.body as {Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Add a new optional conversation/chat channel. Requires admin permissions to the group.
   * @param groupId Group ID of the group to edit.
   * @return Look at the Response property for more information about the nature of this response
   */
  private addOptionalConversationResponse(groupId: number): __Observable<__StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/GroupV2/${groupId}/OptionalConversations/Add/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Add a new optional conversation/chat channel. Requires admin permissions to the group.
   * @param groupId Group ID of the group to edit.
   * @return Look at the Response property for more information about the nature of this response
   */
  addOptionalConversation(groupId: number): __Observable<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.addOptionalConversationResponse(groupId).pipe(
      __map(_r => _r.body as {Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Edit the settings of an optional conversation/chat channel. Requires admin permissions to the group.
   * @param groupId Group ID of the group to edit.
   * @param conversationId Conversation Id of the channel being edited.
   * @return Look at the Response property for more information about the nature of this response
   */
  private editOptionalConversationResponse(groupId: number,
    conversationId: number): __Observable<__StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/GroupV2/${groupId}/OptionalConversations/Edit/${conversationId}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Edit the settings of an optional conversation/chat channel. Requires admin permissions to the group.
   * @param groupId Group ID of the group to edit.
   * @param conversationId Conversation Id of the channel being edited.
   * @return Look at the Response property for more information about the nature of this response
   */
  editOptionalConversation(groupId: number,
    conversationId: number): __Observable<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.editOptionalConversationResponse(groupId, conversationId).pipe(
      __map(_r => _r.body as {Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Get the list of members in a given group.
   * @param groupId The ID of the group.
   * @param currentpage Page number (starting with 1). Each page has a fixed size of 50 items per page.
   * @param nameSearch The name fragment upon which a search should be executed for members with matching display or unique names.
   * @param memberType Filter out other member types. Use None for all members.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getMembersOfGroupResponse(groupId: number,
    currentpage: number,
    nameSearch?: string,
    memberType?: number): __Observable<__StrictHttpResponse<{Response?: SearchResultOfGroupMember, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    if (nameSearch != null) __params = __params.set('nameSearch', nameSearch.toString());
    if (memberType != null) __params = __params.set('memberType', memberType.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/GroupV2/${groupId}/Members/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: SearchResultOfGroupMember, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Get the list of members in a given group.
   * @param groupId The ID of the group.
   * @param currentpage Page number (starting with 1). Each page has a fixed size of 50 items per page.
   * @param nameSearch The name fragment upon which a search should be executed for members with matching display or unique names.
   * @param memberType Filter out other member types. Use None for all members.
   * @return Look at the Response property for more information about the nature of this response
   */
  getMembersOfGroup(groupId: number,
    currentpage: number,
    nameSearch?: string,
    memberType?: number): __Observable<{Response?: SearchResultOfGroupMember, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getMembersOfGroupResponse(groupId, currentpage, nameSearch, memberType).pipe(
      __map(_r => _r.body as {Response?: SearchResultOfGroupMember, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Get the list of members in a given group who are of admin level or higher.
   * @param groupId The ID of the group.
   * @param currentpage Page number (starting with 1). Each page has a fixed size of 50 items per page.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getAdminsAndFounderOfGroupResponse(groupId: number,
    currentpage: number): __Observable<__StrictHttpResponse<{Response?: SearchResultOfGroupMember, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/GroupV2/${groupId}/AdminsAndFounder/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: SearchResultOfGroupMember, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Get the list of members in a given group who are of admin level or higher.
   * @param groupId The ID of the group.
   * @param currentpage Page number (starting with 1). Each page has a fixed size of 50 items per page.
   * @return Look at the Response property for more information about the nature of this response
   */
  getAdminsAndFounderOfGroup(groupId: number,
    currentpage: number): __Observable<{Response?: SearchResultOfGroupMember, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getAdminsAndFounderOfGroupResponse(groupId, currentpage).pipe(
      __map(_r => _r.body as {Response?: SearchResultOfGroupMember, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Edit the membership type of a given member. You must have suitable permissions in the group to perform this operation.
   * @param membershipType Membership type of the provide membership ID.
   * @param membershipId Membership ID to modify.
   * @param memberType New membertype for the specified member.
   * @param groupId ID of the group to which the member belongs.
   * @return Look at the Response property for more information about the nature of this response
   */
  private editGroupMembershipResponse(membershipType: BungieMembershipType,
    membershipId: number,
    memberType: number,
    groupId: number): __Observable<__StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;




    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/GroupV2/${groupId}/Members/${membershipType}/${membershipId}/SetMembershipType/${memberType}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Edit the membership type of a given member. You must have suitable permissions in the group to perform this operation.
   * @param membershipType Membership type of the provide membership ID.
   * @param membershipId Membership ID to modify.
   * @param memberType New membertype for the specified member.
   * @param groupId ID of the group to which the member belongs.
   * @return Look at the Response property for more information about the nature of this response
   */
  editGroupMembership(membershipType: BungieMembershipType,
    membershipId: number,
    memberType: number,
    groupId: number): __Observable<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.editGroupMembershipResponse(membershipType, membershipId, memberType, groupId).pipe(
      __map(_r => _r.body as {Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Kick a member from the given group, forcing them to reapply if they wish to re-join the group. You must have suitable permissions in the group to perform this operation.
   * @param membershipType Membership type of the provided membership ID.
   * @param membershipId Membership ID to kick.
   * @param groupId Group ID to kick the user from.
   * @return Look at the Response property for more information about the nature of this response
   */
  private kickMemberResponse(membershipType: BungieMembershipType,
    membershipId: number,
    groupId: number): __Observable<__StrictHttpResponse<{Response?: GroupMemberLeaveResult, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;



    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/GroupV2/${groupId}/Members/${membershipType}/${membershipId}/Kick/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: GroupMemberLeaveResult, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Kick a member from the given group, forcing them to reapply if they wish to re-join the group. You must have suitable permissions in the group to perform this operation.
   * @param membershipType Membership type of the provided membership ID.
   * @param membershipId Membership ID to kick.
   * @param groupId Group ID to kick the user from.
   * @return Look at the Response property for more information about the nature of this response
   */
  kickMember(membershipType: BungieMembershipType,
    membershipId: number,
    groupId: number): __Observable<{Response?: GroupMemberLeaveResult, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.kickMemberResponse(membershipType, membershipId, groupId).pipe(
      __map(_r => _r.body as {Response?: GroupMemberLeaveResult, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Bans the requested member from the requested group for the specified period of time.
   * @param membershipType Membership type of the provided membership ID.
   * @param membershipId Membership ID of the member to ban from the group.
   * @param groupId Group ID that has the member to ban.
   * @return Look at the Response property for more information about the nature of this response
   */
  private banMemberResponse(membershipType: BungieMembershipType,
    membershipId: number,
    groupId: number): __Observable<__StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;



    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/GroupV2/${groupId}/Members/${membershipType}/${membershipId}/Ban/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Bans the requested member from the requested group for the specified period of time.
   * @param membershipType Membership type of the provided membership ID.
   * @param membershipId Membership ID of the member to ban from the group.
   * @param groupId Group ID that has the member to ban.
   * @return Look at the Response property for more information about the nature of this response
   */
  banMember(membershipType: BungieMembershipType,
    membershipId: number,
    groupId: number): __Observable<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.banMemberResponse(membershipType, membershipId, groupId).pipe(
      __map(_r => _r.body as {Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Unbans the requested member, allowing them to re-apply for membership.
   * @param membershipType Membership type of the provided membership ID.
   * @param membershipId Membership ID of the member to unban from the group
   * @param groupId
   * @return Look at the Response property for more information about the nature of this response
   */
  private unbanMemberResponse(membershipType: BungieMembershipType,
    membershipId: number,
    groupId: number): __Observable<__StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;



    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/GroupV2/${groupId}/Members/${membershipType}/${membershipId}/Unban/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Unbans the requested member, allowing them to re-apply for membership.
   * @param membershipType Membership type of the provided membership ID.
   * @param membershipId Membership ID of the member to unban from the group
   * @param groupId
   * @return Look at the Response property for more information about the nature of this response
   */
  unbanMember(membershipType: BungieMembershipType,
    membershipId: number,
    groupId: number): __Observable<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.unbanMemberResponse(membershipType, membershipId, groupId).pipe(
      __map(_r => _r.body as {Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Get the list of banned members in a given group. Only accessible to group Admins and above. Not applicable to all groups. Check group features.
   * @param groupId Group ID whose banned members you are fetching
   * @param currentpage Page number (starting with 1). Each page has a fixed size of 50 entries.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getBannedMembersOfGroupResponse(groupId: number,
    currentpage: number): __Observable<__StrictHttpResponse<{Response?: SearchResultOfGroupBan, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/GroupV2/${groupId}/Banned/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: SearchResultOfGroupBan, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Get the list of banned members in a given group. Only accessible to group Admins and above. Not applicable to all groups. Check group features.
   * @param groupId Group ID whose banned members you are fetching
   * @param currentpage Page number (starting with 1). Each page has a fixed size of 50 entries.
   * @return Look at the Response property for more information about the nature of this response
   */
  getBannedMembersOfGroup(groupId: number,
    currentpage: number): __Observable<{Response?: SearchResultOfGroupBan, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getBannedMembersOfGroupResponse(groupId, currentpage).pipe(
      __map(_r => _r.body as {Response?: SearchResultOfGroupBan, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * An administrative method to allow the founder of a group or clan to give up their position to another admin permanently.
   * @param membershipType Membership type of the provided founderIdNew.
   * @param groupId The target group id.
   * @param founderIdNew The new founder for this group. Must already be a group admin.
   * @return Look at the Response property for more information about the nature of this response
   */
  private abdicateFoundershipResponse(membershipType: BungieMembershipType,
    groupId: number,
    founderIdNew: number): __Observable<__StrictHttpResponse<{Response?: boolean, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;



    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/GroupV2/${groupId}/Admin/AbdicateFoundership/${membershipType}/${founderIdNew}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: boolean, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * An administrative method to allow the founder of a group or clan to give up their position to another admin permanently.
   * @param membershipType Membership type of the provided founderIdNew.
   * @param groupId The target group id.
   * @param founderIdNew The new founder for this group. Must already be a group admin.
   * @return Look at the Response property for more information about the nature of this response
   */
  abdicateFoundership(membershipType: BungieMembershipType,
    groupId: number,
    founderIdNew: number): __Observable<{Response?: boolean, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.abdicateFoundershipResponse(membershipType, groupId, founderIdNew).pipe(
      __map(_r => _r.body as {Response?: boolean, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Get the list of users who are awaiting a decision on their application to join a given group. Modified to include application info.
   * @param groupId ID of the group.
   * @param currentpage Page number (starting with 1). Each page has a fixed size of 50 items per page.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getPendingMembershipsResponse(groupId: number,
    currentpage: number): __Observable<__StrictHttpResponse<{Response?: SearchResultOfGroupMemberApplication, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/GroupV2/${groupId}/Members/Pending/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: SearchResultOfGroupMemberApplication, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Get the list of users who are awaiting a decision on their application to join a given group. Modified to include application info.
   * @param groupId ID of the group.
   * @param currentpage Page number (starting with 1). Each page has a fixed size of 50 items per page.
   * @return Look at the Response property for more information about the nature of this response
   */
  getPendingMemberships(groupId: number,
    currentpage: number): __Observable<{Response?: SearchResultOfGroupMemberApplication, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getPendingMembershipsResponse(groupId, currentpage).pipe(
      __map(_r => _r.body as {Response?: SearchResultOfGroupMemberApplication, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Get the list of users who have been invited into the group.
   * @param groupId ID of the group.
   * @param currentpage Page number (starting with 1). Each page has a fixed size of 50 items per page.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getInvitedIndividualsResponse(groupId: number,
    currentpage: number): __Observable<__StrictHttpResponse<{Response?: SearchResultOfGroupMemberApplication, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/GroupV2/${groupId}/Members/InvitedIndividuals/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: SearchResultOfGroupMemberApplication, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Get the list of users who have been invited into the group.
   * @param groupId ID of the group.
   * @param currentpage Page number (starting with 1). Each page has a fixed size of 50 items per page.
   * @return Look at the Response property for more information about the nature of this response
   */
  getInvitedIndividuals(groupId: number,
    currentpage: number): __Observable<{Response?: SearchResultOfGroupMemberApplication, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getInvitedIndividualsResponse(groupId, currentpage).pipe(
      __map(_r => _r.body as {Response?: SearchResultOfGroupMemberApplication, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Approve all of the pending users for the given group.
   * @param groupId ID of the group.
   * @return Look at the Response property for more information about the nature of this response
   */
  private approveAllPendingResponse(groupId: number): __Observable<__StrictHttpResponse<{Response?: Array<EntityActionResult>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/GroupV2/${groupId}/Members/ApproveAll/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: Array<EntityActionResult>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Approve all of the pending users for the given group.
   * @param groupId ID of the group.
   * @return Look at the Response property for more information about the nature of this response
   */
  approveAllPending(groupId: number): __Observable<{Response?: Array<EntityActionResult>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.approveAllPendingResponse(groupId).pipe(
      __map(_r => _r.body as {Response?: Array<EntityActionResult>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Deny all of the pending users for the given group.
   * @param groupId ID of the group.
   * @return Look at the Response property for more information about the nature of this response
   */
  private denyAllPendingResponse(groupId: number): __Observable<__StrictHttpResponse<{Response?: Array<EntityActionResult>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/GroupV2/${groupId}/Members/DenyAll/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: Array<EntityActionResult>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Deny all of the pending users for the given group.
   * @param groupId ID of the group.
   * @return Look at the Response property for more information about the nature of this response
   */
  denyAllPending(groupId: number): __Observable<{Response?: Array<EntityActionResult>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.denyAllPendingResponse(groupId).pipe(
      __map(_r => _r.body as {Response?: Array<EntityActionResult>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Approve all of the pending users for the given group.
   * @param groupId ID of the group.
   * @return Look at the Response property for more information about the nature of this response
   */
  private approvePendingForListResponse(groupId: number): __Observable<__StrictHttpResponse<{Response?: Array<EntityActionResult>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/GroupV2/${groupId}/Members/ApproveList/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: Array<EntityActionResult>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Approve all of the pending users for the given group.
   * @param groupId ID of the group.
   * @return Look at the Response property for more information about the nature of this response
   */
  approvePendingForList(groupId: number): __Observable<{Response?: Array<EntityActionResult>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.approvePendingForListResponse(groupId).pipe(
      __map(_r => _r.body as {Response?: Array<EntityActionResult>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Approve the given membershipId to join the group/clan as long as they have applied.
   * @param membershipType Membership type of the supplied membership ID.
   * @param membershipId The membership id being approved.
   * @param groupId ID of the group.
   * @return Look at the Response property for more information about the nature of this response
   */
  private approvePendingResponse(membershipType: BungieMembershipType,
    membershipId: number,
    groupId: number): __Observable<__StrictHttpResponse<{Response?: boolean, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;



    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/GroupV2/${groupId}/Members/Approve/${membershipType}/${membershipId}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: boolean, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Approve the given membershipId to join the group/clan as long as they have applied.
   * @param membershipType Membership type of the supplied membership ID.
   * @param membershipId The membership id being approved.
   * @param groupId ID of the group.
   * @return Look at the Response property for more information about the nature of this response
   */
  approvePending(membershipType: BungieMembershipType,
    membershipId: number,
    groupId: number): __Observable<{Response?: boolean, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.approvePendingResponse(membershipType, membershipId, groupId).pipe(
      __map(_r => _r.body as {Response?: boolean, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Deny all of the pending users for the given group that match the passed-in .
   * @param groupId ID of the group.
   * @return Look at the Response property for more information about the nature of this response
   */
  private denyPendingForListResponse(groupId: number): __Observable<__StrictHttpResponse<{Response?: Array<EntityActionResult>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/GroupV2/${groupId}/Members/DenyList/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: Array<EntityActionResult>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Deny all of the pending users for the given group that match the passed-in .
   * @param groupId ID of the group.
   * @return Look at the Response property for more information about the nature of this response
   */
  denyPendingForList(groupId: number): __Observable<{Response?: Array<EntityActionResult>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.denyPendingForListResponse(groupId).pipe(
      __map(_r => _r.body as {Response?: Array<EntityActionResult>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Get information about the groups that a given member has joined.
   * @param membershipType Membership type of the supplied membership ID.
   * @param membershipId Membership ID to for which to find founded groups.
   * @param groupType Type of group the supplied member founded.
   * @param filter Filter apply to list of joined groups.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getGroupsForMemberResponse(membershipType: BungieMembershipType,
    membershipId: number,
    groupType: number,
    filter: number): __Observable<__StrictHttpResponse<{Response?: GroupMembershipSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;




    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/GroupV2/User/${membershipType}/${membershipId}/${filter}/${groupType}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: GroupMembershipSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Get information about the groups that a given member has joined.
   * @param membershipType Membership type of the supplied membership ID.
   * @param membershipId Membership ID to for which to find founded groups.
   * @param groupType Type of group the supplied member founded.
   * @param filter Filter apply to list of joined groups.
   * @return Look at the Response property for more information about the nature of this response
   */
  getGroupsForMember(membershipType: BungieMembershipType,
    membershipId: number,
    groupType: number,
    filter: number): __Observable<{Response?: GroupMembershipSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getGroupsForMemberResponse(membershipType, membershipId, groupType, filter).pipe(
      __map(_r => _r.body as {Response?: GroupMembershipSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Allows a founder to manually recover a group they can see in game but not on bungie.net
   * @param membershipType Membership type of the supplied membership ID.
   * @param membershipId Membership ID to for which to find founded groups.
   * @param groupType Type of group the supplied member founded.
   * @return Look at the Response property for more information about the nature of this response
   */
  private recoverGroupForFounderResponse(membershipType: BungieMembershipType,
    membershipId: number,
    groupType: number): __Observable<__StrictHttpResponse<{Response?: GroupMembershipSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;



    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/GroupV2/Recover/${membershipType}/${membershipId}/${groupType}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: GroupMembershipSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Allows a founder to manually recover a group they can see in game but not on bungie.net
   * @param membershipType Membership type of the supplied membership ID.
   * @param membershipId Membership ID to for which to find founded groups.
   * @param groupType Type of group the supplied member founded.
   * @return Look at the Response property for more information about the nature of this response
   */
  recoverGroupForFounder(membershipType: BungieMembershipType,
    membershipId: number,
    groupType: number): __Observable<{Response?: GroupMembershipSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.recoverGroupForFounderResponse(membershipType, membershipId, groupType).pipe(
      __map(_r => _r.body as {Response?: GroupMembershipSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Get information about the groups that a given member has applied to or been invited to.
   * @param membershipType Membership type of the supplied membership ID.
   * @param membershipId Membership ID to for which to find applied groups.
   * @param groupType Type of group the supplied member applied.
   * @param filter Filter apply to list of potential joined groups.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getPotentialGroupsForMemberResponse(membershipType: BungieMembershipType,
    membershipId: number,
    groupType: number,
    filter: number): __Observable<__StrictHttpResponse<{Response?: GroupPotentialMembershipSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;




    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/GroupV2/User/Potential/${membershipType}/${membershipId}/${filter}/${groupType}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: GroupPotentialMembershipSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Get information about the groups that a given member has applied to or been invited to.
   * @param membershipType Membership type of the supplied membership ID.
   * @param membershipId Membership ID to for which to find applied groups.
   * @param groupType Type of group the supplied member applied.
   * @param filter Filter apply to list of potential joined groups.
   * @return Look at the Response property for more information about the nature of this response
   */
  getPotentialGroupsForMember(membershipType: BungieMembershipType,
    membershipId: number,
    groupType: number,
    filter: number): __Observable<{Response?: GroupPotentialMembershipSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getPotentialGroupsForMemberResponse(membershipType, membershipId, groupType, filter).pipe(
      __map(_r => _r.body as {Response?: GroupPotentialMembershipSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Invite a user to join this group.
   * @param membershipType MembershipType of the account being invited.
   * @param membershipId Membership id of the account being invited.
   * @param groupId ID of the group you would like to join.
   * @return Look at the Response property for more information about the nature of this response
   */
  private individualGroupInviteResponse(membershipType: BungieMembershipType,
    membershipId: number,
    groupId: number): __Observable<__StrictHttpResponse<{Response?: GroupApplicationResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;



    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/GroupV2/${groupId}/Members/IndividualInvite/${membershipType}/${membershipId}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: GroupApplicationResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Invite a user to join this group.
   * @param membershipType MembershipType of the account being invited.
   * @param membershipId Membership id of the account being invited.
   * @param groupId ID of the group you would like to join.
   * @return Look at the Response property for more information about the nature of this response
   */
  individualGroupInvite(membershipType: BungieMembershipType,
    membershipId: number,
    groupId: number): __Observable<{Response?: GroupApplicationResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.individualGroupInviteResponse(membershipType, membershipId, groupId).pipe(
      __map(_r => _r.body as {Response?: GroupApplicationResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Cancels a pending invitation to join a group.
   * @param membershipType MembershipType of the account being cancelled.
   * @param membershipId Membership id of the account being cancelled.
   * @param groupId ID of the group you would like to join.
   * @return Look at the Response property for more information about the nature of this response
   */
  private individualGroupInviteCancelResponse(membershipType: BungieMembershipType,
    membershipId: number,
    groupId: number): __Observable<__StrictHttpResponse<{Response?: GroupApplicationResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;



    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/GroupV2/${groupId}/Members/IndividualInviteCancel/${membershipType}/${membershipId}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: GroupApplicationResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Cancels a pending invitation to join a group.
   * @param membershipType MembershipType of the account being cancelled.
   * @param membershipId Membership id of the account being cancelled.
   * @param groupId ID of the group you would like to join.
   * @return Look at the Response property for more information about the nature of this response
   */
  individualGroupInviteCancel(membershipType: BungieMembershipType,
    membershipId: number,
    groupId: number): __Observable<{Response?: GroupApplicationResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.individualGroupInviteCancelResponse(membershipType, membershipId, groupId).pipe(
      __map(_r => _r.body as {Response?: GroupApplicationResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }
}

module GroupV2Service {
}

export { GroupV2Service }
