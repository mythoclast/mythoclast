/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { BungieNetConfiguration as __Configuration } from '../bungie-net-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { PostSearchResponse } from '../models/forum/post-search-response';
import { PlatformErrorCodes } from '../models/exceptions/platform-error-codes';
import { TagResponse } from '../models/tags/models/contracts/tag-response';
import { ForumRecruitmentDetail } from '../models/forum/forum-recruitment-detail';

/**
 * forum
 */
@Injectable({
  providedIn: 'root',
})
class ForumService extends __BaseService {
  static readonly getTopicsPagedPath = '/Forum/GetTopicsPaged/{page}/{pageSize}/{group}/{sort}/{quickDate}/{categoryFilter}/';
  static readonly getCoreTopicsPagedPath = '/Forum/GetCoreTopicsPaged/{page}/{sort}/{quickDate}/{categoryFilter}/';
  static readonly getPostsThreadedPagedPath = '/Forum/GetPostsThreadedPaged/{parentPostId}/{page}/{pageSize}/{replySize}/{getParentPost}/{rootThreadMode}/{sortMode}/';
  static readonly getPostsThreadedPagedFromChildPath = '/Forum/GetPostsThreadedPagedFromChild/{childPostId}/{page}/{pageSize}/{replySize}/{rootThreadMode}/{sortMode}/';
  static readonly getPostAndParentPath = '/Forum/GetPostAndParent/{childPostId}/';
  static readonly getPostAndParentAwaitingApprovalPath = '/Forum/GetPostAndParentAwaitingApproval/{childPostId}/';
  static readonly getTopicForContentPath = '/Forum/GetTopicForContent/{contentId}/';
  static readonly getForumTagSuggestionsPath = '/Forum/GetForumTagSuggestions/';
  static readonly getPollPath = '/Forum/Poll/{topicId}/';
  static readonly getRecruitmentThreadSummariesPath = '/Forum/Recruit/Summaries/';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get topics from any forum.
   * @param sort The sort mode.
   * @param quickDate A date filter.
   * @param pageSize Unused
   * @param page Zero paged page number
   * @param group The group, if any.
   * @param categoryFilter A category filter
   * @param tagstring The tags to search, if any.
   * @param locales Comma seperated list of locales posts must match to return in the result list. Default 'en'
   * @return Look at the Response property for more information about the nature of this response
   */
  private getTopicsPagedResponse(sort: number,
    quickDate: number,
    pageSize: number,
    page: number,
    group: number,
    categoryFilter: number,
    tagstring?: string,
    locales?: string): __Observable<__StrictHttpResponse<{Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;






    if (tagstring != null) __params = __params.set('tagstring', tagstring.toString());
    if (locales != null) __params = __params.set('locales', locales.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Forum/GetTopicsPaged/${page}/${pageSize}/${group}/${sort}/${quickDate}/${categoryFilter}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Get topics from any forum.
   * @param sort The sort mode.
   * @param quickDate A date filter.
   * @param pageSize Unused
   * @param page Zero paged page number
   * @param group The group, if any.
   * @param categoryFilter A category filter
   * @param tagstring The tags to search, if any.
   * @param locales Comma seperated list of locales posts must match to return in the result list. Default 'en'
   * @return Look at the Response property for more information about the nature of this response
   */
  getTopicsPaged(sort: number,
    quickDate: number,
    pageSize: number,
    page: number,
    group: number,
    categoryFilter: number,
    tagstring?: string,
    locales?: string): __Observable<{Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getTopicsPagedResponse(sort, quickDate, pageSize, page, group, categoryFilter, tagstring, locales).pipe(
      __map(_r => _r.body as {Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Gets a listing of all topics marked as part of the core group.
   * @param sort The sort mode.
   * @param quickDate The date filter.
   * @param page Zero base page
   * @param categoryFilter The category filter.
   * @param locales Comma seperated list of locales posts must match to return in the result list. Default 'en'
   * @return Look at the Response property for more information about the nature of this response
   */
  private getCoreTopicsPagedResponse(sort: number,
    quickDate: number,
    page: number,
    categoryFilter: number,
    locales?: string): __Observable<__StrictHttpResponse<{Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;




    if (locales != null) __params = __params.set('locales', locales.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Forum/GetCoreTopicsPaged/${page}/${sort}/${quickDate}/${categoryFilter}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets a listing of all topics marked as part of the core group.
   * @param sort The sort mode.
   * @param quickDate The date filter.
   * @param page Zero base page
   * @param categoryFilter The category filter.
   * @param locales Comma seperated list of locales posts must match to return in the result list. Default 'en'
   * @return Look at the Response property for more information about the nature of this response
   */
  getCoreTopicsPaged(sort: number,
    quickDate: number,
    page: number,
    categoryFilter: number,
    locales?: string): __Observable<{Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getCoreTopicsPagedResponse(sort, quickDate, page, categoryFilter, locales).pipe(
      __map(_r => _r.body as {Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Returns a thread of posts at the given parent, optionally returning replies to those posts as well as the original parent.
   * @param sortMode
   * @param rootThreadMode
   * @param replySize
   * @param parentPostId
   * @param pageSize
   * @param page
   * @param getParentPost
   * @param showbanned If this value is not null or empty, banned posts are requested to be returned
   * @return Look at the Response property for more information about the nature of this response
   */
  private getPostsThreadedPagedResponse(sortMode: number,
    rootThreadMode: boolean,
    replySize: number,
    parentPostId: number,
    pageSize: number,
    page: number,
    getParentPost: boolean,
    showbanned?: string): __Observable<__StrictHttpResponse<{Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;







    if (showbanned != null) __params = __params.set('showbanned', showbanned.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Forum/GetPostsThreadedPaged/${parentPostId}/${page}/${pageSize}/${replySize}/${getParentPost}/${rootThreadMode}/${sortMode}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Returns a thread of posts at the given parent, optionally returning replies to those posts as well as the original parent.
   * @param sortMode
   * @param rootThreadMode
   * @param replySize
   * @param parentPostId
   * @param pageSize
   * @param page
   * @param getParentPost
   * @param showbanned If this value is not null or empty, banned posts are requested to be returned
   * @return Look at the Response property for more information about the nature of this response
   */
  getPostsThreadedPaged(sortMode: number,
    rootThreadMode: boolean,
    replySize: number,
    parentPostId: number,
    pageSize: number,
    page: number,
    getParentPost: boolean,
    showbanned?: string): __Observable<{Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getPostsThreadedPagedResponse(sortMode, rootThreadMode, replySize, parentPostId, pageSize, page, getParentPost, showbanned).pipe(
      __map(_r => _r.body as {Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Returns a thread of posts starting at the topicId of the input childPostId, optionally returning replies to those posts as well as the original parent.
   * @param sortMode
   * @param rootThreadMode
   * @param replySize
   * @param pageSize
   * @param page
   * @param childPostId
   * @param showbanned If this value is not null or empty, banned posts are requested to be returned
   * @return Look at the Response property for more information about the nature of this response
   */
  private getPostsThreadedPagedFromChildResponse(sortMode: number,
    rootThreadMode: boolean,
    replySize: number,
    pageSize: number,
    page: number,
    childPostId: number,
    showbanned?: string): __Observable<__StrictHttpResponse<{Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;






    if (showbanned != null) __params = __params.set('showbanned', showbanned.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Forum/GetPostsThreadedPagedFromChild/${childPostId}/${page}/${pageSize}/${replySize}/${rootThreadMode}/${sortMode}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Returns a thread of posts starting at the topicId of the input childPostId, optionally returning replies to those posts as well as the original parent.
   * @param sortMode
   * @param rootThreadMode
   * @param replySize
   * @param pageSize
   * @param page
   * @param childPostId
   * @param showbanned If this value is not null or empty, banned posts are requested to be returned
   * @return Look at the Response property for more information about the nature of this response
   */
  getPostsThreadedPagedFromChild(sortMode: number,
    rootThreadMode: boolean,
    replySize: number,
    pageSize: number,
    page: number,
    childPostId: number,
    showbanned?: string): __Observable<{Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getPostsThreadedPagedFromChildResponse(sortMode, rootThreadMode, replySize, pageSize, page, childPostId, showbanned).pipe(
      __map(_r => _r.body as {Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Returns the post specified and its immediate parent.
   * @param childPostId
   * @param showbanned If this value is not null or empty, banned posts are requested to be returned
   * @return Look at the Response property for more information about the nature of this response
   */
  private getPostAndParentResponse(childPostId: number,
    showbanned?: string): __Observable<__StrictHttpResponse<{Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (showbanned != null) __params = __params.set('showbanned', showbanned.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Forum/GetPostAndParent/${childPostId}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Returns the post specified and its immediate parent.
   * @param childPostId
   * @param showbanned If this value is not null or empty, banned posts are requested to be returned
   * @return Look at the Response property for more information about the nature of this response
   */
  getPostAndParent(childPostId: number,
    showbanned?: string): __Observable<{Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getPostAndParentResponse(childPostId, showbanned).pipe(
      __map(_r => _r.body as {Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Returns the post specified and its immediate parent of posts that are awaiting approval.
   * @param childPostId
   * @param showbanned If this value is not null or empty, banned posts are requested to be returned
   * @return Look at the Response property for more information about the nature of this response
   */
  private getPostAndParentAwaitingApprovalResponse(childPostId: number,
    showbanned?: string): __Observable<__StrictHttpResponse<{Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (showbanned != null) __params = __params.set('showbanned', showbanned.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Forum/GetPostAndParentAwaitingApproval/${childPostId}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Returns the post specified and its immediate parent of posts that are awaiting approval.
   * @param childPostId
   * @param showbanned If this value is not null or empty, banned posts are requested to be returned
   * @return Look at the Response property for more information about the nature of this response
   */
  getPostAndParentAwaitingApproval(childPostId: number,
    showbanned?: string): __Observable<{Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getPostAndParentAwaitingApprovalResponse(childPostId, showbanned).pipe(
      __map(_r => _r.body as {Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Gets the post Id for the given content item's comments, if it exists.
   * @param contentId
   * @return Look at the Response property for more information about the nature of this response
   */
  private getTopicForContentResponse(contentId: number): __Observable<__StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Forum/GetTopicForContent/${contentId}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets the post Id for the given content item's comments, if it exists.
   * @param contentId
   * @return Look at the Response property for more information about the nature of this response
   */
  getTopicForContent(contentId: number): __Observable<{Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getTopicForContentResponse(contentId).pipe(
      __map(_r => _r.body as {Response?: number, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Gets tag suggestions based on partial text entry, matching them with other tags previously used in the forums.
   * @param partialtag The partial tag input to generate suggestions from.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getForumTagSuggestionsResponse(partialtag?: string): __Observable<__StrictHttpResponse<{Response?: Array<TagResponse>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (partialtag != null) __params = __params.set('partialtag', partialtag.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Forum/GetForumTagSuggestions/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: Array<TagResponse>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets tag suggestions based on partial text entry, matching them with other tags previously used in the forums.
   * @param partialtag The partial tag input to generate suggestions from.
   * @return Look at the Response property for more information about the nature of this response
   */
  getForumTagSuggestions(partialtag?: string): __Observable<{Response?: Array<TagResponse>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getForumTagSuggestionsResponse(partialtag).pipe(
      __map(_r => _r.body as {Response?: Array<TagResponse>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Gets the specified forum poll.
   * @param topicId The post id of the topic that has the poll.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getPollResponse(topicId: number): __Observable<__StrictHttpResponse<{Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/Forum/Poll/${topicId}/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Gets the specified forum poll.
   * @param topicId The post id of the topic that has the poll.
   * @return Look at the Response property for more information about the nature of this response
   */
  getPoll(topicId: number): __Observable<{Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getPollResponse(topicId).pipe(
      __map(_r => _r.body as {Response?: PostSearchResponse, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }

  /**
   * Allows the caller to get a list of to 25 recruitment thread summary information objects.
   * @return Look at the Response property for more information about the nature of this response
   */
  private getRecruitmentThreadSummariesResponse(): __Observable<__StrictHttpResponse<{Response?: Array<ForumRecruitmentDetail>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/Forum/Recruit/Summaries/`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{Response?: Array<ForumRecruitmentDetail>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}>;
      })
    );
  }

  /**
   * Allows the caller to get a list of to 25 recruitment thread summary information objects.
   * @return Look at the Response property for more information about the nature of this response
   */
  getRecruitmentThreadSummaries(): __Observable<{Response?: Array<ForumRecruitmentDetail>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string}> {
    return this.getRecruitmentThreadSummariesResponse().pipe(
      __map(_r => _r.body as {Response?: Array<ForumRecruitmentDetail>, ErrorCode?: PlatformErrorCodes, ThrottleSeconds?: number, ErrorStatus?: string, Message?: string, MessageData?: {[key: string]: string}, DetailedErrorTrace?: string})
    );
  }
}

module ForumService {
}

export { ForumService }
