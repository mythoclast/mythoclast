import { NgModule, ModuleWithProviders } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BungieNetConfiguration, BungieKeys } from './bungie-net-configuration';

import { AppService } from './services/app.service';
import { UserService } from './services/user.service';
import { ContentService } from './services/content.service';
import { ForumService } from './services/forum.service';
import { GroupV2Service } from './services/group-v2.service';
import { Destiny2Service } from './services/destiny-2.service';
import { CommunityContentService } from './services/community-content.service';
import { TrendingService } from './services/trending.service';
import { FireteamService } from './services/fireteam.service';
import { CoreService } from './services/core.service';
import { DestinyManifestService } from './services/destiny-manifest.service';
import { BungieNetAuthenticatorService } from './services/bungie-net-authenticator.service';
import { OAuthStateService } from './services/oauth-state.service';
import { BungieNetInterceptor } from './interceptors/bungie-net-interceptor';
import { StorageModule, StorageConfig } from '@ngx-pwa/local-storage';
import { NgxBroadcastChannelModule } from '@mythoclast/ngx-broadcast-channel';

/**
 * Provider for all BungieNet services, plus BungieNetConfiguration
 */
@NgModule({
  imports: [
    HttpClientModule,
    NgxBroadcastChannelModule
  ],
  exports: [
    HttpClientModule
  ],
  declarations: [],
  providers: [
    AppService,
    UserService,
    ContentService,
    ForumService,
    GroupV2Service,
    Destiny2Service,
    CommunityContentService,
    TrendingService,
    FireteamService,
    CoreService,
    DestinyManifestService,
    BungieNetAuthenticatorService,
    OAuthStateService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: BungieNetInterceptor,
      multi: true
    }
  ],
})
export class BungieNetModule {
  static forRoot(
    bungieKeys: BungieKeys = {
      apiKey: null,
      oauthClientId: null,
      oauthClientSecret: null
    },
    storageConfig: StorageConfig = {
      LSPrefix: 'ngx-bungie-net',
      IDBDBName: 'ngx-bungie-net',
      IDBStoreName: 'session',
      IDBNoWrap: true
    }): ModuleWithProviders {
    return {
      ngModule: BungieNetModule,
      providers: [
        {
          provide: BungieNetConfiguration,
          useValue: new BungieNetConfiguration(bungieKeys)
        },
        {
          provide: StorageModule,
          useValue: StorageModule.forRoot(storageConfig)
        }
      ]
    };
  }
}
