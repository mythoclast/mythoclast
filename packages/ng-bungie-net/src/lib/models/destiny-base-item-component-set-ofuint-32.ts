/* tslint:disable */
import { DictionaryComponentResponseOfuint32AndDestinyItemObjectivesComponent } from './dictionary-component-response-ofuint-32and-destiny-item-objectives-component';
export interface DestinyBaseItemComponentSetOfuint32 {
  objectives?: DictionaryComponentResponseOfuint32AndDestinyItemObjectivesComponent;
}
