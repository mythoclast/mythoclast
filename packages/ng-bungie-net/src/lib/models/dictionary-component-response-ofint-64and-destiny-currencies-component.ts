/* tslint:disable */
import { DestinyCurrenciesComponent } from './destiny/components/inventory/destiny-currencies-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface DictionaryComponentResponseOfint64AndDestinyCurrenciesComponent {
  data?: {[key: string]: DestinyCurrenciesComponent};
  privacy?: ComponentPrivacySetting;
}
