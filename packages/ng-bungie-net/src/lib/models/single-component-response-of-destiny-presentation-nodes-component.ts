/* tslint:disable */
import { DestinyPresentationNodesComponent } from './destiny/components/presentation/destiny-presentation-nodes-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface SingleComponentResponseOfDestinyPresentationNodesComponent {
  data?: DestinyPresentationNodesComponent;
  privacy?: ComponentPrivacySetting;
}
