/* tslint:disable */
import { DictionaryComponentResponseOfint32AndDestinyItemInstanceComponent } from './dictionary-component-response-ofint-32and-destiny-item-instance-component';
import { DictionaryComponentResponseOfint32AndDestinyItemPerksComponent } from './dictionary-component-response-ofint-32and-destiny-item-perks-component';
import { DictionaryComponentResponseOfint32AndDestinyItemRenderComponent } from './dictionary-component-response-ofint-32and-destiny-item-render-component';
import { DictionaryComponentResponseOfint32AndDestinyItemStatsComponent } from './dictionary-component-response-ofint-32and-destiny-item-stats-component';
import { DictionaryComponentResponseOfint32AndDestinyItemSocketsComponent } from './dictionary-component-response-ofint-32and-destiny-item-sockets-component';
import { DictionaryComponentResponseOfint32AndDestinyItemTalentGridComponent } from './dictionary-component-response-ofint-32and-destiny-item-talent-grid-component';
import { DictionaryComponentResponseOfuint32AndDestinyItemPlugComponent } from './dictionary-component-response-ofuint-32and-destiny-item-plug-component';
import { DictionaryComponentResponseOfint32AndDestinyItemObjectivesComponent } from './dictionary-component-response-ofint-32and-destiny-item-objectives-component';
export interface DestinyItemComponentSetOfint32 {
  instances?: DictionaryComponentResponseOfint32AndDestinyItemInstanceComponent;
  perks?: DictionaryComponentResponseOfint32AndDestinyItemPerksComponent;
  renderData?: DictionaryComponentResponseOfint32AndDestinyItemRenderComponent;
  stats?: DictionaryComponentResponseOfint32AndDestinyItemStatsComponent;
  sockets?: DictionaryComponentResponseOfint32AndDestinyItemSocketsComponent;
  talentGrids?: DictionaryComponentResponseOfint32AndDestinyItemTalentGridComponent;
  plugStates?: DictionaryComponentResponseOfuint32AndDestinyItemPlugComponent;
  objectives?: DictionaryComponentResponseOfint32AndDestinyItemObjectivesComponent;
}
