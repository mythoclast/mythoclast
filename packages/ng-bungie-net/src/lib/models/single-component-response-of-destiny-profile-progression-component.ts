/* tslint:disable */
import { DestinyProfileProgressionComponent } from './destiny/components/profiles/destiny-profile-progression-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface SingleComponentResponseOfDestinyProfileProgressionComponent {
  data?: DestinyProfileProgressionComponent;
  privacy?: ComponentPrivacySetting;
}
