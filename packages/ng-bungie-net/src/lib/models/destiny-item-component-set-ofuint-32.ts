/* tslint:disable */
import { DictionaryComponentResponseOfuint32AndDestinyItemInstanceComponent } from './dictionary-component-response-ofuint-32and-destiny-item-instance-component';
import { DictionaryComponentResponseOfuint32AndDestinyItemPerksComponent } from './dictionary-component-response-ofuint-32and-destiny-item-perks-component';
import { DictionaryComponentResponseOfuint32AndDestinyItemRenderComponent } from './dictionary-component-response-ofuint-32and-destiny-item-render-component';
import { DictionaryComponentResponseOfuint32AndDestinyItemStatsComponent } from './dictionary-component-response-ofuint-32and-destiny-item-stats-component';
import { DictionaryComponentResponseOfuint32AndDestinyItemSocketsComponent } from './dictionary-component-response-ofuint-32and-destiny-item-sockets-component';
import { DictionaryComponentResponseOfuint32AndDestinyItemTalentGridComponent } from './dictionary-component-response-ofuint-32and-destiny-item-talent-grid-component';
import { DictionaryComponentResponseOfuint32AndDestinyItemPlugComponent } from './dictionary-component-response-ofuint-32and-destiny-item-plug-component';
import { DictionaryComponentResponseOfuint32AndDestinyItemObjectivesComponent } from './dictionary-component-response-ofuint-32and-destiny-item-objectives-component';
export interface DestinyItemComponentSetOfuint32 {
  instances?: DictionaryComponentResponseOfuint32AndDestinyItemInstanceComponent;
  perks?: DictionaryComponentResponseOfuint32AndDestinyItemPerksComponent;
  renderData?: DictionaryComponentResponseOfuint32AndDestinyItemRenderComponent;
  stats?: DictionaryComponentResponseOfuint32AndDestinyItemStatsComponent;
  sockets?: DictionaryComponentResponseOfuint32AndDestinyItemSocketsComponent;
  talentGrids?: DictionaryComponentResponseOfuint32AndDestinyItemTalentGridComponent;
  plugStates?: DictionaryComponentResponseOfuint32AndDestinyItemPlugComponent;
  objectives?: DictionaryComponentResponseOfuint32AndDestinyItemObjectivesComponent;
}
