/* tslint:disable */
import { DestinyProfileComponent } from './destiny/entities/profiles/destiny-profile-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface SingleComponentResponseOfDestinyProfileComponent {
  data?: DestinyProfileComponent;
  privacy?: ComponentPrivacySetting;
}
