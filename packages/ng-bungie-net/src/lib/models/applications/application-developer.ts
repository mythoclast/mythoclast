/* tslint:disable */
import { DeveloperRole } from '../applications/developer-role';
import { UserInfoCard } from '../user/user-info-card';
export interface ApplicationDeveloper {
  role?: DeveloperRole;
  apiEulaVersion?: number;
  user?: UserInfoCard;
}
