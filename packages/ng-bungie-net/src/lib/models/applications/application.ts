/* tslint:disable */
import { OAuthApplicationType } from '../applications/oauth-application-type';
import { ApplicationStatus } from '../applications/application-status';
import { ApplicationDeveloper } from '../applications/application-developer';
export interface Application {

  /**
   * Value of the Origin header sent in requests generated by this application.
   */
  origin?: string;
  applicationType?: OAuthApplicationType;

  /**
   * Name of the application
   */
  name?: string;

  /**
   * URL used to pass the user's authorization code to the application
   */
  redirectUrl?: string;

  /**
   * Link to website for the application where a user can learn more about the app.
   */
  link?: string;

  /**
   * Permissions the application needs to work
   */
  scope?: number;

  /**
   * Unique ID assigned to the application
   */
  applicationId?: number;

  /**
   * Current status of the application.
   */
  status?: ApplicationStatus;

  /**
   * Date the application was first added to our database.
   */
  creationDate?: string;

  /**
   * Date the application status last changed.
   */
  statusChanged?: string;

  /**
   * Date the first time the application status entered the 'Public' status.
   */
  firstPublished?: string;

  /**
   * List of team members who manage this application on Bungie.net. Will always consist of at least the application owner.
   */
  team?: Array<ApplicationDeveloper>;
}
