/* tslint:disable */

export enum OAuthApplicationType {
  None = 0,
  Confidential = 1,
  Public = 2,
}
