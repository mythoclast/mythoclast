/* tslint:disable */
import { Datapoint } from '../applications/datapoint';
export interface Series {

  /**
   * Collection of samples with time and value.
   */
  datapoints?: Array<Datapoint>;

  /**
   * Target to which to datapoints apply.
   */
  target?: string;
}
