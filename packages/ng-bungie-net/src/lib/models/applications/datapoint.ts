/* tslint:disable */
export interface Datapoint {

  /**
   * Timestamp for the related count.
   */
  time?: string;

  /**
   * Count associated with timestamp
   */
  count?: number;
}
