/* tslint:disable */
import { DestinyCharacterComponent } from './destiny/entities/characters/destiny-character-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface SingleComponentResponseOfDestinyCharacterComponent {
  data?: DestinyCharacterComponent;
  privacy?: ComponentPrivacySetting;
}
