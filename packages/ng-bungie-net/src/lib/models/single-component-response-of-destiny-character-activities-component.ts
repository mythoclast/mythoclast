/* tslint:disable */
import { DestinyCharacterActivitiesComponent } from './destiny/entities/characters/destiny-character-activities-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface SingleComponentResponseOfDestinyCharacterActivitiesComponent {
  data?: DestinyCharacterActivitiesComponent;
  privacy?: ComponentPrivacySetting;
}
