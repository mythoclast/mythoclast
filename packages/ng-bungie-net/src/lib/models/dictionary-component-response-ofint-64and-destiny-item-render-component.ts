/* tslint:disable */
import { DestinyItemRenderComponent } from './destiny/entities/items/destiny-item-render-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface DictionaryComponentResponseOfint64AndDestinyItemRenderComponent {
  data?: {[key: string]: DestinyItemRenderComponent};
  privacy?: ComponentPrivacySetting;
}
