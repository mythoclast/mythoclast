/* tslint:disable */
import { DestinyItemTalentGridComponent } from './destiny/entities/items/destiny-item-talent-grid-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface DictionaryComponentResponseOfuint32AndDestinyItemTalentGridComponent {
  data?: {[key: string]: DestinyItemTalentGridComponent};
  privacy?: ComponentPrivacySetting;
}
