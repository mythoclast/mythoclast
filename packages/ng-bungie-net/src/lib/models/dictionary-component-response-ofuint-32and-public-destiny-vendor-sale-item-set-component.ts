/* tslint:disable */
import { PublicDestinyVendorSaleItemSetComponent } from './destiny/responses/public-destiny-vendor-sale-item-set-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface DictionaryComponentResponseOfuint32AndPublicDestinyVendorSaleItemSetComponent {
  data?: {[key: string]: PublicDestinyVendorSaleItemSetComponent};
  privacy?: ComponentPrivacySetting;
}
