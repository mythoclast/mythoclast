/* tslint:disable */
import { DestinyItemSocketsComponent } from './destiny/entities/items/destiny-item-sockets-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface SingleComponentResponseOfDestinyItemSocketsComponent {
  data?: DestinyItemSocketsComponent;
  privacy?: ComponentPrivacySetting;
}
