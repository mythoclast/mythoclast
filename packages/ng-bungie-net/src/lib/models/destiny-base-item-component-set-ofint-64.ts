/* tslint:disable */
import { DictionaryComponentResponseOfint64AndDestinyItemObjectivesComponent } from './dictionary-component-response-ofint-64and-destiny-item-objectives-component';
export interface DestinyBaseItemComponentSetOfint64 {
  objectives?: DictionaryComponentResponseOfint64AndDestinyItemObjectivesComponent;
}
