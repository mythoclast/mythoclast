/* tslint:disable */
import { DestinyItemSocketsComponent } from './destiny/entities/items/destiny-item-sockets-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface DictionaryComponentResponseOfint32AndDestinyItemSocketsComponent {
  data?: {[key: string]: DestinyItemSocketsComponent};
  privacy?: ComponentPrivacySetting;
}
