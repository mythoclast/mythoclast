/* tslint:disable */
import { DestinyPresentationNodesComponent } from './destiny/components/presentation/destiny-presentation-nodes-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface DictionaryComponentResponseOfint64AndDestinyPresentationNodesComponent {
  data?: {[key: string]: DestinyPresentationNodesComponent};
  privacy?: ComponentPrivacySetting;
}
