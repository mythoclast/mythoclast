/* tslint:disable */
import { PersonalDestinyVendorSaleItemSetComponent } from './destiny/responses/personal-destiny-vendor-sale-item-set-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface DictionaryComponentResponseOfuint32AndPersonalDestinyVendorSaleItemSetComponent {
  data?: {[key: string]: PersonalDestinyVendorSaleItemSetComponent};
  privacy?: ComponentPrivacySetting;
}
