/* tslint:disable */
export interface GroupOptionalConversationEditRequest {
  chatEnabled?: boolean;
  chatName?: string;
  chatSecurity?: 0 | 1;
}
