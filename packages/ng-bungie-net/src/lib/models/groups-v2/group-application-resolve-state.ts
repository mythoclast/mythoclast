/* tslint:disable */

export enum GroupApplicationResolveState {
  Unresolved = 0,
  Accepted = 1,
  Denied = 2,
  Rescinded = 3,
}
