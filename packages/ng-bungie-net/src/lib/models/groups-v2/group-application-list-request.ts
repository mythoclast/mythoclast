/* tslint:disable */
import { UserMembership } from '../user/user-membership';
export interface GroupApplicationListRequest {
  memberships?: Array<UserMembership>;
  message?: string;
}
