/* tslint:disable */
import { GroupV2 } from '../groups-v2/group-v2';
export interface GroupMemberLeaveResult {
  group?: GroupV2;
  groupDeleted?: boolean;
}
