/* tslint:disable */

export enum GroupPotentialMemberStatus {
  None = 0,
  Applicant = 1,
  Invitee = 2,
}
