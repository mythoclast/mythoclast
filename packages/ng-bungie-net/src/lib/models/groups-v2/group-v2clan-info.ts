/* tslint:disable */
import { ClanBanner } from '../groups-v2/clan-banner';

/**
 * This contract contains clan-specific group information. It does not include any investment data.
 */
export interface GroupV2ClanInfo {
  clanCallsign?: string;
  clanBannerData?: ClanBanner;
}
