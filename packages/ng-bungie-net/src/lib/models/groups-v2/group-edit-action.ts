/* tslint:disable */
export interface GroupEditAction {
  isPublicTopicAdminOnly?: boolean;
  name?: string;
  motto?: string;
  theme?: string;
  avatarImageIndex?: number;
  tags?: string;
  isPublic?: boolean;
  membershipOption?: 0 | 1 | 2;
  about?: string;
  allowChat?: boolean;
  chatSecurity?: 0 | 1;
  callsign?: string;
  locale?: string;
  homepage?: 0 | 1 | 2;
  enableInvitationMessagingForAdmins?: boolean;
  defaultPublicity?: 0 | 1 | 2;
}
