/* tslint:disable */
import { GroupType } from '../groups-v2/group-type';
export interface GroupNameSearchRequest {
  groupName?: string;
  groupType?: GroupType;
}
