/* tslint:disable */
import { IgnoreLength } from '../ignores/ignore-length';
export interface GroupBanRequest {
  comment?: string;
  length?: IgnoreLength;
}
