/* tslint:disable */

export enum GroupDateRange {
  All = 0,
  PastDay = 1,
  PastWeek = 2,
  PastMonth = 3,
  PastYear = 4,
}
