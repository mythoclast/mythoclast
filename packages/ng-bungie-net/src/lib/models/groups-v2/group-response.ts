/* tslint:disable */
import { GroupV2 } from '../groups-v2/group-v2';
import { GroupMember } from '../groups-v2/group-member';
import { GroupAllianceStatus } from '../groups-v2/group-alliance-status';
import { GroupPotentialMember } from '../groups-v2/group-potential-member';
export interface GroupResponse {
  detail?: GroupV2;
  founder?: GroupMember;
  alliedIds?: Array<number>;
  parentGroup?: GroupV2;
  allianceStatus?: GroupAllianceStatus;
  groupJoinInviteCount?: number;

  /**
   * This property will be populated if the authenticated user is a member of the group. Note that because of account linking, a user can sometimes be part of a clan more than once. As such, this returns the highest member type available.
   */
  currentUserMemberMap?: {[key: string]: GroupMember};

  /**
   * This property will be populated if the authenticated user is an applicant or has an outstanding invitation to join. Note that because of account linking, a user can sometimes be part of a clan more than once.
   */
  currentUserPotentialMemberMap?: {[key: string]: GroupPotentialMember};
}
