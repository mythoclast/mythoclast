/* tslint:disable */
import { GroupPotentialMember } from '../groups-v2/group-potential-member';
import { GroupV2 } from '../groups-v2/group-v2';
export interface GroupPotentialMembership {
  member?: GroupPotentialMember;
  group?: GroupV2;
}
