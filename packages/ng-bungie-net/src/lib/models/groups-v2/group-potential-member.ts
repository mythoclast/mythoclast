/* tslint:disable */
import { GroupPotentialMemberStatus } from '../groups-v2/group-potential-member-status';
import { UserInfoCard } from '../user/user-info-card';
export interface GroupPotentialMember {
  potentialStatus?: GroupPotentialMemberStatus;
  groupId?: number;
  destinyUserInfo?: UserInfoCard;
  bungieNetUserInfo?: UserInfoCard;
  joinDate?: string;
}
