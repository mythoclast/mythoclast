/* tslint:disable */
import { ChatSecuritySetting } from '../groups-v2/chat-security-setting';
import { GroupType } from '../groups-v2/group-type';
import { GroupHomepage } from '../groups-v2/group-homepage';
import { MembershipOption } from '../groups-v2/membership-option';
import { GroupPostPublicity } from '../groups-v2/group-post-publicity';
import { GroupFeatures } from '../groups-v2/group-features';
import { GroupV2ClanInfoAndInvestment } from '../groups-v2/group-v2clan-info-and-investment';
export interface GroupV2 {
  chatSecurity?: ChatSecuritySetting;
  groupId?: number;
  groupType?: GroupType;
  membershipIdCreated?: number;
  creationDate?: string;
  modificationDate?: string;
  about?: string;
  tags?: Array<string>;
  memberCount?: number;
  isPublic?: boolean;
  isPublicTopicAdminOnly?: boolean;
  motto?: string;
  allowChat?: boolean;
  isDefaultPostPublic?: boolean;
  name?: string;
  locale?: string;
  avatarImageIndex?: number;
  homepage?: GroupHomepage;
  membershipOption?: MembershipOption;
  defaultPublicity?: GroupPostPublicity;
  theme?: string;
  bannerPath?: string;
  avatarPath?: string;
  conversationId?: number;
  enableInvitationMessagingForAdmins?: boolean;
  banExpireDate?: string;
  features?: GroupFeatures;
  clanInfo?: GroupV2ClanInfoAndInvestment;
}
