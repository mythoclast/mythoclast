/* tslint:disable */
import { UserInfoCard } from '../user/user-info-card';
export interface GroupBan {
  groupId?: number;
  lastModifiedBy?: UserInfoCard;
  createdBy?: UserInfoCard;
  dateBanned?: string;
  dateExpires?: string;
  comment?: string;
  bungieNetUserInfo?: UserInfoCard;
  destinyUserInfo?: UserInfoCard;
}
