/* tslint:disable */

export enum GroupsForMemberFilter {
  All = 0,
  Founded = 1,
  NonFounded = 2,
}
