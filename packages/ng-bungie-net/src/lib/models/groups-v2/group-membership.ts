/* tslint:disable */
import { GroupMember } from '../groups-v2/group-member';
import { GroupV2 } from '../groups-v2/group-v2';
export interface GroupMembership {
  member?: GroupMember;
  group?: GroupV2;
}
