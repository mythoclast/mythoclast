/* tslint:disable */
import { DestinyProgression } from '../destiny/destiny-progression';
import { ClanBanner } from '../groups-v2/clan-banner';

/**
 * The same as GroupV2ClanInfo, but includes any investment data.
 */
export interface GroupV2ClanInfoAndInvestment {
  d2ClanProgressions?: {[key: string]: DestinyProgression};
  clanCallsign?: string;
  clanBannerData?: ClanBanner;
}
