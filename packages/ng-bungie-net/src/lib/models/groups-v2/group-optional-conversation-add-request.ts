/* tslint:disable */
import { ChatSecuritySetting } from '../groups-v2/chat-security-setting';
export interface GroupOptionalConversationAddRequest {
  chatName?: string;
  chatSecurity?: ChatSecuritySetting;
}
