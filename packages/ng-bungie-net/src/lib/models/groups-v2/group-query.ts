/* tslint:disable */
import { GroupType } from '../groups-v2/group-type';
import { GroupDateRange } from '../groups-v2/group-date-range';
import { GroupSortBy } from '../groups-v2/group-sort-by';

/**
 * NOTE: GroupQuery, as of Destiny 2, has essentially two totally different and incompatible "modes".
 * If you are querying for a group, you can pass any of the properties below.
 * If you are querying for a Clan, you MUST NOT pass any of the following properties (they must be null or undefined in your request, not just empty string/default values):
 * - groupMemberCountFilter - localeFilter - tagText
 * If you pass these, you will get a useless InvalidParameters error.
 */
export interface GroupQuery {
  name?: string;
  groupType?: GroupType;
  creationDate?: GroupDateRange;
  sortBy?: GroupSortBy;
  groupMemberCountFilter?: 0 | 1 | 2 | 3;
  localeFilter?: string;
  tagText?: string;
  itemsPerPage?: number;
  currentPage?: number;
  requestContinuationToken?: string;
}
