/* tslint:disable */
import { Capabilities } from '../groups-v2/capabilities';
import { BungieMembershipType } from '../bungie-membership-type';
import { HostGuidedGamesPermissionLevel } from '../groups-v2/host-guided-games-permission-level';
import { RuntimeGroupMemberType } from '../groups-v2/runtime-group-member-type';
export interface GroupFeatures {
  maximumMembers?: number;

  /**
   * Maximum number of groups of this type a typical membership may join. For example, a user may join about 50 General groups with their Bungie.net account. They may join one clan per Destiny membership.
   */
  maximumMembershipsOfGroupType?: number;
  capabilities?: Capabilities;
  membershipTypes?: Array<BungieMembershipType>;

  /**
   * Minimum Member Level allowed to invite new members to group
   * Always Allowed: Founder, Acting Founder
   * True means admins have this power, false means they don't
   * Default is false for clans, true for groups.
   */
  invitePermissionOverride?: boolean;

  /**
   * Minimum Member Level allowed to update group culture
   * Always Allowed: Founder, Acting Founder
   * True means admins have this power, false means they don't
   * Default is false for clans, true for groups.
   */
  updateCulturePermissionOverride?: boolean;

  /**
   * Minimum Member Level allowed to host guided games
   * Always Allowed: Founder, Acting Founder, Admin
   * Allowed Overrides: None, Member, Beginner
   * Default is Member for clans, None for groups, although this means nothing for groups.
   */
  hostGuidedGamePermissionOverride?: HostGuidedGamesPermissionLevel;

  /**
   * Minimum Member Level allowed to update banner
   * Always Allowed: Founder, Acting Founder
   * True means admins have this power, false means they don't
   * Default is false for clans, true for groups.
   */
  updateBannerPermissionOverride?: boolean;

  /**
   * Level to join a member at when accepting an invite, application, or joining an open clan
   * Default is Beginner.
   */
  joinLevel?: RuntimeGroupMemberType;
}
