/* tslint:disable */
import { GroupApplicationResolveState } from '../groups-v2/group-application-resolve-state';
export interface GroupApplicationResponse {
  resolution?: GroupApplicationResolveState;
}
