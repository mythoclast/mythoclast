/* tslint:disable */
import { GroupApplicationResolveState } from '../groups-v2/group-application-resolve-state';
import { UserInfoCard } from '../user/user-info-card';
export interface GroupMemberApplication {
  groupId?: number;
  creationDate?: string;
  resolveState?: GroupApplicationResolveState;
  resolveDate?: string;
  resolvedByMembershipId?: number;
  requestMessage?: string;
  resolveMessage?: string;
  destinyUserInfo?: UserInfoCard;
  bungieNetUserInfo?: UserInfoCard;
}
