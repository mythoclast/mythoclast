/* tslint:disable */
import { ChatSecuritySetting } from '../groups-v2/chat-security-setting';
export interface GroupOptionalConversation {
  groupId?: number;
  conversationId?: number;
  chatEnabled?: boolean;
  chatName?: string;
  chatSecurity?: ChatSecuritySetting;
}
