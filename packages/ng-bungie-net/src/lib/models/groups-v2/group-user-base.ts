/* tslint:disable */
import { UserInfoCard } from '../user/user-info-card';
export interface GroupUserBase {
  groupId?: number;
  destinyUserInfo?: UserInfoCard;
  bungieNetUserInfo?: UserInfoCard;
  joinDate?: string;
}
