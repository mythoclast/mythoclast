/* tslint:disable */
import { GroupV2 } from '../groups-v2/group-v2';
export interface GroupMembershipBase {
  group?: GroupV2;
}
