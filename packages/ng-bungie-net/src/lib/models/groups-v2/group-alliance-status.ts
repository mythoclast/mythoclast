/* tslint:disable */

export enum GroupAllianceStatus {
  Unallied = 0,
  Parent = 1,
  Child = 2,
}
