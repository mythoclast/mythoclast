/* tslint:disable */
import { GroupType } from '../groups-v2/group-type';
import { MembershipOption } from '../groups-v2/membership-option';
import { Capabilities } from '../groups-v2/capabilities';
import { GroupV2ClanInfo } from '../groups-v2/group-v2clan-info';

/**
 * A small infocard of group information, usually used for when a list of groups are returned
 */
export interface GroupV2Card {
  memberCount?: number;
  groupId?: number;
  groupType?: GroupType;
  creationDate?: string;
  about?: string;
  motto?: string;
  name?: string;
  locale?: string;
  membershipOption?: MembershipOption;
  capabilities?: Capabilities;
  clanInfo?: GroupV2ClanInfo;
  avatarPath?: string;
  theme?: string;
}
