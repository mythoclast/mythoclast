/* tslint:disable */
import { RuntimeGroupMemberType } from '../groups-v2/runtime-group-member-type';
import { UserInfoCard } from '../user/user-info-card';
export interface GroupMember {
  memberType?: RuntimeGroupMemberType;
  isOnline?: boolean;
  lastOnlineStatusChange?: number;
  groupId?: number;
  destinyUserInfo?: UserInfoCard;
  bungieNetUserInfo?: UserInfoCard;
  joinDate?: string;
}
