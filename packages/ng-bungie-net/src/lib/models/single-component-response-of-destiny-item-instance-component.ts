/* tslint:disable */
import { DestinyItemInstanceComponent } from './destiny/entities/items/destiny-item-instance-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface SingleComponentResponseOfDestinyItemInstanceComponent {
  data?: DestinyItemInstanceComponent;
  privacy?: ComponentPrivacySetting;
}
