/* tslint:disable */
import { DestinyCharacterRenderComponent } from './destiny/entities/characters/destiny-character-render-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface SingleComponentResponseOfDestinyCharacterRenderComponent {
  data?: DestinyCharacterRenderComponent;
  privacy?: ComponentPrivacySetting;
}
