/* tslint:disable */
import { FireteamSummary } from '../fireteam/fireteam-summary';
import { FireteamMember } from '../fireteam/fireteam-member';
export interface FireteamResponse {
  Summary?: FireteamSummary;
  Members?: Array<FireteamMember>;
  Alternates?: Array<FireteamMember>;
}
