/* tslint:disable */
import { FireteamPlatform } from '../fireteam/fireteam-platform';
import { FireteamActivityType } from '../fireteam/fireteam-activity-type';
export interface FireteamSummary {
  availablePlayerSlotCount?: number;
  fireteamId?: number;
  platform?: FireteamPlatform;
  activityType?: FireteamActivityType;
  isImmediate?: boolean;
  scheduledTime?: string;
  ownerMembershipId?: number;
  playerSlotCount?: number;
  alternateSlotCount?: number;
  groupId?: number;
  availableAlternateSlotCount?: number;
  title?: string;
  dateCreated?: string;
  dateModified?: string;
  isPublic?: boolean;
  locale?: string;
  isValid?: boolean;
  datePlayerModified?: string;
}
