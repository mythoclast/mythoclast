/* tslint:disable */

export enum FireteamPlatform {
  Unknown = 0,
  Playstation4 = 1,
  XboxOne = 2,
  Blizzard = 3,
}
