/* tslint:disable */

export enum FireteamActivityType {
  All = 0,
  Raid = 1,
  Crucible = 2,
  Trials = 3,
  Nightfall = 4,
  Anything = 5,
  Gambit = 6,
  BlindWell = 7,
  EscalationProtocol = 8,
  Forge = 9,
  Reckoning = 10,
  Menagerie = 11,
}
