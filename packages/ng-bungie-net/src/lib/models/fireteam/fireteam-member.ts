/* tslint:disable */
import { UserInfoCard } from '../user/user-info-card';
import { FireteamPlatformInviteResult } from '../fireteam/fireteam-platform-invite-result';
export interface FireteamMember {
  destinyUserInfo?: UserInfoCard;
  bungieNetUserInfo?: UserInfoCard;
  characterId?: number;
  dateJoined?: string;
  hasMicrophone?: boolean;
  lastPlatformInviteAttemptDate?: string;
  lastPlatformInviteAttemptResult?: FireteamPlatformInviteResult;
}
