/* tslint:disable */

export enum FireteamPublicSearchOption {
  PublicAndPrivate = 0,
  PublicOnly = 1,
  PrivateOnly = 2,
}
