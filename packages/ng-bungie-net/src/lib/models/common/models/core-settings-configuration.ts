/* tslint:disable */
import { CoreSetting } from '../../common/models/core-setting';
import { CoreSystem } from '../../common/models/core-system';
import { Destiny2CoreSettings } from '../../common/models/destiny-2core-settings';
import { EmailSettings } from '../../user/email-settings';
export interface CoreSettingsConfiguration {
  systemContentLocales?: Array<CoreSetting>;
  environment?: string;
  ignoreReasons?: Array<CoreSetting>;
  forumCategories?: Array<CoreSetting>;
  groupAvatars?: Array<CoreSetting>;
  destinyMembershipTypes?: Array<CoreSetting>;
  recruitmentPlatformTags?: Array<CoreSetting>;
  recruitmentMiscTags?: Array<CoreSetting>;
  recruitmentActivities?: Array<CoreSetting>;
  userContentLocales?: Array<CoreSetting>;
  systems?: {[key: string]: CoreSystem};
  clanBannerDecals?: Array<CoreSetting>;
  clanBannerDecalColors?: Array<CoreSetting>;
  clanBannerGonfalons?: Array<CoreSetting>;
  clanBannerGonfalonColors?: Array<CoreSetting>;
  clanBannerGonfalonDetails?: Array<CoreSetting>;
  clanBannerGonfalonDetailColors?: Array<CoreSetting>;
  clanBannerStandards?: Array<CoreSetting>;
  destiny2CoreSettings?: Destiny2CoreSettings;
  emailSettings?: EmailSettings;
}
