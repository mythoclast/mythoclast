/* tslint:disable */
import { DestinyVendorGroupComponent } from './destiny/components/vendors/destiny-vendor-group-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface SingleComponentResponseOfDestinyVendorGroupComponent {
  data?: DestinyVendorGroupComponent;
  privacy?: ComponentPrivacySetting;
}
