/* tslint:disable */
import { DestinyProfileCollectiblesComponent } from './destiny/components/collectibles/destiny-profile-collectibles-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface SingleComponentResponseOfDestinyProfileCollectiblesComponent {
  data?: DestinyProfileCollectiblesComponent;
  privacy?: ComponentPrivacySetting;
}
