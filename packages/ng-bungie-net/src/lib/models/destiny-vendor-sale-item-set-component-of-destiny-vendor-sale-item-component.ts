/* tslint:disable */
import { DestinyVendorSaleItemComponent } from './destiny/entities/vendors/destiny-vendor-sale-item-component';
export interface DestinyVendorSaleItemSetComponentOfDestinyVendorSaleItemComponent {
  saleItems?: {[key: string]: DestinyVendorSaleItemComponent};
}
