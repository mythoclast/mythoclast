/* tslint:disable */
import { DestinyVendorCategoriesComponent } from './destiny/entities/vendors/destiny-vendor-categories-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface SingleComponentResponseOfDestinyVendorCategoriesComponent {
  data?: DestinyVendorCategoriesComponent;
  privacy?: ComponentPrivacySetting;
}
