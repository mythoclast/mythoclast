/* tslint:disable */
import { DestinyVendorSaleItemComponent } from './destiny/entities/vendors/destiny-vendor-sale-item-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface DictionaryComponentResponseOfint32AndDestinyVendorSaleItemComponent {
  data?: {[key: string]: DestinyVendorSaleItemComponent};
  privacy?: ComponentPrivacySetting;
}
