/* tslint:disable */
import { DestinyProfileRecordsComponent } from './destiny/components/records/destiny-profile-records-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface SingleComponentResponseOfDestinyProfileRecordsComponent {
  data?: DestinyProfileRecordsComponent;
  privacy?: ComponentPrivacySetting;
}
