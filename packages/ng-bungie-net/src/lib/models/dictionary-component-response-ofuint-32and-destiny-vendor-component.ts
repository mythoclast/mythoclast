/* tslint:disable */
import { DestinyVendorComponent } from './destiny/entities/vendors/destiny-vendor-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface DictionaryComponentResponseOfuint32AndDestinyVendorComponent {
  data?: {[key: string]: DestinyVendorComponent};
  privacy?: ComponentPrivacySetting;
}
