/* tslint:disable */
import { DestinyCharacterComponent } from './destiny/entities/characters/destiny-character-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface DictionaryComponentResponseOfint64AndDestinyCharacterComponent {
  data?: {[key: string]: DestinyCharacterComponent};
  privacy?: ComponentPrivacySetting;
}
