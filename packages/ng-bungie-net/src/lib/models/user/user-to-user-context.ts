/* tslint:disable */
import { IgnoreResponse } from '../ignores/ignore-response';
export interface UserToUserContext {
  isFollowing?: boolean;
  ignoreStatus?: IgnoreResponse;
  globalIgnoreEndDate?: string;
}
