/* tslint:disable */
import { EMailSettingSubscriptionLocalization } from '../user/email-setting-subscription-localization';

/**
 * Defines a single subscription: permission to send emails for a specific, focused subject (generally timeboxed, such as for a specific release of a product or feature).
 */
export interface EmailSubscriptionDefinition {

  /**
   * The unique identifier for this subscription.
   */
  name?: string;

  /**
   * A dictionary of localized text for the EMail Opt-in setting, keyed by the locale.
   */
  localization?: {[key: string]: EMailSettingSubscriptionLocalization};

  /**
   * The bitflag value for this subscription. Should be a unique power of two value.
   */
  value?: number;
}
