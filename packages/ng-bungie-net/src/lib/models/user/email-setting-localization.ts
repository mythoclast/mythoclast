/* tslint:disable */

/**
 * Localized text relevant to a given EMail setting in a given localization.
 */
export interface EMailSettingLocalization {
  title?: string;
  description?: string;
}
