/* tslint:disable */
import { BungieMembershipType } from '../bungie-membership-type';

/**
 * Very basic info about a user as returned by the Account server.
 */
export interface UserMembership {

  /**
   * Type of the membership.
   */
  membershipType?: BungieMembershipType;

  /**
   * Membership ID as they user is known in the Accounts service
   */
  membershipId?: number;

  /**
   * Display Name the player has chosen for themselves. The display name is optional when the data type is used as input to a platform API.
   */
  displayName?: string;
}
