/* tslint:disable */
import { UserToUserContext } from '../user/user-to-user-context';
export interface GeneralUser {
  xboxDisplayName?: string;
  membershipId?: number;
  normalizedName?: string;
  displayName?: string;
  profilePicture?: number;
  profileTheme?: number;
  userTitle?: number;
  successMessageFlags?: number;
  isDeleted?: boolean;
  about?: string;
  firstAccess?: string;
  lastUpdate?: string;
  legacyPortalUID?: number;
  context?: UserToUserContext;
  psnDisplayName?: string;
  uniqueName?: string;
  fbDisplayName?: string;
  showActivity?: boolean;
  locale?: string;
  localeInheritDefault?: boolean;
  lastBanReportId?: number;
  showGroupMessaging?: boolean;
  profilePicturePath?: string;
  profilePictureWidePath?: string;
  profileThemeName?: string;
  userTitleDisplay?: string;
  statusText?: string;
  statusDate?: string;
  profileBanExpire?: string;
  blizzardDisplayName?: string;
}
