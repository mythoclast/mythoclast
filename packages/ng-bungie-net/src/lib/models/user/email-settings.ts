/* tslint:disable */
import { EmailOptInDefinition } from '../user/email-opt-in-definition';
import { EmailSubscriptionDefinition } from '../user/email-subscription-definition';
import { EmailViewDefinition } from '../user/email-view-definition';

/**
 * The set of all email subscription/opt-in settings and definitions.
 */
export interface EmailSettings {

  /**
   * Keyed by the name identifier of the opt-in definition.
   */
  optInDefinitions?: {[key: string]: EmailOptInDefinition};

  /**
   * Keyed by the name identifier of the Subscription definition.
   */
  subscriptionDefinitions?: {[key: string]: EmailSubscriptionDefinition};

  /**
   * Keyed by the name identifier of the View definition.
   */
  views?: {[key: string]: EmailViewDefinition};
}
