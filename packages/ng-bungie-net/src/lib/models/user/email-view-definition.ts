/* tslint:disable */
import { EmailViewDefinitionSetting } from '../user/email-view-definition-setting';

/**
 * Represents a data-driven view for Email settings. Web/Mobile UI can use this data to show new EMail settings consistently without further manual work.
 */
export interface EmailViewDefinition {

  /**
   * The identifier for this view.
   */
  name?: string;

  /**
   * The ordered list of settings to show in this view.
   */
  viewSettings?: Array<EmailViewDefinitionSetting>;
}
