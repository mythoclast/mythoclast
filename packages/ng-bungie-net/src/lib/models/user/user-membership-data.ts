/* tslint:disable */
import { UserInfoCard } from '../user/user-info-card';
import { GeneralUser } from '../user/general-user';
export interface UserMembershipData {

  /**
   * this allows you to see destiny memberships that are visible and linked to this account (regardless of whether or not they have characters on the world server)
   */
  destinyMemberships?: Array<UserInfoCard>;
  bungieNetUser?: GeneralUser;
}
