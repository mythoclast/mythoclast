/* tslint:disable */
import { OptInFlags } from '../user/opt-in-flags';
import { EmailSubscriptionDefinition } from '../user/email-subscription-definition';

/**
 * Defines a single opt-in category: a wide-scoped permission to send emails for the subject related to the opt-in.
 */
export interface EmailOptInDefinition {

  /**
   * The unique identifier for this opt-in category.
   */
  name?: string;

  /**
   * The flag value for this opt-in category. For historical reasons, this is defined as a flags enum.
   */
  value?: OptInFlags;

  /**
   * If true, this opt-in setting should be set by default in situations where accounts are created without explicit choices about what they're opting into.
   */
  setByDefault?: boolean;

  /**
   * Information about the dependent subscriptions for this opt-in.
   */
  dependentSubscriptions?: Array<EmailSubscriptionDefinition>;
}
