/* tslint:disable */

export enum OptInFlags {
  Newsletter = 1,
  System = 2,
  Marketing = 4,
  UserResearch = 8,
  CustomerService = 16,
  Social = 32,
}
