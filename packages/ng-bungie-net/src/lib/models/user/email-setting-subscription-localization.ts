/* tslint:disable */

/**
 * Localized text relevant to a given EMail setting in a given localization. Extra settings specifically for subscriptions.
 */
export interface EMailSettingSubscriptionLocalization {
  unknownUserDescription?: string;
  registeredUserDescription?: string;
  unregisteredUserDescription?: string;
  unknownUserActionText?: string;
  knownUserActionText?: string;
  title?: string;
  description?: string;
}
