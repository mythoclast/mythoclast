/* tslint:disable */
import { DestinyItemPlugComponent } from './destiny/components/items/destiny-item-plug-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface DictionaryComponentResponseOfuint32AndDestinyItemPlugComponent {
  data?: {[key: string]: DestinyItemPlugComponent};
  privacy?: ComponentPrivacySetting;
}
