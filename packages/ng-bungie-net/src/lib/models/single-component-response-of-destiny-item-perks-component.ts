/* tslint:disable */
import { DestinyItemPerksComponent } from './destiny/entities/items/destiny-item-perks-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface SingleComponentResponseOfDestinyItemPerksComponent {
  data?: DestinyItemPerksComponent;
  privacy?: ComponentPrivacySetting;
}
