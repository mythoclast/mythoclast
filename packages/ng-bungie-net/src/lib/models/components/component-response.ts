/* tslint:disable */
import { ComponentPrivacySetting } from '../components/component-privacy-setting';

/**
 * The base class for any component-returning object that may need to indicate information about the state of the component being returned.
 */
export interface ComponentResponse {
  privacy?: ComponentPrivacySetting;
}
