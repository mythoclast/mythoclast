/* tslint:disable */
import { DestinyItemStatsComponent } from './destiny/entities/items/destiny-item-stats-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface DictionaryComponentResponseOfint64AndDestinyItemStatsComponent {
  data?: {[key: string]: DestinyItemStatsComponent};
  privacy?: ComponentPrivacySetting;
}
