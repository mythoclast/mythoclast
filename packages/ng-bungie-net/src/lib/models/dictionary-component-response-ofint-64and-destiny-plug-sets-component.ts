/* tslint:disable */
import { DestinyPlugSetsComponent } from './destiny/components/plug-sets/destiny-plug-sets-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface DictionaryComponentResponseOfint64AndDestinyPlugSetsComponent {
  data?: {[key: string]: DestinyPlugSetsComponent};
  privacy?: ComponentPrivacySetting;
}
