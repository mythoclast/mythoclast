/* tslint:disable */
import { DestinyCollectiblesComponent } from './destiny/components/collectibles/destiny-collectibles-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface SingleComponentResponseOfDestinyCollectiblesComponent {
  data?: DestinyCollectiblesComponent;
  privacy?: ComponentPrivacySetting;
}
