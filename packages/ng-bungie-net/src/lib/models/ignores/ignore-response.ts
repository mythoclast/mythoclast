/* tslint:disable */
import { IgnoreStatus } from '../ignores/ignore-status';
export interface IgnoreResponse {
  isIgnored?: boolean;
  ignoreFlags?: IgnoreStatus;
}
