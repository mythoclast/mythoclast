/* tslint:disable */
import { DestinyPublicVendorSaleItemComponent } from './destiny/components/vendors/destiny-public-vendor-sale-item-component';
export interface DestinyVendorSaleItemSetComponentOfDestinyPublicVendorSaleItemComponent {
  saleItems?: {[key: string]: DestinyPublicVendorSaleItemComponent};
}
