/* tslint:disable */
import { DestinyPlatformSilverComponent } from './destiny/components/inventory/destiny-platform-silver-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface SingleComponentResponseOfDestinyPlatformSilverComponent {
  data?: DestinyPlatformSilverComponent;
  privacy?: ComponentPrivacySetting;
}
