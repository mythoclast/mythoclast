/* tslint:disable */
import { DestinyKiosksComponent } from './destiny/components/kiosks/destiny-kiosks-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface SingleComponentResponseOfDestinyKiosksComponent {
  data?: DestinyKiosksComponent;
  privacy?: ComponentPrivacySetting;
}
