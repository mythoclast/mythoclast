/* tslint:disable */
import { GlobalAlertLevel } from './global-alert-level';
import { GlobalAlertType } from './global-alert-type';
import { StreamInfo } from './stream-info';
export interface GlobalAlert {
  AlertKey?: string;
  AlertHtml?: string;
  AlertTimestamp?: string;
  AlertLink?: string;
  AlertLevel?: GlobalAlertLevel;
  AlertType?: GlobalAlertType;
  StreamInfo?: StreamInfo;
}
