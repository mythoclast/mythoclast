/* tslint:disable */
import { DestinyItemSocketsComponent } from './destiny/entities/items/destiny-item-sockets-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface DictionaryComponentResponseOfint64AndDestinyItemSocketsComponent {
  data?: {[key: string]: DestinyItemSocketsComponent};
  privacy?: ComponentPrivacySetting;
}
