/* tslint:disable */
import { DestinyVendorReceiptsComponent } from './destiny/entities/profiles/destiny-vendor-receipts-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface SingleComponentResponseOfDestinyVendorReceiptsComponent {
  data?: DestinyVendorReceiptsComponent;
  privacy?: ComponentPrivacySetting;
}
