/* tslint:disable */
import { DestinyPublicVendorComponent } from './destiny/components/vendors/destiny-public-vendor-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface DictionaryComponentResponseOfuint32AndDestinyPublicVendorComponent {
  data?: {[key: string]: DestinyPublicVendorComponent};
  privacy?: ComponentPrivacySetting;
}
