/* tslint:disable */
import { DestinyCharacterRenderComponent } from './destiny/entities/characters/destiny-character-render-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface DictionaryComponentResponseOfint64AndDestinyCharacterRenderComponent {
  data?: {[key: string]: DestinyCharacterRenderComponent};
  privacy?: ComponentPrivacySetting;
}
