/* tslint:disable */
export interface ContentTypeDefaultValue {
  whenClause?: string;
  whenValue?: string;
  defaultValue?: string;
}
