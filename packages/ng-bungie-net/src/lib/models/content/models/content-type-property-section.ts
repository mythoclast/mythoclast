/* tslint:disable */
export interface ContentTypePropertySection {
  name?: string;
  readableName?: string;
  collapsed?: boolean;
}
