/* tslint:disable */
import { ContentTypeProperty } from '../../content/models/content-type-property';
import { TagMetadataDefinition } from '../../content/models/tag-metadata-definition';
import { TagMetadataItem } from '../../content/models/tag-metadata-item';
import { ContentPreview } from '../../content/models/content-preview';
import { ContentTypePropertySection } from '../../content/models/content-type-property-section';
export interface ContentTypeDescription {
  showInContentEditor?: boolean;
  cType?: string;
  contentDescription?: string;
  previewImage?: string;
  priority?: number;
  reminder?: string;
  properties?: Array<ContentTypeProperty>;
  tagMetadata?: Array<TagMetadataDefinition>;
  tagMetadataItems?: {[key: string]: TagMetadataItem};
  usageExamples?: Array<string>;
  name?: string;
  typeOf?: string;
  bindIdentifierToProperty?: string;
  boundRegex?: string;
  forceIdentifierBinding?: boolean;
  allowComments?: boolean;
  autoEnglishPropertyFallback?: boolean;
  bulkUploadable?: boolean;
  previews?: Array<ContentPreview>;
  suppressCmsPath?: boolean;
  propertySections?: Array<ContentTypePropertySection>;
}
