/* tslint:disable */
import { TagMetadataItem } from '../../content/models/tag-metadata-item';
export interface TagMetadataDefinition {
  description?: string;
  order?: number;
  items?: Array<TagMetadataItem>;
  datatype?: string;
  name?: string;
  isRequired?: boolean;
}
