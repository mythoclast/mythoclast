/* tslint:disable */
export interface ContentPreview {
  name?: string;
  path?: string;
  itemInSet?: boolean;
  setTag?: string;
  setNesting?: number;
  useSetId?: number;
}
