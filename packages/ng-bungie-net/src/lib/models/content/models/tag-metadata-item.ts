/* tslint:disable */
export interface TagMetadataItem {
  description?: string;
  tagText?: string;
  groups?: Array<string>;
  isDefault?: boolean;
  name?: string;
}
