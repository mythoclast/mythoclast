/* tslint:disable */
export interface ContentRepresentation {
  name?: string;
  path?: string;
  validationString?: string;
}
