/* tslint:disable */
export interface CommentSummary {
  topicId?: number;
  commentCount?: number;
}
