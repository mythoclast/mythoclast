/* tslint:disable */
import { PlatformErrorCodes } from '../exceptions/platform-error-codes';
export interface EntityActionResult {
  entityId?: number;
  result?: PlatformErrorCodes;
}
