/* tslint:disable */
import { DestinyCharacterProgressionComponent } from './destiny/entities/characters/destiny-character-progression-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface SingleComponentResponseOfDestinyCharacterProgressionComponent {
  data?: DestinyCharacterProgressionComponent;
  privacy?: ComponentPrivacySetting;
}
