/* tslint:disable */
import { DestinyItemComponent } from './destiny/entities/items/destiny-item-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface SingleComponentResponseOfDestinyItemComponent {
  data?: DestinyItemComponent;
  privacy?: ComponentPrivacySetting;
}
