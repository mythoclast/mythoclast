/* tslint:disable */

/**
 * A hint for how the presentation node should be displayed when shown in a list. How you use this is your UI is up to you.
 */

export enum DestinyPresentationDisplayStyle {
  Category = 0,
  Badge = 1,
  Medals = 2,
  Collectible = 3,
  Record = 4,
}
