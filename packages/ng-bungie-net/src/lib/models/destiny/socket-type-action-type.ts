/* tslint:disable */

/**
 * Indicates the type of actions that can be performed
 */

export enum SocketTypeActionType {
  InsertPlug = 0,
  InfuseItem = 1,
  ReinitializeSocket = 2,
}
