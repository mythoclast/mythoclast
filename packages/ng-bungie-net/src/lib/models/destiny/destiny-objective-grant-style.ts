/* tslint:disable */

/**
 * Some Objectives provide perks, generally as part of providing some kind of interesting modifier for a Challenge or Quest. This indicates when the Perk is granted.
 */

export enum DestinyObjectiveGrantStyle {
  WhenIncomplete = 0,
  WhenComplete = 1,
  Always = 2,
}
