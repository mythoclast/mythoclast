/* tslint:disable */

/**
 * Represents a potential state of an Activity Graph node.
 */

export enum DestinyGraphNodeState {
  Hidden = 0,
  Visible = 1,
  Teaser = 2,
  Incomplete = 3,
  Completed = 4,
}
