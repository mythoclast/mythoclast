/* tslint:disable */

/**
 * The action that happens when the user attempts to refund an item.
 */

export enum DestinyVendorItemRefundPolicy {
  NotRefundable = 0,
  DeletesItem = 1,
  RevokesLicense = 2,
}
