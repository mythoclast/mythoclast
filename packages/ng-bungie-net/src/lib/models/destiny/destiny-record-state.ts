/* tslint:disable */

/**
 * A Flags enumeration/bitmask where each bit represents a possible state that a Record/Triumph can be in.
 */

export enum DestinyRecordState {
  None = 0,
  RecordRedeemed = 1,
  RewardUnavailable = 2,
  ObjectiveNotCompleted = 4,
  Obscured = 8,
  Invisible = 16,
  EntitlementUnowned = 32,
  CanEquipTitle = 64,
}
