/* tslint:disable */
import { AwaUserSelection } from '../../destiny/advanced/awa-user-selection';
export interface AwaUserResponse {

  /**
   * Indication of the selection the user has made (Approving or rejecting the action)
   */
  selection?: AwaUserSelection;

  /**
   * Correlation ID of the request
   */
  correlationId?: string;

  /**
   * Secret nonce received via the PUSH notification.
   */
  nonce?: Array<string>;
}
