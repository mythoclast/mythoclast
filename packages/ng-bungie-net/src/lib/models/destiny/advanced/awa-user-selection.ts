/* tslint:disable */

export enum AwaUserSelection {
  None = 0,
  Rejected = 1,
  Approved = 2,
}
