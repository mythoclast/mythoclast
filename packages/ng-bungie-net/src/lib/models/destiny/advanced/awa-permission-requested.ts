/* tslint:disable */
import { AwaType } from '../../destiny/advanced/awa-type';
import { BungieMembershipType } from '../../bungie-membership-type';
export interface AwaPermissionRequested {

  /**
   * Type of advanced write action.
   */
  type?: AwaType;

  /**
   * Item instance ID the action shall be applied to. This is optional for all but a new AwaType values. Rule of thumb is to provide the item instance ID if one is available.
   */
  affectedItemId?: number;

  /**
   * Destiny membership type of the account to modify.
   */
  membershipType?: BungieMembershipType;

  /**
   * Destiny character ID, if applicable, that will be affected by the action.
   */
  characterId?: number;
}
