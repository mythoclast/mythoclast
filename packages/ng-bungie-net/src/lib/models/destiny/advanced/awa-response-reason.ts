/* tslint:disable */

export enum AwaResponseReason {
  None = 0,
  Answered = 1,
  TimedOut = 2,
  Replaced = 3,
}
