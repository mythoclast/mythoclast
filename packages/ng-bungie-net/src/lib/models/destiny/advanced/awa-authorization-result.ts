/* tslint:disable */
import { AwaUserSelection } from '../../destiny/advanced/awa-user-selection';
import { AwaResponseReason } from '../../destiny/advanced/awa-response-reason';
import { AwaType } from '../../destiny/advanced/awa-type';
import { BungieMembershipType } from '../../bungie-membership-type';
export interface AwaAuthorizationResult {

  /**
   * Indication of how the user responded to the request. If the value is "Approved" the actionToken will contain the token that can be presented when performing the advanced write action.
   */
  userSelection?: AwaUserSelection;
  responseReason?: AwaResponseReason;

  /**
   * Message to the app developer to help understand the response.
   */
  developerNote?: string;

  /**
   * Credential used to prove the user authorized an advanced write action.
   */
  actionToken?: string;

  /**
   * This token may be used to perform the requested action this number of times, at a maximum. If this value is 0, then there is no limit.
   */
  maximumNumberOfUses?: number;

  /**
   * Time, UTC, when token expires.
   */
  validUntil?: string;

  /**
   * Advanced Write Action Type from the permission request.
   */
  type?: AwaType;

  /**
   * MembershipType from the permission request.
   */
  membershipType?: BungieMembershipType;
}
