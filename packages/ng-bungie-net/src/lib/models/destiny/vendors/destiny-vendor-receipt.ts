/* tslint:disable */
import { DestinyItemQuantity } from '../../destiny/destiny-item-quantity';
import { DestinyVendorItemRefundPolicy } from '../../destiny/destiny-vendor-item-refund-policy';

/**
 * If a character purchased an item that is refundable, a Vendor Receipt will be created on the user's Destiny Profile. These expire after a configurable period of time, but until then can be used to get refunds on items. BNet does not provide the ability to refund a purchase *yet*, but you know.
 */
export interface DestinyVendorReceipt {

  /**
   * The amount paid for the item, in terms of items that were consumed in the purchase and their quantity.
   */
  currencyPaid?: Array<DestinyItemQuantity>;

  /**
   * The item that was received, and its quantity.
   */
  itemReceived?: {};

  /**
   * The unlock flag used to determine whether you still have the purchased item.
   */
  licenseUnlockHash?: number;

  /**
   * The ID of the character who made the purchase.
   */
  purchasedByCharacterId?: number;

  /**
   * Whether you can get a refund, and what happens in order for the refund to be received. See the DestinyVendorItemRefundPolicy enum for details.
   */
  refundPolicy?: DestinyVendorItemRefundPolicy;

  /**
   * The identifier of this receipt.
   */
  sequenceNumber?: number;

  /**
   * The seconds since epoch at which this receipt is rendered invalid.
   */
  timeToExpiration?: number;

  /**
   * The date at which this receipt is rendered invalid.
   */
  expiresOn?: string;
}
