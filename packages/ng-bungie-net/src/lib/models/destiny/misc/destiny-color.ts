/* tslint:disable */

/**
 * Represents a color whose RGBA values are all represented as values between 0 and 255.
 */
export interface DestinyColor {
  red?: string;
  green?: string;
  blue?: string;
  alpha?: string;
}
