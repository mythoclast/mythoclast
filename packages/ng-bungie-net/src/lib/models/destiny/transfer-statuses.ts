/* tslint:disable */

/**
 * Whether you can transfer an item, and why not if you can't.
 */

export enum TransferStatuses {
  CanTransfer = 0,
  ItemIsEquipped = 1,
  NotTransferrable = 2,
  NoRoomInDestination = 4,
}
