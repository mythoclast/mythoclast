/* tslint:disable */

/**
 * Display categories can have custom sort orders. These are the possible options.
 */

export enum VendorDisplayCategorySortOrder {
  Default = 0,
  SortByTier = 1,
}
