/* tslint:disable */
import { DestinyItemSocketState } from '../../../destiny/entities/items/destiny-item-socket-state';

/**
 * Instanced items can have sockets, which are slots on the item where plugs can be inserted.
 * Sockets are a bit complex: be sure to examine the documentation on the DestinyInventoryItemDefinition's "socket" block and elsewhere on these objects for more details.
 */
export interface DestinyItemSocketsComponent {

  /**
   * The list of all sockets on the item, and their status information.
   */
  sockets?: Array<DestinyItemSocketState>;
}
