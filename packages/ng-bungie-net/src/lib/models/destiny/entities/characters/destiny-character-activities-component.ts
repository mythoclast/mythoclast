/* tslint:disable */
import { DestinyActivity } from '../../../destiny/destiny-activity';
import { DestinyActivityModeType } from '../../../destiny/historical-stats/definitions/destiny-activity-mode-type';

/**
 * This component holds activity data for a character. It will tell you about the character's current activity status, as well as activities that are available to the user.
 */
export interface DestinyCharacterActivitiesComponent {

  /**
   * The last date that the user started playing an activity.
   */
  dateActivityStarted?: string;

  /**
   * The list of activities that the user can play.
   */
  availableActivities?: Array<DestinyActivity>;

  /**
   * If the user is in an activity, this will be the hash of the Activity being played. Note that you must combine this info with currentActivityModeHash to get a real picture of what the user is doing right now. For instance, PVP "Activities" are just maps: it's the ActivityMode that determines what type of PVP game they're playing.
   */
  currentActivityHash?: number;

  /**
   * If the user is in an activity, this will be the hash of the activity mode being played. Combine with currentActivityHash to give a person a full picture of what they're doing right now.
   */
  currentActivityModeHash?: number;

  /**
   * And the current activity's most specific mode type, if it can be found.
   */
  currentActivityModeType?: DestinyActivityModeType;

  /**
   * If the user is in an activity, this will be the hashes of the DestinyActivityModeDefinition being played. Combine with currentActivityHash to give a person a full picture of what they're doing right now.
   */
  currentActivityModeHashes?: Array<number>;

  /**
   * All Activity Modes that apply to the current activity being played, in enum form.
   */
  currentActivityModeTypes?: Array<DestinyActivityModeType>;

  /**
   * If the user is in a playlist, this is the hash identifier for the playlist that they chose.
   */
  currentPlaylistActivityHash?: number;

  /**
   * This will have the activity hash of the last completed story/campaign mission, in case you care about that.
   */
  lastCompletedStoryHash?: number;
}
