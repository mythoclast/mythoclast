/* tslint:disable */

/**
 * A flags enumeration/bitmask where each bit represents a different possible state that the item can be in that may effect how the item is displayed to the user and what actions can be performed against it.
 */

export enum ItemState {
  None = 0,
  Locked = 1,
  Tracked = 2,
  Masterwork = 4,
}
