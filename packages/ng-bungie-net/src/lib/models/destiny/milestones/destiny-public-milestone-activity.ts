/* tslint:disable */
import { DestinyPublicMilestoneActivityVariant } from '../../destiny/milestones/destiny-public-milestone-activity-variant';

/**
 * A milestone may have one or more conceptual Activities associated with it, and each of those conceptual activities could have a variety of variants, modes, tiers, what-have-you. Our attempts to determine what qualifies as a conceptual activity are, unfortunately, janky. So if you see missing modes or modes that don't seem appropriate to you, let us know and I'll buy you a beer if we ever meet up in person.
 */
export interface DestinyPublicMilestoneActivity {

  /**
   * The hash identifier of the activity that's been chosen to be considered the canonical "conceptual" activity definition. This may have many variants, defined herein.
   */
  activityHash?: number;

  /**
   * The activity may have 0-to-many modifiers: if it does, this will contain the hashes to the DestinyActivityModifierDefinition that defines the modifier being applied.
   */
  modifierHashes?: Array<number>;

  /**
   * Every relevant variation of this conceptual activity, including the conceptual activity itself, have variants defined here.
   */
  variants?: Array<DestinyPublicMilestoneActivityVariant>;

  /**
   * The hash identifier of the most specific Activity Mode under which this activity is played. This is useful for situations where the activity in question is - for instance - a PVP map, but it's not clear what mode the PVP map is being played under. If it's a playlist, this will be less specific: but hopefully useful in some way.
   */
  activityModeHash?: number;

  /**
   * The enumeration equivalent of the most specific Activity Mode under which this activity is played.
   */
  activityModeType?: 0 | 2 | 3 | 4 | 5 | 6 | 7 | 9 | 10 | 11 | 12 | 13 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 24 | 25 | 26 | 27 | 28 | 29 | 30 | 31 | 32 | 37 | 38 | 39 | 40 | 41 | 42 | 43 | 44 | 45 | 46 | 47 | 48 | 49 | 50 | 51 | 52 | 53 | 54 | 55 | 56 | 57 | 58 | 59 | 60 | 61 | 62 | 63 | 64 | 65 | 66 | 67 | 68 | 69 | 70 | 71 | 72 | 73 | 74 | 75 | 76 | 77;
}
