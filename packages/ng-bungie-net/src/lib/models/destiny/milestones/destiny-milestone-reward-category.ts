/* tslint:disable */
import { DestinyMilestoneRewardEntry } from '../../destiny/milestones/destiny-milestone-reward-entry';

/**
 * Represents a category of "summary" rewards that can be earned for the Milestone regardless of specific quest rewards that can be earned.
 */
export interface DestinyMilestoneRewardCategory {

  /**
   * Look up the relevant DestinyMilestoneDefinition, and then use rewardCategoryHash to look up the category info in DestinyMilestoneDefinition.rewards.
   */
  rewardCategoryHash?: number;

  /**
   * The individual reward entries for this category, and their status.
   */
  entries?: Array<DestinyMilestoneRewardEntry>;
}
