/* tslint:disable */

/**
 * Indicates how a socket is populated, and where you should look for valid plug data. This is a flags enumeration/bitmask field, as you may have to look in multiple sources across multiple components for valid plugs.
 */

export enum SocketPlugSources {
  None = 0,
  InventorySourced = 1,
  ReusablePlugItems = 2,
  ProfilePlugSet = 4,
  CharacterPlugSet = 8,
}
