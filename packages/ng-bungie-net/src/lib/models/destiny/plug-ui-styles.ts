/* tslint:disable */

/**
 * If the plug has a specific custom style, this enumeration will represent that style/those styles.
 */

export enum PlugUiStyles {
  None = 0,
  Masterwork = 1,
}
