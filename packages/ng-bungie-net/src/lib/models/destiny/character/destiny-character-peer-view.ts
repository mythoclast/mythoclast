/* tslint:disable */
import { DestinyItemPeerView } from '../../destiny/character/destiny-item-peer-view';

/**
 * A minimal view of a character's equipped items, for the purpose of rendering a summary screen or showing the character in 3D.
 */
export interface DestinyCharacterPeerView {
  equipment?: Array<DestinyItemPeerView>;
}
