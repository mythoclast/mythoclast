/* tslint:disable */

export enum DestinyPresentationNodeType {
  Default = 0,
  Category = 1,
  Collectibles = 2,
  Records = 3,
}
