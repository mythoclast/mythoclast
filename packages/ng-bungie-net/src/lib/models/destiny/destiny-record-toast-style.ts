/* tslint:disable */

export enum DestinyRecordToastStyle {
  None = 0,
  Record = 1,
  Lore = 2,
  Badge = 3,
  MetaRecord = 4,
  MedalComplete = 5,
}
