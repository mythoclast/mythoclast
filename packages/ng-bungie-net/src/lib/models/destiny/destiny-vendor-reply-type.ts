/* tslint:disable */

/**
 * This determines the type of reply that a Vendor will have during an Interaction.
 */

export enum DestinyVendorReplyType {
  Accept = 0,
  Decline = 1,
  Complete = 2,
}
