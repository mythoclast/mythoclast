/* tslint:disable */
export interface ImagePyramidEntry {

  /**
   * The name of the subfolder where these images are located.
   */
  name?: string;

  /**
   * The factor by which the original image size has been reduced.
   */
  factor?: number;
}
