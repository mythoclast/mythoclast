/* tslint:disable */
import { GearAssetDataBaseDefinition } from '../../destiny/config/gear-asset-data-base-definition';
import { ImagePyramidEntry } from '../../destiny/config/image-pyramid-entry';

/**
 * DestinyManifest is the external-facing contract for just the properties needed by those calling the Destiny Platform.
 */
export interface DestinyManifest {
  version?: string;
  mobileAssetContentPath?: string;
  mobileGearAssetDataBases?: Array<GearAssetDataBaseDefinition>;
  mobileWorldContentPaths?: {[key: string]: string};
  jsonWorldContentPaths?: {[key: string]: string};
  mobileClanBannerDatabasePath?: string;
  mobileGearCDN?: {[key: string]: string};

  /**
   * Information about the "Image Pyramid" for Destiny icons. Where possible, we create smaller versions of Destiny icons. These are found as subfolders under the location of the "original/full size" Destiny images, with the same file name and extension as the original image itself. (this lets us avoid sending largely redundant path info with every entity, at the expense of the smaller versions of the image being less discoverable)
   */
  iconImagePyramidInfo?: Array<ImagePyramidEntry>;
}
