import {
  DestinyPlaceDefinition,
  DestinyActivityDefinition,
  DestinyActivityTypeDefinition,
  DestinyClassDefinition,
  DestinyGenderDefinition,
  DestinyInventoryBucketDefinition,
  DestinyRaceDefinition,
  DestinyTalentGridDefinition,
  DestinyUnlockDefinition,
  DestinyMaterialRequirementSetDefinition,
  DestinySandboxPerkDefinition,
  DestinyStatGroupDefinition,
  DestinyProgressionMappingDefinition,
  DestinyFactionDefinition,
  DestinyVendorGroupDefinition,
  DestinyRewardSourceDefinition,
  DestinyUnlockValueDefinition,
  DestinyItemCategoryDefinition,
  DestinyDamageTypeDefinition,
  DestinyActivityModeDefinition,
  DestinyActivityGraphDefinition,
  DestinyCollectibleDefinition,
  DestinyStatDefinition,
  DestinyItemTierTypeDefinition,
  DestinyPresentationNodeDefinition,
  DestinyRecordDefinition,
  DestinyDestinationDefinition,
  DestinyEquipmentSlotDefinition,
  DestinyInventoryItemDefinition,
  DestinyLocationDefinition,
  DestinyLoreDefinition,
  DestinyObjectiveDefinition,
  DestinyProgressionDefinition,
  DestinyProgressionLevelRequirementDefinition,
  DestinySeasonDefinition,
  DestinySocketCategoryDefinition,
  DestinySocketTypeDefinition,
  DestinyVendorDefinition,
  DestinyMilestoneDefinition,
  DestinyActivityModifierDefinition,
  DestinyReportReasonCategoryDefinition,
  DestinyPlugSetDefinition,
  DestinyChecklistDefinition,
  DestinyEnemyRaceDefinition,
  DestinyNodeStepSummaryDefinition,
  DestinyArtDyeChannelDefinition,
  DestinyArtDyeReferenceDefinition,
  DestinyRewardMappingDefinition,
  DestinyRewardSheetDefinition,
  DestinyMedalTierDefinition,
  DestinyAchievementDefinition,
  DestinyActivityInteractableDefinition,
  DestinyEntitlementOfferDefinition,
  DestinyPlatformBucketMappingDefinition,
  DestinyBondDefinition,
  DestinyCharacterCustomizationCategoryDefinition,
  DestinyCharacterCustomizationOptionDefinition,
  DestinyRewardAdjusterProgressionMapDefinition,
  DestinyRewardItemListDefinition,
  DestinySackRewardItemListDefinition,
  DestinySandboxPatternDefinition,
  DestinyUnlockCountMappingDefinition,
  DestinyUnlockEventDefinition,
  DestinyUnlockExpressionMappingDefinition,
  DestinyRewardAdjusterPointerDefinition
} from '../../../models';

export interface DestinyEnemyRaceDefinitions {
  [prop: string]: {[key: string]: DestinyEnemyRaceDefinition};
}
export interface DestinyNodeStepSummaryDefinitions {
  [prop: string]: {[key: string]: DestinyNodeStepSummaryDefinition};
}
export interface DestinyArtDyeChannelDefinitions {
  [prop: string]: {[key: string]: DestinyArtDyeChannelDefinition};
}
export interface DestinyArtDyeReferenceDefinitions {
  [prop: string]: {[key: string]: DestinyArtDyeReferenceDefinition};
}
export interface DestinyPlaceDefinitions {
  [prop: string]: {[key: string]: DestinyPlaceDefinition};
}
export interface DestinyActivityDefinitions {
  [prop: string]: {[key: string]: DestinyActivityDefinition};
}
export interface DestinyActivityTypeDefinitions {
  [prop: string]: {[key: string]: DestinyActivityTypeDefinition};
}
export interface DestinyClassDefinitions {
  [prop: string]: {[key: string]: DestinyClassDefinition};
}
export interface DestinyGenderDefinitions {
  [prop: string]: {[key: string]: DestinyGenderDefinition};
}
export interface DestinyInventoryBucketDefinitions {
  [prop: string]: {[key: string]: DestinyInventoryBucketDefinition};
}
export interface DestinyRaceDefinitions {
  [prop: string]: {[key: string]: DestinyRaceDefinition};
}
export interface DestinyTalentGridDefinitions {
  [prop: string]: {[key: string]: DestinyTalentGridDefinition};
}
export interface DestinyUnlockDefinitions {
  [prop: string]: {[key: string]: DestinyUnlockDefinition};
}
export interface DestinyMaterialRequirementSetDefinitions {
  [prop: string]: {[key: string]: DestinyMaterialRequirementSetDefinition};
}
export interface DestinySandboxPerkDefinitions {
  [prop: string]: {[key: string]: DestinySandboxPerkDefinition};
}
export interface DestinyStatGroupDefinitions {
  [prop: string]: {[key: string]: DestinyStatGroupDefinition};
}
export interface DestinyProgressionMappingDefinitions {
  [prop: string]: {[key: string]: DestinyProgressionMappingDefinition};
}
export interface DestinyFactionDefinitions {
  [prop: string]: {[key: string]: DestinyFactionDefinition};
}
export interface DestinyVendorGroupDefinitions {
  [prop: string]: {[key: string]: DestinyVendorGroupDefinition};
}
export interface DestinyRewardSourceDefinitions {
  [prop: string]: {[key: string]: DestinyRewardSourceDefinition};
}
export interface DestinyUnlockValueDefinitions {
  [prop: string]: {[key: string]: DestinyUnlockValueDefinition};
}
export interface DestinyRewardMappingDefinitions {
  [prop: string]: {[key: string]: DestinyRewardMappingDefinition};
}
export interface DestinyRewardSheetDefinitions {
  [prop: string]: {[key: string]: DestinyRewardSheetDefinition};
}
export interface DestinyItemCategoryDefinitions {
  [prop: string]: {[key: string]: DestinyItemCategoryDefinition};
}
export interface DestinyDamageTypeDefinitions {
  [prop: string]: {[key: string]: DestinyDamageTypeDefinition};
}
export interface DestinyActivityModeDefinitions {
  [prop: string]: {[key: string]: DestinyActivityModeDefinition};
}
export interface DestinyMedalTierDefinitions {
  [prop: string]: {[key: string]: DestinyMedalTierDefinition};
}
export interface DestinyAchievementDefinitions {
  [prop: string]: {[key: string]: DestinyAchievementDefinition};
}
export interface DestinyActivityGraphDefinitions {
  [prop: string]: {[key: string]: DestinyActivityGraphDefinition};
}
export interface DestinyActivityInteractableDefinitions {
  [prop: string]: {[key: string]: DestinyActivityInteractableDefinition};
}
export interface DestinyCollectibleDefinitions {
  [prop: string]: {[key: string]: DestinyCollectibleDefinition};
}
export interface DestinyEntitlementOfferDefinitions {
  [prop: string]: {[key: string]: DestinyEntitlementOfferDefinition};
}
export interface DestinyStatDefinitions {
  [prop: string]: {[key: string]: DestinyStatDefinition};
}
export interface DestinyItemTierTypeDefinitions {
  [prop: string]: {[key: string]: DestinyItemTierTypeDefinition};
}
export interface DestinyPlatformBucketMappingDefinitions {
  [prop: string]: {[key: string]: DestinyPlatformBucketMappingDefinition};
}
export interface DestinyPresentationNodeDefinitions {
  [prop: string]: {[key: string]: DestinyPresentationNodeDefinition};
}
export interface DestinyRecordDefinitions {
  [prop: string]: {[key: string]: DestinyRecordDefinition};
}
export interface DestinyBondDefinitions {
  [prop: string]: {[key: string]: DestinyBondDefinition};
}
export interface DestinyCharacterCustomizationCategoryDefinitions {
  [prop: string]: {[key: string]: DestinyCharacterCustomizationCategoryDefinition};
}
export interface DestinyCharacterCustomizationOptionDefinitions {
  [prop: string]: {[key: string]: DestinyCharacterCustomizationOptionDefinition};
}
export interface DestinyDestinationDefinitions {
  [prop: string]: {[key: string]: DestinyDestinationDefinition};
}
export interface DestinyEquipmentSlotDefinitions {
  [prop: string]: {[key: string]: DestinyEquipmentSlotDefinition};
}
export interface DestinyInventoryItemDefinitions {
  [prop: string]: {[key: string]: DestinyInventoryItemDefinition};
}
export interface DestinyLocationDefinitions {
  [prop: string]: {[key: string]: DestinyLocationDefinition};
}
export interface DestinyLoreDefinitions {
  [prop: string]: {[key: string]: DestinyLoreDefinition};
}
export interface DestinyObjectiveDefinitions {
  [prop: string]: {[key: string]: DestinyObjectiveDefinition};
}
export interface DestinyProgressionDefinitions {
  [prop: string]: {[key: string]: DestinyProgressionDefinition};
}
export interface DestinyProgressionLevelRequirementDefinitions {
  [prop: string]: {[key: string]: DestinyProgressionLevelRequirementDefinition};
}
export interface DestinyRewardAdjusterProgressionMapDefinitions {
  [prop: string]: {[key: string]: DestinyRewardAdjusterProgressionMapDefinition};
}
export interface DestinyRewardItemListDefinitions {
  [prop: string]: {[key: string]: DestinyRewardItemListDefinition};
}
export interface DestinySackRewardItemListDefinitions {
  [prop: string]: {[key: string]: DestinySackRewardItemListDefinition};
}
export interface DestinySandboxPatternDefinitions {
  [prop: string]: {[key: string]: DestinySandboxPatternDefinition};
}
export interface DestinySeasonDefinitions {
  [prop: string]: {[key: string]: DestinySeasonDefinition};
}
export interface DestinySocketCategoryDefinitions {
  [prop: string]: {[key: string]: DestinySocketCategoryDefinition};
}
export interface DestinySocketTypeDefinitions {
  [prop: string]: {[key: string]: DestinySocketTypeDefinition};
}
export interface DestinyUnlockCountMappingDefinitions {
  [prop: string]: {[key: string]: DestinyUnlockCountMappingDefinition};
}
export interface DestinyUnlockEventDefinitions {
  [prop: string]: {[key: string]: DestinyUnlockEventDefinition};
}
export interface DestinyUnlockExpressionMappingDefinitions {
  [prop: string]: {[key: string]: DestinyUnlockExpressionMappingDefinition};
}
export interface DestinyVendorDefinitions {
  [prop: string]: {[key: string]: DestinyVendorDefinition};
}
export interface DestinyRewardAdjusterPointerDefinitions {
  [prop: string]: {[key: string]: DestinyRewardAdjusterPointerDefinition};
}
export interface DestinyMilestoneDefinitions {
  [prop: string]: {[key: string]: DestinyMilestoneDefinition};
}
export interface DestinyActivityModifierDefinitions {
  [prop: string]: {[key: string]: DestinyActivityModifierDefinition};
}
export interface DestinyReportReasonCategoryDefinitions {
  [prop: string]: {[key: string]: DestinyReportReasonCategoryDefinition};
}
export interface DestinyPlugSetDefinitions {
  [prop: string]: {[key: string]: DestinyPlugSetDefinition};
}
export interface DestinyChecklistDefinitions {
  [prop: string]: {[key: string]: DestinyChecklistDefinition};
}
export interface DestinyManifestDatabase {
  DestinyEnemyRaceDefinition: DestinyEnemyRaceDefinitions;
  DestinyNodeStepSummaryDefinition: DestinyNodeStepSummaryDefinitions;
  DestinyArtDyeChannelDefinition: DestinyArtDyeChannelDefinitions;
  DestinyArtDyeReferenceDefinition: DestinyArtDyeReferenceDefinitions;
  DestinyPlaceDefinition: DestinyPlaceDefinitions;
  DestinyActivityDefinition: DestinyActivityDefinitions;
  DestinyActivityTypeDefinition: DestinyActivityTypeDefinitions;
  DestinyClassDefinition: DestinyClassDefinitions;
  DestinyGenderDefinition: DestinyGenderDefinitions;
  DestinyInventoryBucketDefinition: DestinyInventoryBucketDefinitions;
  DestinyRaceDefinition: DestinyRaceDefinitions;
  DestinyTalentGridDefinition: DestinyTalentGridDefinitions;
  DestinyUnlockDefinition: DestinyUnlockDefinitions;
  DestinyMaterialRequirementSetDefinition: DestinyMaterialRequirementSetDefinitions;
  DestinySandboxPerkDefinition: DestinySandboxPerkDefinitions;
  DestinyStatGroupDefinition: DestinyStatGroupDefinitions;
  DestinyProgressionMappingDefinition: DestinyProgressionMappingDefinitions;
  DestinyFactionDefinition: DestinyFactionDefinitions;
  DestinyVendorGroupDefinition: DestinyVendorGroupDefinitions;
  DestinyRewardSourceDefinition: DestinyRewardSourceDefinitions;
  DestinyUnlockValueDefinition: DestinyUnlockValueDefinitions;
  DestinyRewardMappingDefinition: DestinyRewardMappingDefinitions;
  DestinyRewardSheetDefinition: DestinyRewardSheetDefinitions;
  DestinyItemCategoryDefinition: DestinyItemCategoryDefinitions;
  DestinyDamageTypeDefinition: DestinyDamageTypeDefinitions;
  DestinyActivityModeDefinition: DestinyActivityModeDefinitions;
  DestinyMedalTierDefinition: DestinyMedalTierDefinitions;
  DestinyAchievementDefinition: DestinyAchievementDefinitions;
  DestinyActivityGraphDefinition: DestinyActivityGraphDefinitions;
  DestinyActivityInteractableDefinition: DestinyActivityInteractableDefinitions;
  DestinyCollectibleDefinition: DestinyCollectibleDefinitions;
  DestinyEntitlementOfferDefinition: DestinyEntitlementOfferDefinitions;
  DestinyStatDefinition: DestinyStatDefinitions;
  DestinyItemTierTypeDefinition: DestinyItemTierTypeDefinitions;
  DestinyPlatformBucketMappingDefinition: DestinyPlatformBucketMappingDefinitions;
  DestinyPresentationNodeDefinition: DestinyPresentationNodeDefinitions;
  DestinyRecordDefinition: DestinyRecordDefinitions;
  DestinyBondDefinition: DestinyBondDefinitions;
  DestinyCharacterCustomizationCategoryDefinition: DestinyCharacterCustomizationCategoryDefinitions;
  DestinyCharacterCustomizationOptionDefinition: DestinyCharacterCustomizationOptionDefinitions;
  DestinyDestinationDefinition: DestinyDestinationDefinitions;
  DestinyEquipmentSlotDefinition: DestinyEquipmentSlotDefinitions;
  DestinyInventoryItemDefinition: DestinyInventoryItemDefinitions;
  DestinyLocationDefinition: DestinyLocationDefinitions;
  DestinyLoreDefinition: DestinyLoreDefinitions;
  DestinyObjectiveDefinition: DestinyObjectiveDefinitions;
  DestinyProgressionDefinition: DestinyProgressionDefinitions;
  DestinyProgressionLevelRequirementDefinition: DestinyProgressionLevelRequirementDefinitions;
  DestinyRewardAdjusterProgressionMapDefinition: DestinyRewardAdjusterProgressionMapDefinitions;
  DestinyRewardItemListDefinition: DestinyRewardItemListDefinitions;
  DestinySackRewardItemListDefinition: DestinySackRewardItemListDefinitions;
  DestinySandboxPatternDefinition: DestinySandboxPatternDefinitions;
  DestinySeasonDefinition: DestinySeasonDefinitions;
  DestinySocketCategoryDefinition: DestinySocketCategoryDefinitions;
  DestinySocketTypeDefinition: DestinySocketTypeDefinitions;
  DestinyUnlockCountMappingDefinition: DestinyUnlockCountMappingDefinitions;
  DestinyUnlockEventDefinition: DestinyUnlockEventDefinitions;
  DestinyUnlockExpressionMappingDefinition: DestinyUnlockExpressionMappingDefinitions;
  DestinyVendorDefinition: DestinyVendorDefinitions;
  DestinyRewardAdjusterPointerDefinition: DestinyRewardAdjusterPointerDefinitions;
  DestinyMilestoneDefinition: DestinyMilestoneDefinitions;
  DestinyActivityModifierDefinition: DestinyActivityModifierDefinitions;
  DestinyReportReasonCategoryDefinition: DestinyReportReasonCategoryDefinitions;
  DestinyPlugSetDefinition: DestinyPlugSetDefinitions;
  DestinyChecklistDefinition: DestinyChecklistDefinitions;
}
