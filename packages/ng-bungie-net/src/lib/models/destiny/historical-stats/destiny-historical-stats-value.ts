/* tslint:disable */
export interface DestinyHistoricalStatsValue {

  /**
   * Unique ID for this stat
   */
  statId?: string;

  /**
   * Basic stat value.
   */
  basic?: {};

  /**
   * Per game average for the statistic, if applicable
   */
  pga?: {};

  /**
   * Weighted value of the stat if a weight greater than 1 has been assigned.
   */
  weighted?: {};

  /**
   * When a stat represents the best, most, longest, fastest or some other personal best, the actual activity ID where that personal best was established is available on this property.
   */
  activityId?: number;
}
