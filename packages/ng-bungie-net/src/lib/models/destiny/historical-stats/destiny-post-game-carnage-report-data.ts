/* tslint:disable */
import { DestinyPostGameCarnageReportEntry } from '../../destiny/historical-stats/destiny-post-game-carnage-report-entry';
import { DestinyPostGameCarnageReportTeamEntry } from '../../destiny/historical-stats/destiny-post-game-carnage-report-team-entry';
export interface DestinyPostGameCarnageReportData {

  /**
   * Date and time for the activity.
   */
  period?: string;

  /**
   * If this activity has "phases", this is the phase at which the activity was started.
   */
  startingPhaseIndex?: number;

  /**
   * Details about the activity.
   */
  activityDetails?: {};

  /**
   * Collection of players and their data for this activity.
   */
  entries?: Array<DestinyPostGameCarnageReportEntry>;

  /**
   * Collection of stats for the player in this activity.
   */
  teams?: Array<DestinyPostGameCarnageReportTeamEntry>;
}
