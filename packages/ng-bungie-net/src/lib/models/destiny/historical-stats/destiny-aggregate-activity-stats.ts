/* tslint:disable */
import { DestinyHistoricalStatsValue } from '../../destiny/historical-stats/destiny-historical-stats-value';
export interface DestinyAggregateActivityStats {

  /**
   * Hash ID that can be looked up in the DestinyActivityTable.
   */
  activityHash?: number;

  /**
   * Collection of stats for the player in this activity.
   */
  values?: {[key: string]: DestinyHistoricalStatsValue};
}
