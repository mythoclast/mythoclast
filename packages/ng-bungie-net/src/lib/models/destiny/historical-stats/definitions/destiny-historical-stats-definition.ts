/* tslint:disable */
import { PeriodType } from '../../../destiny/historical-stats/definitions/period-type';
import { DestinyActivityModeType } from '../../../destiny/historical-stats/definitions/destiny-activity-mode-type';
import { DestinyStatsCategoryType } from '../../../destiny/historical-stats/definitions/destiny-stats-category-type';
import { DestinyStatsGroupType } from '../../../destiny/historical-stats/definitions/destiny-stats-group-type';
import { UnitType } from '../../../destiny/historical-stats/definitions/unit-type';
export interface DestinyHistoricalStatsDefinition {

  /**
   * Description of a stat if applicable.
   */
  statDescription?: string;

  /**
   * Unique programmer friendly ID for this stat
   */
  statId?: string;

  /**
   * Time periods the statistic covers
   */
  periodTypes?: Array<PeriodType>;

  /**
   * Game modes where this statistic can be reported.
   */
  modes?: Array<DestinyActivityModeType>;

  /**
   * Category for the stat.
   */
  category?: DestinyStatsCategoryType;

  /**
   * Display name
   */
  statName?: string;

  /**
   * Display name abbreviated
   */
  statNameAbbr?: string;

  /**
   * Statistic group
   */
  group?: DestinyStatsGroupType;

  /**
   * Unit, if any, for the statistic
   */
  unitType?: UnitType;

  /**
   * Optional URI to an icon for the statistic
   */
  iconImage?: string;

  /**
   * Optional icon for the statistic
   */
  mergeMethod?: 0 | 1 | 2;

  /**
   * Localized Unit Name for the stat.
   */
  unitLabel?: string;

  /**
   * Weight assigned to this stat indicating its relative impressiveness.
   */
  weight?: number;

  /**
   * The tier associated with this medal - be it implicitly or explicitly.
   */
  medalTierHash?: number;
}
