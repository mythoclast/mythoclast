/* tslint:disable */

export enum DestinyStatsMergeMethod {
  Add = 0,
  Min = 1,
  Max = 2,
}
