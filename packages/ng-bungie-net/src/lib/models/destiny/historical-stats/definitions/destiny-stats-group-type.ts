/* tslint:disable */

/**
 * If the enum value is > 100, it is a "special" group that cannot be queried for directly (special cases apply to when they are returned, and are not relevant in general cases)
 */

export enum DestinyStatsGroupType {
  None = 0,
  General = 1,
  Weapons = 2,
  Medals = 3,
  ReservedGroups = 100,
  Leaderboard = 101,
  Activity = 102,
  UniqueWeapon = 103,
  Internal = 104,
}
