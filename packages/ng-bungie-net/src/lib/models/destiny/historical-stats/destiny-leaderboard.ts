/* tslint:disable */
import { DestinyLeaderboardEntry } from '../../destiny/historical-stats/destiny-leaderboard-entry';
export interface DestinyLeaderboard {
  statId?: string;
  entries?: Array<DestinyLeaderboardEntry>;
}
