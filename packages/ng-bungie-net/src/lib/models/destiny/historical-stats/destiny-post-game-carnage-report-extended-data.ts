/* tslint:disable */
import { DestinyHistoricalWeaponStats } from '../../destiny/historical-stats/destiny-historical-weapon-stats';
import { DestinyHistoricalStatsValue } from '../../destiny/historical-stats/destiny-historical-stats-value';
export interface DestinyPostGameCarnageReportExtendedData {

  /**
   * List of weapons and their perspective values.
   */
  weapons?: Array<DestinyHistoricalWeaponStats>;

  /**
   * Collection of stats for the player in this activity.
   */
  values?: {[key: string]: DestinyHistoricalStatsValue};
}
