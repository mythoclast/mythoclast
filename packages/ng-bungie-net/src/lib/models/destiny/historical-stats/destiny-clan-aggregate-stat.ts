/* tslint:disable */
import { DestinyActivityModeType } from '../../destiny/historical-stats/definitions/destiny-activity-mode-type';
export interface DestinyClanAggregateStat {

  /**
   * The id of the mode of stats (allPvp, allPvE, etc)
   */
  mode?: DestinyActivityModeType;

  /**
   * The id of the stat
   */
  statId?: string;

  /**
   * Value of the stat for this player
   */
  value?: {};
}
