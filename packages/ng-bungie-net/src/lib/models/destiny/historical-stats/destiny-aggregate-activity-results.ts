/* tslint:disable */
import { DestinyAggregateActivityStats } from '../../destiny/historical-stats/destiny-aggregate-activity-stats';
export interface DestinyAggregateActivityResults {

  /**
   * List of all activities the player has participated in.
   */
  activities?: Array<DestinyAggregateActivityStats>;
}
