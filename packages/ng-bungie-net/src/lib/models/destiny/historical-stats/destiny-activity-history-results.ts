/* tslint:disable */
import { DestinyHistoricalStatsPeriodGroup } from '../../destiny/historical-stats/destiny-historical-stats-period-group';
export interface DestinyActivityHistoryResults {

  /**
   * List of activities, the most recent activity first.
   */
  activities?: Array<DestinyHistoricalStatsPeriodGroup>;
}
