/* tslint:disable */
import { DestinyHistoricalStatsByPeriod } from '../../destiny/historical-stats/destiny-historical-stats-by-period';
export interface DestinyHistoricalStatsWithMerged {
  results?: {[key: string]: DestinyHistoricalStatsByPeriod};
  merged?: DestinyHistoricalStatsByPeriod;
}
