/* tslint:disable */
import { DestinyHistoricalStatsByPeriod } from '../../destiny/historical-stats/destiny-historical-stats-by-period';
export interface DestinyHistoricalStatsResults {

  [prop: string]: DestinyHistoricalStatsByPeriod;
}
