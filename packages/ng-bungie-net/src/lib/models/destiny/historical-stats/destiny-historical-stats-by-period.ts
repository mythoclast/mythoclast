/* tslint:disable */
import { DestinyHistoricalStatsValue } from '../../destiny/historical-stats/destiny-historical-stats-value';
import { DestinyHistoricalStatsPeriodGroup } from '../../destiny/historical-stats/destiny-historical-stats-period-group';
export interface DestinyHistoricalStatsByPeriod {
  allTime?: {[key: string]: DestinyHistoricalStatsValue};
  allTimeTier1?: {[key: string]: DestinyHistoricalStatsValue};
  allTimeTier2?: {[key: string]: DestinyHistoricalStatsValue};
  allTimeTier3?: {[key: string]: DestinyHistoricalStatsValue};
  daily?: Array<DestinyHistoricalStatsPeriodGroup>;
  monthly?: Array<DestinyHistoricalStatsPeriodGroup>;
}
