/* tslint:disable */
export interface DestinyPostGameCarnageReportTeamEntry {

  /**
   * Integer ID for the team.
   */
  teamId?: number;

  /**
   * Team's standing relative to other teams.
   */
  standing?: {};

  /**
   * Score earned by the team
   */
  score?: {};

  /**
   * Alpha or Bravo
   */
  teamName?: string;
}
