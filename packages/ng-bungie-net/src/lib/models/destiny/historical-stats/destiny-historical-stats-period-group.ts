/* tslint:disable */
import { DestinyHistoricalStatsValue } from '../../destiny/historical-stats/destiny-historical-stats-value';
export interface DestinyHistoricalStatsPeriodGroup {

  /**
   * Period for the group. If the stat periodType is day, then this will have a specific day. If the type is monthly, then this value will be the first day of the applicable month. This value is not set when the periodType is 'all time'.
   */
  period?: string;

  /**
   * If the period group is for a specific activity, this property will be set.
   */
  activityDetails?: {};

  /**
   * Collection of stats for the period.
   */
  values?: {[key: string]: DestinyHistoricalStatsValue};
}
