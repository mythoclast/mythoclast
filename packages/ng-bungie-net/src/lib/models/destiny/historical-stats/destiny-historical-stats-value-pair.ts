/* tslint:disable */
export interface DestinyHistoricalStatsValuePair {

  /**
   * Raw value of the statistic
   */
  value?: number;

  /**
   * Localized formated version of the value.
   */
  displayValue?: string;
}
