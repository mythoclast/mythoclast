/* tslint:disable */
import { DestinyHistoricalStatsWithMerged } from '../../destiny/historical-stats/destiny-historical-stats-with-merged';
import { DestinyHistoricalStatsPerCharacter } from '../../destiny/historical-stats/destiny-historical-stats-per-character';
export interface DestinyHistoricalStatsAccountResult {
  mergedDeletedCharacters?: DestinyHistoricalStatsWithMerged;
  mergedAllCharacters?: DestinyHistoricalStatsWithMerged;
  characters?: Array<DestinyHistoricalStatsPerCharacter>;
}
