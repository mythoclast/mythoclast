/* tslint:disable */
import { DestinyHistoricalStatsValue } from '../../destiny/historical-stats/destiny-historical-stats-value';
export interface DestinyHistoricalWeaponStats {

  /**
   * The hash ID of the item definition that describes the weapon.
   */
  referenceId?: number;

  /**
   * Collection of stats for the period.
   */
  values?: {[key: string]: DestinyHistoricalStatsValue};
}
