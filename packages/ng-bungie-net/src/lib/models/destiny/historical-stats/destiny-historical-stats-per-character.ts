/* tslint:disable */
import { DestinyHistoricalStatsByPeriod } from '../../destiny/historical-stats/destiny-historical-stats-by-period';
export interface DestinyHistoricalStatsPerCharacter {
  characterId?: number;
  deleted?: boolean;
  results?: {[key: string]: DestinyHistoricalStatsByPeriod};
  merged?: DestinyHistoricalStatsByPeriod;
}
