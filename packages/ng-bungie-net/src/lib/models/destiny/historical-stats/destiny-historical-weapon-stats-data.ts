/* tslint:disable */
import { DestinyHistoricalWeaponStats } from '../../destiny/historical-stats/destiny-historical-weapon-stats';
export interface DestinyHistoricalWeaponStatsData {

  /**
   * List of weapons and their perspective values.
   */
  weapons?: Array<DestinyHistoricalWeaponStats>;
}
