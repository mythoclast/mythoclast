/* tslint:disable */
export interface DestinyPlayer {

  /**
   * Level of the character if available. Zero if it is not available.
   */
  characterLevel?: number;

  /**
   * Details about the player as they are known in game (platform display name, Destiny emblem)
   */
  destinyUserInfo?: {};
  classHash?: number;
  raceHash?: number;
  genderHash?: number;

  /**
   * Class of the character if applicable and available.
   */
  characterClass?: string;

  /**
   * Light Level of the character if available. Zero if it is not available.
   */
  lightLevel?: number;

  /**
   * Details about the player as they are known on BungieNet. This will be undefined if the player has marked their credential private, or does not have a BungieNet account.
   */
  bungieNetUserInfo?: {};

  /**
   * Current clan name for the player. This value may be null or an empty string if the user does not have a clan.
   */
  clanName?: string;

  /**
   * Current clan tag for the player. This value may be null or an empty string if the user does not have a clan.
   */
  clanTag?: string;

  /**
   * If we know the emblem's hash, this can be used to look up the player's emblem at the time of a match when receiving PGCR data, or otherwise their currently equipped emblem (if we are able to obtain it).
   */
  emblemHash?: number;
}
