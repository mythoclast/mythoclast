/* tslint:disable */
import { DestinyLeaderboard } from '../../destiny/historical-stats/destiny-leaderboard';
interface IndexedDestinyLeaderboardResults {
  [prop: string]: {[key: string]: DestinyLeaderboard};
}

export type DestinyLeaderboardResults = IndexedDestinyLeaderboardResults & {
  /**
   * Indicate the membership ID of the account that is the focal point of the provided leaderboards.
   */
  focusMembershipId?: number;

  /**
   * Indicate the character ID of the character that is the focal point of the provided leaderboards. May be null, in which case any character from the focus membership can appear in the provided leaderboards.
   */
  focusCharacterId?: number;
}
