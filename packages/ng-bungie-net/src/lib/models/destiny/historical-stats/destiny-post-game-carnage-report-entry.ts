/* tslint:disable */
import { DestinyHistoricalStatsValue } from '../../destiny/historical-stats/destiny-historical-stats-value';
export interface DestinyPostGameCarnageReportEntry {

  /**
   * Standing of the player
   */
  standing?: number;

  /**
   * Score of the player if available
   */
  score?: {};

  /**
   * Identity details of the player
   */
  player?: {};

  /**
   * ID of the player's character used in the activity.
   */
  characterId?: number;

  /**
   * Collection of stats for the player in this activity.
   */
  values?: {[key: string]: DestinyHistoricalStatsValue};

  /**
   * Extended data extracted from the activity blob.
   */
  extended?: {};
}
