/* tslint:disable */

/**
 * The response contract for GetDestinyCharacter, with components that can be returned for character and item-level data.
 */
export interface DestinyCharacterResponse {

  /**
   * When sockets refer to reusable Plug Sets (see DestinyPlugSetDefinition for more info), this is the set of plugs and their states that are scoped to this character.
   * This comes back with ItemSockets, as it is needed for a complete picture of the sockets on requested items.
   * COMPONENT TYPE: ItemSockets
   */
  plugSets?: {};

  /**
   * The character-level non-equipped inventory items.
   * COMPONENT TYPE: CharacterInventories
   */
  inventory?: {};

  /**
   * Character progression data, including Milestones.
   * COMPONENT TYPE: CharacterProgressions
   */
  progressions?: {};

  /**
   * Character rendering data - a minimal set of information about equipment and dyes used for rendering.
   * COMPONENT TYPE: CharacterRenderData
   */
  renderData?: {};

  /**
   * Activity data - info about current activities available to the player.
   * COMPONENT TYPE: CharacterActivities
   */
  activities?: {};

  /**
   * Equipped items on the character.
   * COMPONENT TYPE: CharacterEquipment
   */
  equipment?: {};

  /**
   * Items available from Kiosks that are available to this specific character.
   * COMPONENT TYPE: Kiosks
   */
  kiosks?: {};

  /**
   * Base information about the character in question.
   * COMPONENT TYPE: Characters
   */
  character?: {};

  /**
   * COMPONENT TYPE: PresentationNodes
   */
  presentationNodes?: {};

  /**
   * COMPONENT TYPE: Records
   */
  records?: {};

  /**
   * COMPONENT TYPE: Collectibles
   */
  collectibles?: {};

  /**
   * The set of components belonging to the player's instanced items.
   * COMPONENT TYPE: [See inside the DestinyItemComponentSet contract for component types.]
   */
  itemComponents?: {};

  /**
   * The set of components belonging to the player's UNinstanced items. Because apparently now those too can have information relevant to the character's state.
   * COMPONENT TYPE: [See inside the DestinyItemComponentSet contract for component types.]
   */
  uninstancedItemComponents?: {};

  /**
   * A "lookup" convenience component that can be used to quickly check if the character has access to items that can be used for purchasing.
   * COMPONENT TYPE: CurrencyLookups
   */
  currencyLookups?: {};
}
