/* tslint:disable */
import { DestinyItemComponent } from '../../destiny/entities/items/destiny-item-component';

/**
 * A response containing all of the components for all requested vendors.
 */
export interface InventoryChangedResponse {

  /**
   * Items that appeared in the inventory possibly as a result of an action.
   */
  addedInventoryItems?: Array<DestinyItemComponent>;

  /**
   * Items that disappeared from the inventory possibly as a result of an action.
   */
  removedInventoryItems?: Array<DestinyItemComponent>;
}
