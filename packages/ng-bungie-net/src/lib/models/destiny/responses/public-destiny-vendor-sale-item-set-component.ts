/* tslint:disable */
import { DestinyPublicVendorSaleItemComponent } from '../../destiny/components/vendors/destiny-public-vendor-sale-item-component';
export interface PublicDestinyVendorSaleItemSetComponent {
  saleItems?: {[key: string]: DestinyPublicVendorSaleItemComponent};
}
