/* tslint:disable */

/**
 * A response containing all of the components for a vendor.
 */
export interface DestinyVendorResponse {

  /**
   * The base properties of the vendor.
   * COMPONENT TYPE: Vendors
   */
  vendor?: {};

  /**
   * Categories that the vendor has available, and references to the sales therein.
   * COMPONENT TYPE: VendorCategories
   */
  categories?: {};

  /**
   * Sales, keyed by the vendorItemIndex of the item being sold.
   * COMPONENT TYPE: VendorSales
   */
  sales?: {};

  /**
   * Item components, keyed by the vendorItemIndex of the active sale items.
   * COMPONENT TYPE: [See inside the DestinyItemComponentSet contract for component types.]
   */
  itemComponents?: {};

  /**
   * A "lookup" convenience component that can be used to quickly check if the character has access to items that can be used for purchasing.
   * COMPONENT TYPE: CurrencyLookups
   */
  currencyLookups?: {};
}
