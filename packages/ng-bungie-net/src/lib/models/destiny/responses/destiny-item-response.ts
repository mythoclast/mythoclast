/* tslint:disable */

/**
 * The response object for retrieving an individual instanced item. None of these components are relevant for an item that doesn't have an "itemInstanceId": for those, get your information from the DestinyInventoryDefinition.
 */
export interface DestinyItemResponse {

  /**
   * If the item is on a character, this will return the ID of the character that is holding the item.
   */
  characterId?: number;

  /**
   * Common data for the item relevant to its non-instanced properties.
   * COMPONENT TYPE: ItemCommonData
   */
  item?: {};

  /**
   * Basic instance data for the item.
   * COMPONENT TYPE: ItemInstances
   */
  instance?: {};

  /**
   * Information specifically about the item's objectives.
   * COMPONENT TYPE: ItemObjectives
   */
  objectives?: {};

  /**
   * Information specifically about the perks currently active on the item.
   * COMPONENT TYPE: ItemPerks
   */
  perks?: {};

  /**
   * Information about how to render the item in 3D.
   * COMPONENT TYPE: ItemRenderData
   */
  renderData?: {};

  /**
   * Information about the computed stats of the item: power, defense, etc...
   * COMPONENT TYPE: ItemStats
   */
  stats?: {};

  /**
   * Information about the talent grid attached to the item. Talent nodes can provide a variety of benefits and abilities, and in Destiny 2 are used almost exclusively for the character's "Builds".
   * COMPONENT TYPE: ItemTalentGrids
   */
  talentGrid?: {};

  /**
   * Information about the sockets of the item: which are currently active, what potential sockets you could have and the stats/abilities/perks you can gain from them.
   * COMPONENT TYPE: ItemSockets
   */
  sockets?: {};
}
