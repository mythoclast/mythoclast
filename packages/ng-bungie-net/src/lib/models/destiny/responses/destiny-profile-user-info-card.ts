/* tslint:disable */
import { BungieMembershipType } from '../../bungie-membership-type';
export interface DestinyProfileUserInfoCard {
  dateLastPlayed?: string;

  /**
   * The list of membership types/Platforms that this Destiny Account can be used on - either its own original platform, or any subservient platforms hooked up to it through Cross Save.
   */
  applicableMembershipTypes?: Array<BungieMembershipType>;

  /**
   * If this profile is being overridden/obscured by Cross Save, this will be set to true. We will still return the profile for display purposes where users need to know the info: it is up to any given area of the app/site to determine if this profile should still be shown.
   */
  isOverridden?: boolean;

  /**
   * If true, this account is hooked up as the "Primary" cross save account for one or more platforms.
   */
  isCrossSavePrimary?: boolean;

  /**
   * A platform specific additional display name - ex: psn Real Name, bnet Unique Name, etc.
   */
  supplementalDisplayName?: string;

  /**
   * URL the Icon if available.
   */
  iconPath?: string;

  /**
   * Type of the membership.
   */
  membershipType?: BungieMembershipType;

  /**
   * Membership ID as they user is known in the Accounts service
   */
  membershipId?: number;

  /**
   * Display Name the player has chosen for themselves. The display name is optional when the data type is used as input to a platform API.
   */
  displayName?: string;
}
