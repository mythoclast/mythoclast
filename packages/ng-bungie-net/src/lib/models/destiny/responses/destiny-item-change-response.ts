/* tslint:disable */
import { DestinyItemResponse } from '../../destiny/responses/destiny-item-response';
import { DestinyItemComponent } from '../../destiny/entities/items/destiny-item-component';
export interface DestinyItemChangeResponse {
  item?: DestinyItemResponse;

  /**
   * Items that appeared in the inventory possibly as a result of an action.
   */
  addedInventoryItems?: Array<DestinyItemComponent>;

  /**
   * Items that disappeared from the inventory possibly as a result of an action.
   */
  removedInventoryItems?: Array<DestinyItemComponent>;
}
