/* tslint:disable */

/**
 * Activity Modes are grouped into a few possible broad categories.
 */

export enum DestinyActivityModeCategory {
  None = 0,
  PvE = 1,
  PvP = 2,
  PvECompetitive = 3,
}
