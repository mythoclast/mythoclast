/* tslint:disable */
import { DestinyItemSocketEntryDefinition } from '../../destiny/definitions/destiny-item-socket-entry-definition';
import { DestinyItemIntrinsicSocketEntryDefinition } from '../../destiny/definitions/destiny-item-intrinsic-socket-entry-definition';
import { DestinyItemSocketCategoryDefinition } from '../../destiny/definitions/destiny-item-socket-category-definition';

/**
 * If defined, the item has at least one socket.
 */
export interface DestinyItemSocketBlockDefinition {

  /**
   * This was supposed to be a string that would give per-item details about sockets. In practice, it turns out that all this ever has is the localized word "details". ... that's lame, but perhaps it will become something cool in the future.
   */
  detail?: string;

  /**
   * Each non-intrinsic (or mutable) socket on an item is defined here. Check inside for more info.
   */
  socketEntries?: Array<DestinyItemSocketEntryDefinition>;

  /**
   * Each intrinsic (or immutable/permanent) socket on an item is defined here, along with the plug that is permanently affixed to the socket.
   */
  intrinsicSockets?: Array<DestinyItemIntrinsicSocketEntryDefinition>;

  /**
   * A convenience property, that refers to the sockets in the "sockets" property, pre-grouped by category and ordered in the manner that they should be grouped in the UI. You could form this yourself with the existing data, but why would you want to? Enjoy life man.
   */
  socketCategories?: Array<DestinyItemSocketCategoryDefinition>;
}
