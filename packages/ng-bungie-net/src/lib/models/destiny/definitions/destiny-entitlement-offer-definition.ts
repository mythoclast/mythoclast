export interface DestinyEntitlementOfferDefinition {
  offerKey: string;
  hash: number;
  index: number;
  redacted: boolean;
  blacklisted: boolean;
}
