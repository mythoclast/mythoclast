/* tslint:disable */
export interface DestinyMilestoneChallengeDefinition {

  /**
   * The challenge related to this milestone.
   */
  challengeObjectiveHash?: number;
}
