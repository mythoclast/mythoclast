/* tslint:disable */
import { DestinyMilestoneRewardEntryDefinition } from '../../../destiny/definitions/milestones/destiny-milestone-reward-entry-definition';

/**
 * The definition of a category of rewards, that contains many individual rewards.
 */
export interface DestinyMilestoneRewardCategoryDefinition {

  /**
   * Identifies the reward category. Only guaranteed unique within this specific component!
   */
  categoryHash?: number;

  /**
   * The string identifier for the category, if you want to use it for some end. Guaranteed unique within the specific component.
   */
  categoryIdentifier?: string;

  /**
   * Hopefully this is obvious by now.
   */
  displayProperties?: {};

  /**
   * If this milestone can provide rewards, this will define the sets of rewards that can be earned, the conditions under which they can be acquired, internal data that we'll use at runtime to determine whether you've already earned or redeemed this set of rewards, and the category that this reward should be placed under.
   */
  rewardEntries?: {[key: string]: DestinyMilestoneRewardEntryDefinition};

  /**
   * If you want to use BNet's recommended order for rendering categories programmatically, use this value and compare it to other categories to determine the order in which they should be rendered. I don't feel great about putting this here, I won't lie.
   */
  order?: number;
}
