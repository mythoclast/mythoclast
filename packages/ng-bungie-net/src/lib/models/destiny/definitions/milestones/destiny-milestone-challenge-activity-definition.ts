/* tslint:disable */
import { DestinyMilestoneChallengeDefinition } from '../../../destiny/definitions/milestones/destiny-milestone-challenge-definition';
import { DestinyMilestoneChallengeActivityGraphNodeEntry } from '../../../destiny/definitions/milestones/destiny-milestone-challenge-activity-graph-node-entry';
import { DestinyMilestoneChallengeActivityPhase } from '../../../destiny/definitions/milestones/destiny-milestone-challenge-activity-phase';
export interface DestinyMilestoneChallengeActivityDefinition {

  /**
   * The activity for which this challenge is active.
   */
  activityHash?: number;
  challenges?: Array<DestinyMilestoneChallengeDefinition>;

  /**
   * If the activity and its challenge is visible on any of these nodes, it will be returned.
   */
  activityGraphNodes?: Array<DestinyMilestoneChallengeActivityGraphNodeEntry>;

  /**
   * Phases related to this activity, if there are any.
   * These will be listed in the order in which they will appear in the actual activity.
   */
  phases?: Array<DestinyMilestoneChallengeActivityPhase>;
}
