/* tslint:disable */

/**
 * The type of milestone. Milestones can be Tutorials, one-time/triggered/non-repeating but not necessarily tutorials, or Repeating Milestones.
 */

export enum DestinyMilestoneType {
  Unknown = 0,
  Tutorial = 1,
  OneTime = 2,
  Weekly = 3,
  Daily = 4,
  Special = 5,
}
