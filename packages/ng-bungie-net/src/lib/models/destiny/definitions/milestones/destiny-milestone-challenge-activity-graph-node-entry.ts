/* tslint:disable */
export interface DestinyMilestoneChallengeActivityGraphNodeEntry {
  activityGraphHash?: number;
  activityGraphNodeHash?: number;
}
