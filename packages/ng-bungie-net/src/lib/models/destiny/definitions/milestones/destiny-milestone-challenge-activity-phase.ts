/* tslint:disable */
export interface DestinyMilestoneChallengeActivityPhase {

  /**
   * The hash identifier of the activity's phase.
   */
  phaseHash?: number;
}
