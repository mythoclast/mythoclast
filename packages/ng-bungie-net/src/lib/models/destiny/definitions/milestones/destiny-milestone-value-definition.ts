/* tslint:disable */
import { DestinyDisplayPropertiesDefinition } from '../../../destiny/definitions/common/destiny-display-properties-definition';

/**
 * The definition for information related to a key/value pair that is relevant for a particular Milestone or component within the Milestone.
 * This lets us more flexibly pass up information that's useful to someone, even if it's not necessarily us.
 */
export interface DestinyMilestoneValueDefinition {
  key?: string;
  displayProperties?: DestinyDisplayPropertiesDefinition;
}
