/* tslint:disable */
import { DestinyUnlockExpressionDefinition } from '../../../destiny/definitions/destiny-unlock-expression-definition';
import { DestinyLinkedGraphEntryDefinition } from '../../../destiny/definitions/director/destiny-linked-graph-entry-definition';

/**
 * This describes links between the current graph and others, as well as when that link is relevant.
 */
export interface DestinyLinkedGraphDefinition {
  description?: string;
  name?: string;
  unlockExpression?: DestinyUnlockExpressionDefinition;
  linkedGraphId?: number;
  linkedGraphs?: Array<DestinyLinkedGraphEntryDefinition>;
  overview?: string;
}
