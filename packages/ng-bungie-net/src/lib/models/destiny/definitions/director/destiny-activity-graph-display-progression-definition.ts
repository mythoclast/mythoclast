/* tslint:disable */

/**
 * When a Graph needs to show active Progressions, this defines those objectives as well as an identifier.
 */
export interface DestinyActivityGraphDisplayProgressionDefinition {
  id?: number;
  progressionHash?: number;
}
