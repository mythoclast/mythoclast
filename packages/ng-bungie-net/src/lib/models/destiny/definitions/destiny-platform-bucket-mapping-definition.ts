export interface DestinyPlatformBucketMappingDefinition {
  membershipType: number;
  bucketHash: number;
  hash: number;
  index: number;
  redacted: boolean;
  blacklisted: boolean;
}
