/* tslint:disable */
import { DestinyTalentNodeStepWeaponPerformances } from '../../destiny/definitions/destiny-talent-node-step-weapon-performances';
import { DestinyTalentNodeStepImpactEffects } from '../../destiny/definitions/destiny-talent-node-step-impact-effects';
import { DestinyTalentNodeStepGuardianAttributes } from '../../destiny/definitions/destiny-talent-node-step-guardian-attributes';
import { DestinyTalentNodeStepLightAbilities } from '../../destiny/definitions/destiny-talent-node-step-light-abilities';
import { DestinyTalentNodeStepDamageTypes } from '../../destiny/definitions/destiny-talent-node-step-damage-types';

/**
 * These properties are an attempt to categorize talent node steps by certain common properties. See the related enumerations for the type of properties being categorized.
 */
export interface DestinyTalentNodeStepGroups {
  weaponPerformance?: DestinyTalentNodeStepWeaponPerformances;
  impactEffects?: DestinyTalentNodeStepImpactEffects;
  guardianAttributes?: DestinyTalentNodeStepGuardianAttributes;
  lightAbilities?: DestinyTalentNodeStepLightAbilities;
  damageTypes?: DestinyTalentNodeStepDamageTypes;
}
