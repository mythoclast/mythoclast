export interface DestinyRewardItemListDefinition {
  hash: number;
  index: number;
  redacted: boolean;
  blacklisted: boolean;
}
