/* tslint:disable */

/**
 * An overly complicated wrapper for the item level at which the item should spawn.
 */
export interface DestinyItemCreationEntryLevelDefinition {
  level?: number;
}
