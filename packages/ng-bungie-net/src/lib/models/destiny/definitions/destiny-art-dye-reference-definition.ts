export interface DestinyArtDyeReferenceDefinition {
  artDyeHash: number;
  index: number;
  dyeManifestHash: number;
  hash: number;
  redacted: boolean;
  blacklisted: boolean;
}
