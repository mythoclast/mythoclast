/* tslint:disable */
import { DestinyDisplayPropertiesDefinition } from '../../destiny/definitions/common/destiny-display-properties-definition';
import { DestinyRace } from '../../destiny/destiny-race';

/**
 * In Destiny, "Races" are really more like "Species". Sort of. I mean, are the Awoken a separate species from humans? I'm not sure. But either way, they're defined here. You'll see Exo, Awoken, and Human as examples of these Species. Players will choose one for their character.
 */
export interface DestinyRaceDefinition {
  displayProperties?: DestinyDisplayPropertiesDefinition;

  /**
   * An enumeration defining the existing, known Races/Species for player characters. This value will be the enum value matching this definition.
   */
  raceType?: DestinyRace;

  /**
   * A localized string referring to the singular form of the Race's name when referred to in gendered form. Keyed by the DestinyGender.
   */
  genderedRaceNames?: {[key: string]: string};
  genderedRaceNamesByGenderHash?: {[key: string]: string};

  /**
   * The unique identifier for this entity. Guaranteed to be unique for the type of entity, but not globally.
   * When entities refer to each other in Destiny content, it is this hash that they are referring to.
   */
  hash?: number;

  /**
   * The index of the entity as it was found in the investment tables.
   */
  index?: number;

  /**
   * If this is true, then there is an entity with this identifier/type combination, but BNet is not yet allowed to show it. Sorry!
   */
  redacted?: boolean;
}
