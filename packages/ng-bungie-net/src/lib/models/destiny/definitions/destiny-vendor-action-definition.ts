/* tslint:disable */

/**
 * If a vendor can ever end up performing actions, these are the properties that will be related to those actions. I'm not going to bother documenting this yet, as it is unused and unclear if it will ever be used... but in case it is ever populated and someone finds it useful, it is defined here.
 */
export interface DestinyVendorActionDefinition {
  description?: string;
  executeSeconds?: number;
  icon?: string;
  name?: string;
  verb?: string;
  isPositive?: boolean;
  actionId?: string;
  actionHash?: number;
  autoPerformAction?: boolean;
}
