/* tslint:disable */
export interface DestinyParentItemOverride {
  additionalEquipRequirementsDisplayStrings?: Array<string>;
  pipIcon?: string;
}
