/* tslint:disable */
export interface DestinyObjectiveDisplayProperties {

  /**
   * The activity associated with this objective in the context of this item, if any.
   */
  activityHash?: number;

  /**
   * If true, the game shows this objective on item preview screens.
   */
  displayOnItemPreviewScreen?: boolean;
}
