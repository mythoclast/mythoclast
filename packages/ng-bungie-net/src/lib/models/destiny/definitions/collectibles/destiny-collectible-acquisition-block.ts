/* tslint:disable */
export interface DestinyCollectibleAcquisitionBlock {
  acquireMaterialRequirementHash?: number;
  acquireTimestampUnlockValueHash?: number;
}
