/* tslint:disable */
import { DestinyCollectibleAcquisitionBlock } from '../../../destiny/definitions/collectibles/destiny-collectible-acquisition-block';
import { DestinyDisplayPropertiesDefinition } from '../../../destiny/definitions/common/destiny-display-properties-definition';
import { DestinyScope } from '../../../destiny/destiny-scope';
import { DestinyCollectibleStateBlock } from '../../../destiny/definitions/collectibles/destiny-collectible-state-block';
import { DestinyPresentationChildBlock } from '../../../destiny/definitions/presentation/destiny-presentation-child-block';

/**
 * Defines a
 */
export interface DestinyCollectibleDefinition {
  acquisitionInfo?: DestinyCollectibleAcquisitionBlock;
  displayProperties?: DestinyDisplayPropertiesDefinition;

  /**
   * A human readable string for a hint about how to acquire the item.
   */
  sourceString?: string;

  /**
   * This is a hash identifier we are building on the BNet side in an attempt to let people group collectibles by similar sources.
   * I can't promise that it's going to be 100% accurate, but if the designers were consistent in assigning the same source strings to items with the same sources, it *ought to* be. No promises though.
   * This hash also doesn't relate to an actual definition, just to note: we've got nothing useful other than the source string for this data.
   */
  sourceHash?: number;
  itemHash?: number;

  /**
   * Indicates whether this Collectible's state is determined on a per-character or on an account-wide basis.
   */
  scope?: DestinyScope;
  stateInfo?: DestinyCollectibleStateBlock;
  presentationInfo?: DestinyPresentationChildBlock;

  /**
   * The unique identifier for this entity. Guaranteed to be unique for the type of entity, but not globally.
   * When entities refer to each other in Destiny content, it is this hash that they are referring to.
   */
  hash?: number;

  /**
   * The index of the entity as it was found in the investment tables.
   */
  index?: number;

  /**
   * If this is true, then there is an entity with this identifier/type combination, but BNet is not yet allowed to show it. Sorry!
   */
  redacted?: boolean;
}
