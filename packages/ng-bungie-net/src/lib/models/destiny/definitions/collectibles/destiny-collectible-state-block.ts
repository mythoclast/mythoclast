/* tslint:disable */
import { DestinyPresentationNodeRequirementsBlock } from '../../../destiny/definitions/presentation/destiny-presentation-node-requirements-block';
export interface DestinyCollectibleStateBlock {
  obscuredOverrideItemHash?: number;
  requirements?: DestinyPresentationNodeRequirementsBlock;
}
