/* tslint:disable */
import { DestinyItemSourceDefinition } from '../../destiny/definitions/sources/destiny-item-source-definition';
import { BungieMembershipType } from '../../bungie-membership-type';
import { DestinyItemVendorSourceReference } from '../../destiny/definitions/destiny-item-vendor-source-reference';

/**
 * Data about an item's "sources": ways that the item can be obtained.
 */
export interface DestinyItemSourceBlockDefinition {

  /**
   * The list of hash identifiers for Reward Sources that hint where the item can be found (DestinyRewardSourceDefinition).
   */
  sourceHashes?: Array<number>;

  /**
   * A collection of details about the stats that were computed for the ways we found that the item could be spawned.
   */
  sources?: Array<DestinyItemSourceDefinition>;

  /**
   * If we found that this item is exclusive to a specific platform, this will be set to the BungieMembershipType enumeration that matches that platform.
   */
  exclusive?: BungieMembershipType;

  /**
   * A denormalized reference back to vendors that potentially sell this item.
   */
  vendorSources?: Array<DestinyItemVendorSourceReference>;
}
