/* tslint:disable */

/**
 * Basic identifying data about the bubble. Combine with DestinyDestinationBubbleSettingDefinition - see DestinyDestinationDefinition.bubbleSettings for more information.
 */
export interface DestinyBubbleDefinition {

  /**
   * The identifier for the bubble: only guaranteed to be unique within the Destination.
   */
  hash?: number;

  /**
   * The display properties of this bubble, so you don't have to look them up in a separate list anymore.
   */
  displayProperties?: {};
}
