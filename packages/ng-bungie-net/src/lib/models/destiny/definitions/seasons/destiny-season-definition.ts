/* tslint:disable */
import { DestinyDisplayPropertiesDefinition } from '../../../destiny/definitions/common/destiny-display-properties-definition';

/**
 * Defines a canonical "Season" of Destiny: a range of a few months where the game highlights certain challenges, provides new loot, has new Clan-related rewards and celebrates various seasonal events.
 */
export interface DestinySeasonDefinition {
  displayProperties?: DestinyDisplayPropertiesDefinition;
  seasonNumber?: number;

  /**
   * The unique identifier for this entity. Guaranteed to be unique for the type of entity, but not globally.
   * When entities refer to each other in Destiny content, it is this hash that they are referring to.
   */
  hash?: number;

  /**
   * The index of the entity as it was found in the investment tables.
   */
  index?: number;

  /**
   * If this is true, then there is an entity with this identifier/type combination, but BNet is not yet allowed to show it. Sorry!
   */
  redacted?: boolean;
}
