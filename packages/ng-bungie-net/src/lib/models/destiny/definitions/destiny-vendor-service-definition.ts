/* tslint:disable */

/**
 * When a vendor provides services, this is the localized name of those services.
 */
export interface DestinyVendorServiceDefinition {

  /**
   * The localized name of a service provided.
   */
  name?: string;
}
