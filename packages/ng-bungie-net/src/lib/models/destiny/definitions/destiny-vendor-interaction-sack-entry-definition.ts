/* tslint:disable */

/**
 * Compare this sackType to the sack identifier in the DestinyInventoryItemDefinition.vendorSackType property of items. If they match, show this sack with this interaction.
 */
export interface DestinyVendorInteractionSackEntryDefinition {
  sackType?: number;
}
