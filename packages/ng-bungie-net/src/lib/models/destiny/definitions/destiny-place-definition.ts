/* tslint:disable */
import { DestinyDisplayPropertiesDefinition } from '../../destiny/definitions/common/destiny-display-properties-definition';

/**
 * Okay, so Activities (DestinyActivityDefinition) take place in Destinations (DestinyDestinationDefinition). Destinations are part of larger locations known as Places (you're reading its documentation right now).
 * Places are more on the planetary scale, like "Earth" and "Your Mom."
 */
export interface DestinyPlaceDefinition {
  displayProperties?: DestinyDisplayPropertiesDefinition;

  /**
   * The unique identifier for this entity. Guaranteed to be unique for the type of entity, but not globally.
   * When entities refer to each other in Destiny content, it is this hash that they are referring to.
   */
  hash?: number;

  /**
   * The index of the entity as it was found in the investment tables.
   */
  index?: number;

  /**
   * If this is true, then there is an entity with this identifier/type combination, but BNet is not yet allowed to show it. Sorry!
   */
  redacted?: boolean;
}
