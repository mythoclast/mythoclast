import { DestinyCharacterCustomizationCategoryOptionsDefinition } from './destiny-character-customization-category-options-definition';

export interface DestinyCharacterCustomizationOptionDefinition {
  displayProperties: DestinyCharacterCustomizationCategoryOptionsDefinition;
  genderHash: number;
  raceHash: number;
  decalColorOptions: DestinyCharacterCustomizationCategoryOptionsDefinition;
  decalOptions: DestinyCharacterCustomizationCategoryOptionsDefinition;
  eyeColorOptions: DestinyCharacterCustomizationCategoryOptionsDefinition;
  faceOptionCinematicHostPatternIds: number[];
  faceOptions: DestinyCharacterCustomizationCategoryOptionsDefinition;
  featureColorOptions: DestinyCharacterCustomizationCategoryOptionsDefinition;
  featureOptions: DestinyCharacterCustomizationCategoryOptionsDefinition;
  hairColorOptions: DestinyCharacterCustomizationCategoryOptionsDefinition;
  hairOptions: DestinyCharacterCustomizationCategoryOptionsDefinition;
  helmetPreferences: DestinyCharacterCustomizationCategoryOptionsDefinition;
  lipColorOptions: DestinyCharacterCustomizationCategoryOptionsDefinition;
  personalityOptions: DestinyCharacterCustomizationCategoryOptionsDefinition;
  skinColorOptions: DestinyCharacterCustomizationCategoryOptionsDefinition;
  hash: number;
  index: number;
  redacted: boolean;
  blacklisted: boolean;
}
