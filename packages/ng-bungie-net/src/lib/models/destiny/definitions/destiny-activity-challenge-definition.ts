/* tslint:disable */
import { DestinyItemQuantity } from '../../destiny/destiny-item-quantity';

/**
 * Represents a reference to a Challenge, which for now is just an Objective.
 */
export interface DestinyActivityChallengeDefinition {

  /**
   * The hash for the Objective that matches this challenge. Use it to look up the DestinyObjectiveDefinition.
   */
  objectiveHash?: number;

  /**
   * The rewards as they're represented in the UI. Note that they generally link to "dummy" items that give a summary of rewards rather than direct, real items themselves.
   * If the quantity is 0, don't show the quantity.
   */
  dummyRewards?: Array<DestinyItemQuantity>;
}
