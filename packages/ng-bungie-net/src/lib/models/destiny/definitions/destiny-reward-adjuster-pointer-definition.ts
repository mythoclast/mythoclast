export interface DestinyRewardAdjusterPointerDefinition {
  adjusterType: number;
  hash: number;
  index: number;
  redacted: boolean;
  blacklisted: boolean;
}
