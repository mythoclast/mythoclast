export interface DestinyArtFilterDefinition {
  arrangementIndexByStatValue: { number: number };
  artArrangementRegionHash: number;
  artArrangementRegionIndex: number;
  statHash: number;
}
