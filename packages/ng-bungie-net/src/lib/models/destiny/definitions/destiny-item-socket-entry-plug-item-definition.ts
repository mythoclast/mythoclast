/* tslint:disable */

/**
 * The definition of a known, reusable plug that can be applied to a socket.
 */
export interface DestinyItemSocketEntryPlugItemDefinition {

  /**
   * The hash identifier of a DestinyInventoryItemDefinition representing the plug that can be inserted.
   */
  plugItemHash?: number;
}
