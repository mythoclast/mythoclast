/* tslint:disable */

/**
 * BNet's custom categorization of reward sources. We took a look at the existing ways that items could be spawned, and tried to make high-level categorizations of them. This needs to be re-evaluated for Destiny 2.
 */

export enum DestinyRewardSourceCategory {
  None = 0,
  Activity = 1,
  Vendor = 2,
  Aggregate = 3,
}
