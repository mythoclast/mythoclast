export interface DestinyUnlockEntryDefinition {
  unlockHash: number;
  selectedValue: number;
  clearedValue: number;
  unlockValueHash: number;
}
