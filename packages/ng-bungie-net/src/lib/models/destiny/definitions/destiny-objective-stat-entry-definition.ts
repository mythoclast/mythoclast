/* tslint:disable */
import { DestinyObjectiveGrantStyle } from '../../destiny/destiny-objective-grant-style';

/**
 * Defines the conditions under which stat modifications will be applied to a Character while participating in an objective.
 */
export interface DestinyObjectiveStatEntryDefinition {

  /**
   * The stat being modified, and the value used.
   */
  stat?: {};

  /**
   * Whether it will be applied as long as the objective is active, when it's completed, or until it's completed.
   */
  style?: DestinyObjectiveGrantStyle;
}
