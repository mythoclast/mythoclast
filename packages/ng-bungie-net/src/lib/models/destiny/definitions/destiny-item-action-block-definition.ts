/* tslint:disable */
import { DestinyProgressionRewardDefinition } from '../../destiny/definitions/destiny-progression-reward-definition';
import { DestinyItemActionRequiredItemDefinition } from '../../destiny/definitions/destiny-item-action-required-item-definition';

/**
 * If an item can have an action performed on it (like "Dismantle"), it will be defined here if you care.
 */
export interface DestinyItemActionBlockDefinition {

  /**
   * If performing this action earns you Progression, this is the list of progressions and values granted for those progressions by performing this action.
   */
  progressionRewards?: Array<DestinyProgressionRewardDefinition>;

  /**
   * Localized text for the verb of the action being performed.
   */
  verbName?: string;

  /**
   * The content has this property, however it's not entirely clear how it is used.
   */
  isPositive?: boolean;

  /**
   * If the action has an overlay screen associated with it, this is the name of that screen. Unfortunately, we cannot return the screen's data itself.
   */
  overlayScreenName?: string;

  /**
   * The icon associated with the overlay screen for the action, if any.
   */
  overlayIcon?: string;

  /**
   * The number of seconds to delay before allowing this action to be performed again.
   */
  requiredCooldownSeconds?: number;

  /**
   * If the action requires other items to exist or be destroyed, this is the list of those items and requirements.
   */
  requiredItems?: Array<DestinyItemActionRequiredItemDefinition>;

  /**
   * Localized text describing the action being performed.
   */
  verbDescription?: string;

  /**
   * The internal identifier for the action.
   */
  actionTypeLabel?: string;

  /**
   * Theoretically, an item could have a localized string for a hint about the location in which the action should be performed. In practice, no items yet have this property.
   */
  requiredLocation?: string;

  /**
   * The identifier hash for the Cooldown associated with this action. We have not pulled this data yet for you to have more data to use for cooldowns.
   */
  requiredCooldownHash?: number;

  /**
   * If true, the item is deleted when the action completes.
   */
  deleteOnAction?: boolean;

  /**
   * If true, the entire stack is deleted when the action completes.
   */
  consumeEntireStack?: boolean;

  /**
   * If true, this action will be performed as soon as you earn this item. Some rewards work this way, providing you a single item to pick up from a reward-granting vendor in-game and then immediately consuming itself to provide you multiple items.
   */
  useOnAcquire?: boolean;
}
