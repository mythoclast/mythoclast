/* tslint:disable */
import { DestinyDisplayPropertiesDefinition } from '../../destiny/definitions/common/destiny-display-properties-definition';

/**
 * Human readable data about the bubble. Combine with DestinyBubbleDefinition - see DestinyDestinationDefinition.bubbleSettings for more information.
 * DEPRECATED - Just use bubbles.
 */
export interface DestinyDestinationBubbleSettingDefinition {
  displayProperties?: DestinyDisplayPropertiesDefinition;
}
