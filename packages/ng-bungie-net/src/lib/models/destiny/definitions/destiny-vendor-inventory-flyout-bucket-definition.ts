/* tslint:disable */
import { DestinyItemSortType } from '../../destiny/destiny-item-sort-type';

/**
 * Information about a single inventory bucket in a vendor flyout UI and how it is shown.
 */
export interface DestinyVendorInventoryFlyoutBucketDefinition {

  /**
   * If true, the inventory bucket should be able to be collapsed visually.
   */
  collapsible?: boolean;

  /**
   * The inventory bucket whose contents should be shown.
   */
  inventoryBucketHash?: number;

  /**
   * The methodology to use for sorting items from the flyout.
   */
  sortItemsBy?: DestinyItemSortType;
}
