export interface DestinyMedalTierDefinition {
  tierName: string;
  order: number;
  hash: number;
  index: number;
  redacted: boolean;
  blacklisted: boolean;
}
