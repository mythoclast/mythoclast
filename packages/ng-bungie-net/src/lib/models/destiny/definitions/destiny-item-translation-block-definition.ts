/* tslint:disable */
import { DyeReference } from '../../destiny/dye-reference';
import { DestinyGearArtArrangementReference } from '../../destiny/definitions/destiny-gear-art-arrangement-reference';

/**
 * This Block defines the rendering data associated with the item, if any.
 */
export interface DestinyItemTranslationBlockDefinition {
  weaponPatternIdentifier?: string;
  weaponPatternHash?: number;
  defaultDyes?: Array<DyeReference>;
  lockedDyes?: Array<DyeReference>;
  customDyes?: Array<DyeReference>;
  arrangements?: Array<DestinyGearArtArrangementReference>;
  hasGeometry?: boolean;
}
