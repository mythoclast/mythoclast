/* tslint:disable */
import { DestinyActivityLoadoutRequirement } from '../../destiny/definitions/destiny-activity-loadout-requirement';
export interface DestinyActivityLoadoutRequirementSet {

  /**
   * The set of requirements that will be applied on the activity if this requirement set is active.
   */
  requirements?: Array<DestinyActivityLoadoutRequirement>;
}
