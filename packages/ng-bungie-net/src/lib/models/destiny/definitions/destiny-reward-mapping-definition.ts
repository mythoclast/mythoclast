export interface DestinyRewardMappingDefinition {
  mappingHash: number;
  mappingIndex: number;
  isGlobal: boolean;
  hash: number;
  index: number;
  redacted: boolean;
  blacklisted: boolean;
}
