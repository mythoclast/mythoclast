export interface DestinyUnlockExpressionMappingDefinition {
  hash: number;
  index: number;
  redacted: boolean;
  blacklisted: boolean;
}
