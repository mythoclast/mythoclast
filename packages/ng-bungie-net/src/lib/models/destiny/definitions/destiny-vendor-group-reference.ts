/* tslint:disable */
export interface DestinyVendorGroupReference {

  /**
   * The DestinyVendorGroupDefinition to which this Vendor can belong.
   */
  vendorGroupHash?: number;
}
