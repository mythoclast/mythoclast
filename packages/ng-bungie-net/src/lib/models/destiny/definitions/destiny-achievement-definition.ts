import { DestinyDisplayPropertiesDefinition } from './common/destiny-display-properties-definition';

export interface DestinyAchievementDefinition {
  displayProperties: DestinyDisplayPropertiesDefinition;
  acccumulatorThreshold: number;
  platformIndex: number;
  hash: number;
  index: number;
  redacted: boolean;
  blacklisted: boolean;
}
