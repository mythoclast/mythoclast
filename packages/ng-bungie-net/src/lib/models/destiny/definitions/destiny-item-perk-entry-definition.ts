/* tslint:disable */
import { ItemPerkVisibility } from '../../destiny/item-perk-visibility';

/**
 * An intrinsic perk on an item, and the requirements for it to be activated.
 */
export interface DestinyItemPerkEntryDefinition {

  /**
   * If this perk is not active, this is the string to show for why it's not providing its benefits.
   */
  requirementDisplayString?: string;

  /**
   * A hash identifier for the DestinySandboxPerkDefinition being provided on the item.
   */
  perkHash?: number;

  /**
   * Indicates whether this perk should be shown, or if it should be shown disabled.
   */
  perkVisibility?: ItemPerkVisibility;
}
