import { DestinyUnlockEntryDefinition } from './destiny-unlock-entry-definition';

export interface DestinyUnlockEventDefinition {
  sequenceLastUpdatedUnlockValueHash: number;
  sequenceUnlockValueHash: number;
  newSequenceRewardSiteHash: number;
  unlockEntries: DestinyUnlockEntryDefinition[];
  hash: number;
  index: number;
  redacted: boolean;
  blacklisted: boolean;
}
