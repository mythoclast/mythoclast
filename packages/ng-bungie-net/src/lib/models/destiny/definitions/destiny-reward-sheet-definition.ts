export interface DestinyRewardSheetDefinition {
  sheetHash: number;
  sheetIndex: number;
  hash: number;
  index: number;
  redacted: boolean;
  blacklisted: boolean;
}
