/* tslint:disable */
import { SocketTypeActionType } from '../../../destiny/socket-type-action-type';

/**
 * Data related to what happens while a plug is being inserted, mostly for UI purposes.
 */
export interface DestinyInsertPlugActionDefinition {

  /**
   * How long it takes for the Plugging of the item to be completed once it is initiated, if you care.
   */
  actionExecuteSeconds?: number;

  /**
   * The type of action being performed when you act on this Socket Type. The most common value is "insert plug", but there are others as well (for instance, a "Masterwork" socket may allow for Re-initialization, and an Infusion socket allows for items to be consumed to upgrade the item)
   */
  actionType?: SocketTypeActionType;
}
