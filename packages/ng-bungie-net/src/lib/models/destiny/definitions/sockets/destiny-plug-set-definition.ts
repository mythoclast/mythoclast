/* tslint:disable */
import { DestinyItemSocketEntryPlugItemDefinition } from '../../../destiny/definitions/destiny-item-socket-entry-plug-item-definition';

/**
 * Sometimes, we have large sets of reusable plugs that are defined identically and thus can (and in some cases, are so large that they *must*) be shared across the places where they are used. These are the definitions for those reusable sets of plugs.
 * See DestinyItemSocketEntryDefinition.plugSource and reusablePlugSetHash for the relationship between these reusable plug sets and the sockets that leverage them (for starters, Emotes).
 */
export interface DestinyPlugSetDefinition {

  /**
   * If you want to show these plugs in isolation, these are the display properties for them.
   */
  displayProperties?: {};

  /**
   * This is a list of pre-determined plugs that can be plugged into this socket, without the character having the plug in their inventory.
   * If this list is populated, you will not be allowed to plug an arbitrary item in the socket: you will only be able to choose from one of these reusable plugs.
   */
  reusablePlugItems?: Array<DestinyItemSocketEntryPlugItemDefinition>;

  /**
   * The unique identifier for this entity. Guaranteed to be unique for the type of entity, but not globally.
   * When entities refer to each other in Destiny content, it is this hash that they are referring to.
   */
  hash?: number;

  /**
   * The index of the entity as it was found in the investment tables.
   */
  index?: number;

  /**
   * If this is true, then there is an entity with this identifier/type combination, but BNet is not yet allowed to show it. Sorry!
   */
  redacted?: boolean;
}
