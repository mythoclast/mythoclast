export interface DestinyUnlockCountMappingDefinition {
  unlockValueHash: number;
  hash: number;
  index: number;
  redacted: boolean;
  blacklisted: boolean;
}
