/* tslint:disable */
import { DestinyItemSetBlockEntryDefinition } from '../../destiny/definitions/destiny-item-set-block-entry-definition';

/**
 * Primarily for Quests, this is the definition of properties related to the item if it is a quest and its various quest steps.
 */
export interface DestinyItemSetBlockDefinition {

  /**
   * A collection of hashes of set items, for items such as Quest Metadata items that possess this data.
   */
  itemList?: Array<DestinyItemSetBlockEntryDefinition>;

  /**
   * If true, items in the set can only be added in increasing order, and adding an item will remove any previous item. For Quests, this is by necessity true. Only one quest step is present at a time, and previous steps are removed as you advance in the quest.
   */
  requireOrderedSetItemAdd?: boolean;

  /**
   * If true, the UI should treat this quest as "featured"
   */
  setIsFeatured?: boolean;

  /**
   * A string identifier we can use to attempt to identify the category of the Quest.
   */
  setType?: string;
}
