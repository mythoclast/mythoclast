import { DestinyDisplayPropertiesDefinition } from './common/destiny-display-properties-definition';

export interface DestinyCharacterCustomizationCategoryDefinition {
  displayProperties: DestinyDisplayPropertiesDefinition;
  hash: number;
  index: number;
  redacted: boolean;
  blacklisted: boolean;
}
