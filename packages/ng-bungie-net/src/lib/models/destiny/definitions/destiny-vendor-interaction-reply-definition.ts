/* tslint:disable */
import { DestinyVendorInteractionRewardSelection } from '../../destiny/destiny-vendor-interaction-reward-selection';
import { DestinyVendorReplyType } from '../../destiny/destiny-vendor-reply-type';

/**
 * When the interaction is replied to, Reward sites will fire and items potentially selected based on whether the given unlock expression is TRUE.
 * You can potentially choose one from multiple replies when replying to an interaction: this is how you get either/or rewards from vendors.
 */
export interface DestinyVendorInteractionReplyDefinition {

  /**
   * The rewards granted upon responding to the vendor.
   */
  itemRewardsSelection?: DestinyVendorInteractionRewardSelection;

  /**
   * The localized text for the reply.
   */
  reply?: string;

  /**
   * An enum indicating the type of reply being made.
   */
  replyType?: DestinyVendorReplyType;
}
