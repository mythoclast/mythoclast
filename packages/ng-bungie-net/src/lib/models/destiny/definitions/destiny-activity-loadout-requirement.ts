/* tslint:disable */
import { DestinyItemSubType } from '../../destiny/destiny-item-sub-type';
export interface DestinyActivityLoadoutRequirement {
  equipmentSlotHash?: number;
  allowedEquippedItemHashes?: Array<number>;
  allowedWeaponSubTypes?: Array<DestinyItemSubType>;
}
