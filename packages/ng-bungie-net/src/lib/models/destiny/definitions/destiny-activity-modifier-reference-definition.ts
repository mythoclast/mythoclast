/* tslint:disable */

/**
 * A reference to an Activity Modifier from another entity, such as an Activity (for now, just Activities).
 * This defines some
 */
export interface DestinyActivityModifierReferenceDefinition {

  /**
   * The hash identifier for the DestinyActivityModifierDefinition referenced by this activity.
   */
  activityModifierHash?: number;
}
