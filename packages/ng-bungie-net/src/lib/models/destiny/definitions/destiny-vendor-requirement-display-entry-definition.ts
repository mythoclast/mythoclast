/* tslint:disable */

/**
 * The localized properties of the requirementsDisplay, allowing information about the requirement or item being featured to be seen.
 */
export interface DestinyVendorRequirementDisplayEntryDefinition {
  icon?: string;
  name?: string;
  source?: string;
  type?: string;
}
