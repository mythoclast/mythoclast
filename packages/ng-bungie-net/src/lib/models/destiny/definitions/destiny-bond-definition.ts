import { DestinyDisplayPropertiesDefinition } from './common/destiny-display-properties-definition';

export interface DestinyBondDefinition {
  displayProperties: DestinyDisplayPropertiesDefinition;
  providedUnlockHash: number;
  providedUnlockValueHash: number;
  hash: number;
  index: number;
  redacted: boolean;
  blacklisted: boolean;
}
