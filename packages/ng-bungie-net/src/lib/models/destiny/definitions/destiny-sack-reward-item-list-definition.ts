export interface DestinySackRewardItemListDefinition {
  hash: number;
  index: number;
  redacted: boolean;
  blacklisted: boolean;
}
