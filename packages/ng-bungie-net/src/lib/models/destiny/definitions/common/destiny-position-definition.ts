/* tslint:disable */
export interface DestinyPositionDefinition {
  x?: number;
  y?: number;
  z?: number;
}
