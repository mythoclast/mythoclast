/* tslint:disable */
export interface DestinyItemSocketEntryPlugItemRandomizedDefinition {

  /**
   * The hash identifier of a DestinyInventoryItemDefinition representing the plug that can be inserted.
   */
  plugItemHash?: number;
}
