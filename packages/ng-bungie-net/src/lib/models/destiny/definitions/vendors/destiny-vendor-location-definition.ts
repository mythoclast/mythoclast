/* tslint:disable */

/**
 * These definitions represent vendors' locations and relevant display information at different times in the game.
 */
export interface DestinyVendorLocationDefinition {

  /**
   * The hash identifier for a Destination at which this vendor may be located. Each destination where a Vendor may exist will only ever have a single entry.
   */
  destinationHash?: number;

  /**
   * The relative path to the background image representing this Vendor at this location, for use in a banner.
   */
  backgroundImagePath?: string;
}
