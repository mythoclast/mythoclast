import { DestinyInteractibleActivityEntry } from './destiny-interactible-activity-entry';

export interface DestinyActivityInteractableDefinition {
  entries: DestinyInteractibleActivityEntry[];
  hash: number;
  index: number;
  redacted: boolean;
  blacklisted: boolean;
}
