/* tslint:disable */

/**
 * Not terribly useful, some basic cooldown interaction info.
 */
export interface DestinyVendorSaleItemActionBlockDefinition {
  executeSeconds?: number;
  isPositive?: boolean;
}
