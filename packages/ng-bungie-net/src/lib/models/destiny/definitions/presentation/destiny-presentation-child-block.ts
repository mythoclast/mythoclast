/* tslint:disable */
import { DestinyPresentationNodeType } from '../../../destiny/destiny-presentation-node-type';
import { DestinyPresentationDisplayStyle } from '../../../destiny/destiny-presentation-display-style';
export interface DestinyPresentationChildBlock {
  presentationNodeType?: DestinyPresentationNodeType;
  parentPresentationNodeHashes?: Array<number>;
  displayStyle?: DestinyPresentationDisplayStyle;
}
