/* tslint:disable */
import { DestinyPresentationNodeChildEntry } from '../../../destiny/definitions/presentation/destiny-presentation-node-child-entry';
import { DestinyPresentationNodeCollectibleChildEntry } from '../../../destiny/definitions/presentation/destiny-presentation-node-collectible-child-entry';
import { DestinyPresentationNodeRecordChildEntry } from '../../../destiny/definitions/presentation/destiny-presentation-node-record-child-entry';

/**
 * As/if presentation nodes begin to host more entities as children, these lists will be added to. One list property exists per type of entity that can be treated as a child of this presentation node, and each holds the identifier of the entity and any associated information needed to display the UI for that entity (if anything)
 */
export interface DestinyPresentationNodeChildrenBlock {
  presentationNodes?: Array<DestinyPresentationNodeChildEntry>;
  collectibles?: Array<DestinyPresentationNodeCollectibleChildEntry>;
  records?: Array<DestinyPresentationNodeRecordChildEntry>;
}
