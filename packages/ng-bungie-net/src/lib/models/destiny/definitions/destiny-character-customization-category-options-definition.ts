import { DestinyCharacterCustomizationCategoryOption } from './destiny-character-customization-category-option';

export interface DestinyCharacterCustomizationCategoryOptionsDefinition {
  customizationCategoryHash: number;
  displayName: string;
  options: DestinyCharacterCustomizationCategoryOption[];
}
