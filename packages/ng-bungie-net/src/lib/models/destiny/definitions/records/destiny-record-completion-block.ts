/* tslint:disable */
import { DestinyRecordToastStyle } from '../../../destiny/destiny-record-toast-style';
export interface DestinyRecordCompletionBlock {

  /**
   * The number of objectives that must be completed before the objective is considered "complete"
   */
  partialCompletionObjectiveCountThreshold?: number;
  ScoreValue?: number;
  shouldFireToast?: boolean;
  toastStyle?: DestinyRecordToastStyle;
}
