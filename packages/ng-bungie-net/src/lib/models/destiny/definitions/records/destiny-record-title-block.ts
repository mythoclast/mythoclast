/* tslint:disable */
export interface DestinyRecordTitleBlock {
  hasTitle?: boolean;
  titlesByGender?: {[key: string]: string};

  /**
   * For those who prefer to use the definitions.
   */
  titlesByGenderHash?: {[key: string]: string};
}
