/* tslint:disable */
import { DestinyRecordCompletionBlock } from '../../../destiny/definitions/records/destiny-record-completion-block';
import { DestinyDisplayPropertiesDefinition } from '../../../destiny/definitions/common/destiny-display-properties-definition';
import { DestinyPresentationChildBlock } from '../../../destiny/definitions/presentation/destiny-presentation-child-block';
import { DestinyRecordValueStyle } from '../../../destiny/destiny-record-value-style';
import { DestinyRecordTitleBlock } from '../../../destiny/definitions/records/destiny-record-title-block';
import { DestinyScope } from '../../../destiny/destiny-scope';
import { SchemaRecordStateBlock } from '../../../destiny/definitions/records/schema-record-state-block';
import { DestinyPresentationNodeRequirementsBlock } from '../../../destiny/definitions/presentation/destiny-presentation-node-requirements-block';
import { DestinyRecordExpirationBlock } from '../../../destiny/definitions/records/destiny-record-expiration-block';
import { DestinyItemQuantity } from '../../../destiny/destiny-item-quantity';
export interface DestinyRecordDefinition {
  completionInfo?: DestinyRecordCompletionBlock;
  displayProperties?: DestinyDisplayPropertiesDefinition;
  presentationInfo?: DestinyPresentationChildBlock;
  loreHash?: number;
  objectiveHashes?: Array<number>;
  recordValueStyle?: DestinyRecordValueStyle;
  titleInfo?: DestinyRecordTitleBlock;

  /**
   * Indicates whether this Record's state is determined on a per-character or on an account-wide basis.
   */
  scope?: DestinyScope;
  stateInfo?: SchemaRecordStateBlock;
  requirements?: DestinyPresentationNodeRequirementsBlock;
  expirationInfo?: DestinyRecordExpirationBlock;

  /**
   * If there is any publicly available information about rewards earned for achieving this record, this is the list of those items.
   *  However, note that some records intentionally have "hidden" rewards. These will not be returned in this list.
   */
  rewardItems?: Array<DestinyItemQuantity>;

  /**
   * The unique identifier for this entity. Guaranteed to be unique for the type of entity, but not globally.
   * When entities refer to each other in Destiny content, it is this hash that they are referring to.
   */
  hash?: number;

  /**
   * The index of the entity as it was found in the investment tables.
   */
  index?: number;

  /**
   * If this is true, then there is an entity with this identifier/type combination, but BNet is not yet allowed to show it. Sorry!
   */
  redacted?: boolean;
}
