/* tslint:disable */

/**
 * If this record has an expiration after which it cannot be earned, this is some information about that expiration.
 */
export interface DestinyRecordExpirationBlock {
  hasExpiration?: boolean;
  description?: string;
  icon?: string;
}
