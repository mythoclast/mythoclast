/* tslint:disable */
export interface SchemaRecordStateBlock {
  featuredPriority?: number;
  obscuredString?: string;
}
