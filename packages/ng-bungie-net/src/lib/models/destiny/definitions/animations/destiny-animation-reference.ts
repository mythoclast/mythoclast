/* tslint:disable */
export interface DestinyAnimationReference {
  animName?: string;
  animIdentifier?: string;
  path?: string;
}
