import { DestinyArtFilterDefinition } from './destiny-art-filter-definition';

export interface DestinySandboxPatternDefinition {
  patternHash: number;
  patternGlobalTagIdHash: number;
  weaponContentGroupHash: number;
  weaponTranslationGroupHash: number;
  weaponTypeHash: number;
  weaponType: number;
  filters: DestinyArtFilterDefinition[];
  hash: number;
  index: number;
  redacted: boolean;
  blacklisted: boolean;
}
