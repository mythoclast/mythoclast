export interface DestinyRewardAdjusterProgressionMapDefinition {
  isAdditive: boolean;
  hash: number;
  index: number;
  redacted: boolean;
  blacklisted: boolean;
}
