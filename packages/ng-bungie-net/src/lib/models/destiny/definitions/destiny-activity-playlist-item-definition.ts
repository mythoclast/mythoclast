/* tslint:disable */
import { DestinyActivityModeType } from '../../destiny/historical-stats/definitions/destiny-activity-mode-type';

/**
 * If the activity is a playlist, this is the definition for a specific entry in the playlist: a single possible combination of Activity and Activity Mode that can be chosen.
 */
export interface DestinyActivityPlaylistItemDefinition {

  /**
   * The hash identifier of the Activity that can be played. Use it to look up the DestinyActivityDefinition.
   */
  activityHash?: number;

  /**
   * If this playlist entry had an activity mode directly defined on it, this will be the hash of that mode.
   */
  directActivityModeHash?: number;

  /**
   * If the playlist entry had an activity mode directly defined on it, this will be the enum value of that mode.
   */
  directActivityModeType?: DestinyActivityModeType;

  /**
   * The hash identifiers for Activity Modes relevant to this entry.
   */
  activityModeHashes?: Array<number>;

  /**
   * The activity modes - if any - in enum form. Because we can't seem to escape the enums.
   */
  activityModeTypes?: Array<DestinyActivityModeType>;
}
