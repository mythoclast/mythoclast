import { DestinyDisplayPropertiesDefinition } from './common/destiny-display-properties-definition';

export interface DestinyCharacterCustomizationCategoryOption {
  displayProperties: DestinyDisplayPropertiesDefinition;
  value: number | number[];
}
