export interface DestinyArtDyeChannelDefinition {
  channelHash: number;
  index: number;
  hash: number;
  redacted: boolean;
  blacklisted: boolean;
}
