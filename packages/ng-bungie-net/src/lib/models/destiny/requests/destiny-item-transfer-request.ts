/* tslint:disable */
import { BungieMembershipType } from '../../bungie-membership-type';
export interface DestinyItemTransferRequest {
  itemReferenceHash?: number;
  stackSize?: number;
  transferToVault?: boolean;
  itemId?: number;
  characterId?: number;
  membershipType?: BungieMembershipType;
}
