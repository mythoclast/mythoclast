/* tslint:disable */
import { BungieMembershipType } from '../../../bungie-membership-type';
export interface DestinyPostmasterTransferRequest {
  itemReferenceHash?: number;
  stackSize?: number;
  itemId?: number;
  characterId?: number;
  membershipType?: BungieMembershipType;
}
