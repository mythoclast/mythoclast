/* tslint:disable */
import { BungieMembershipType } from '../../../bungie-membership-type';
export interface DestinyCharacterActionRequest {
  characterId?: number;
  membershipType?: BungieMembershipType;
}
