/* tslint:disable */
import { BungieMembershipType } from '../../../bungie-membership-type';
export interface DestinyInsertPlugsActionRequest {

  /**
   * Action token provided by the AwaGetActionToken API call.
   */
  actionToken?: string;

  /**
   * The instance ID of the item having a plug inserted. Only instanced items can have sockets.
   */
  itemInstanceId?: number;

  /**
   * The plugs being inserted.
   */
  plug?: {};
  characterId?: number;
  membershipType?: BungieMembershipType;
}
