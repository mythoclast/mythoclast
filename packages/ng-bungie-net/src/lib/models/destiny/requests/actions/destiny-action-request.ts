/* tslint:disable */
import { BungieMembershipType } from '../../../bungie-membership-type';
export interface DestinyActionRequest {
  membershipType?: BungieMembershipType;
}
