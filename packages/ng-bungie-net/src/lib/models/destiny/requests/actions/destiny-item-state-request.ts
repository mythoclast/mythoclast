/* tslint:disable */
import { BungieMembershipType } from '../../../bungie-membership-type';
export interface DestinyItemStateRequest {
  state?: boolean;
  itemId?: number;
  characterId?: number;
  membershipType?: BungieMembershipType;
}
