/* tslint:disable */
import { BungieMembershipType } from '../../../bungie-membership-type';
export interface DestinyItemSetActionRequest {
  itemIds?: Array<number>;
  characterId?: number;
  membershipType?: BungieMembershipType;
}
