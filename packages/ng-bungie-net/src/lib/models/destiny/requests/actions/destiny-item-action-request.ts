/* tslint:disable */
import { BungieMembershipType } from '../../../bungie-membership-type';
export interface DestinyItemActionRequest {
  itemId?: number;
  characterId?: number;
  membershipType?: BungieMembershipType;
}
