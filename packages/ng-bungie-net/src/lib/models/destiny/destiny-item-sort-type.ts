/* tslint:disable */

/**
 * Determines how items are sorted in an inventory bucket.
 */

export enum DestinyItemSortType {
  ItemId = 0,
  Timestamp = 1,
  StackSize = 2,
}
