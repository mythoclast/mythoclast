/* tslint:disable */

export enum DestinyGender {
  Male = 0,
  Female = 1,
  Unknown = 2,
}
