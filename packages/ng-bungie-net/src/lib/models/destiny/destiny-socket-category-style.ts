/* tslint:disable */

/**
 * Represents the possible and known UI styles used by the game for rendering Socket Categories.
 */

export enum DestinySocketCategoryStyle {
  Unknown = 0,
  Reusable = 1,
  Consumable = 2,
  Unlockable = 3,
  Intrinsic = 4,
}
