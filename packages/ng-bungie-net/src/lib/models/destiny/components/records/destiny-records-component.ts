/* tslint:disable */
import { DestinyRecordComponent } from '../../../destiny/components/records/destiny-record-component';
export interface DestinyRecordsComponent {
  records?: {[key: string]: DestinyRecordComponent};
}
