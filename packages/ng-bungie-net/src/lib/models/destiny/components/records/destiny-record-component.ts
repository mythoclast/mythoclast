/* tslint:disable */
import { DestinyRecordState } from '../../../destiny/destiny-record-state';
import { DestinyObjectiveProgress } from '../../../destiny/quests/destiny-objective-progress';
export interface DestinyRecordComponent {
  state?: DestinyRecordState;
  objectives?: Array<DestinyObjectiveProgress>;
}
