/* tslint:disable */
import { DestinyRecordComponent } from '../../../destiny/components/records/destiny-record-component';
export interface DestinyCharacterRecordsComponent {
  featuredRecordHashes?: Array<number>;
  records?: {[key: string]: DestinyRecordComponent};
}
