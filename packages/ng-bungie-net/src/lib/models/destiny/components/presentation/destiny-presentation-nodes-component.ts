/* tslint:disable */
import { DestinyPresentationNodeComponent } from '../../../destiny/components/presentation/destiny-presentation-node-component';
export interface DestinyPresentationNodesComponent {
  nodes?: {[key: string]: DestinyPresentationNodeComponent};
}
