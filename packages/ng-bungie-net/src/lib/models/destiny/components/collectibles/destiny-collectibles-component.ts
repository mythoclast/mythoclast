/* tslint:disable */
import { DestinyCollectibleComponent } from '../../../destiny/components/collectibles/destiny-collectible-component';
export interface DestinyCollectiblesComponent {
  collectibles?: {[key: string]: DestinyCollectibleComponent};
}
