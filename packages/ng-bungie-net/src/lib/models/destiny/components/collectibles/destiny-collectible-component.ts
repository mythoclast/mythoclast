/* tslint:disable */
import { DestinyCollectibleState } from '../../../destiny/destiny-collectible-state';
export interface DestinyCollectibleComponent {
  state?: DestinyCollectibleState;
}
