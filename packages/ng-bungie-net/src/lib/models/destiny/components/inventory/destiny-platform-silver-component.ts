/* tslint:disable */
import { DestinyItemComponent } from '../../../destiny/entities/items/destiny-item-component';
export interface DestinyPlatformSilverComponent {

  /**
   * If a Profile is played on multiple platforms, this is the silver they have for each platform, keyed by Membership Type.
   */
  platformSilver?: {[key: string]: DestinyItemComponent};
}
