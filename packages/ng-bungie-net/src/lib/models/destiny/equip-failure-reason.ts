/* tslint:disable */

/**
 * The reasons why an item cannot be equipped, if any. Many flags can be set, or "None" if
 */

export enum EquipFailureReason {
  None = 0,
  ItemUnequippable = 1,
  ItemUniqueEquipRestricted = 2,
  ItemFailedUnlockCheck = 4,
  ItemFailedLevelCheck = 8,
  ItemNotOnCharacter = 16,
}
