/* tslint:disable */

export enum DestinySocketVisibility {
  Visible = 0,
  Hidden = 1,
  HiddenWhenEmpty = 2,
  HiddenIfNoPlugsAvailable = 3,
}
