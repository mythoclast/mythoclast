/* tslint:disable */

/**
 * If progression is earned, this determines whether the progression shows visual effects on the character or its item - or neither.
 */

export enum DestinyProgressionStepDisplayEffect {
  None = 0,
  Character = 1,
  Item = 2,
}
