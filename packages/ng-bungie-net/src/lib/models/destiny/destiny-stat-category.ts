/* tslint:disable */

/**
 * At last, stats have categories. Use this for whatever purpose you might wish.
 */

export enum DestinyStatCategory {
  Gameplay = 0,
  Weapon = 1,
  Defense = 2,
  Primary = 3,
}
