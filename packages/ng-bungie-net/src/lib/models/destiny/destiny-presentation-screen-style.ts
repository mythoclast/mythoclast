/* tslint:disable */

/**
 * A hint for what screen should be shown when this presentation node is clicked into. How you use this is your UI is up to you.
 */

export enum DestinyPresentationScreenStyle {
  Default = 0,
  CategorySets = 1,
  Badge = 2,
}
