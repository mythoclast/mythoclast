/* tslint:disable */
import { DestinyEquipItemResult } from '../destiny/destiny-equip-item-result';

/**
 * The results of a bulk Equipping operation performed through the Destiny API.
 */
export interface DestinyEquipItemResults {
  equipResults?: Array<DestinyEquipItemResult>;
}
