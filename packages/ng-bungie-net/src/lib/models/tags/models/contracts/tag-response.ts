/* tslint:disable */
import { IgnoreResponse } from '../../../ignores/ignore-response';
export interface TagResponse {
  tagText?: string;
  ignoreStatus?: IgnoreResponse;
}
