/* tslint:disable */
import { DestinyItemPerksComponent } from './destiny/entities/items/destiny-item-perks-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface DictionaryComponentResponseOfint64AndDestinyItemPerksComponent {
  data?: {[key: string]: DestinyItemPerksComponent};
  privacy?: ComponentPrivacySetting;
}
