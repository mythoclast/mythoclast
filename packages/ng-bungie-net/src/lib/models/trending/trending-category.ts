/* tslint:disable */
import { SearchResultOfTrendingEntry } from '../search-result-of-trending-entry';
export interface TrendingCategory {
  categoryName?: string;
  entries?: SearchResultOfTrendingEntry;
  categoryId?: string;
}
