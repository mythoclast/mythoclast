/* tslint:disable */
export interface TrendingEntryCommunityCreation {
  media?: string;
  title?: string;
  author?: string;
  authorMembershipId?: number;
  postId?: number;
  body?: string;
  upvotes?: number;
}
