/* tslint:disable */
import { DestinyPublicActivityStatus } from '../destiny/activities/destiny-public-activity-status';
export interface TrendingEntryDestinyActivity {
  activityHash?: number;
  status?: DestinyPublicActivityStatus;
}
