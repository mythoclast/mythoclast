/* tslint:disable */
import { TrendingEntryType } from '../trending/trending-entry-type';
import { TrendingEntryNews } from '../trending/trending-entry-news';
import { TrendingEntrySupportArticle } from '../trending/trending-entry-support-article';
import { TrendingEntryDestinyItem } from '../trending/trending-entry-destiny-item';
import { TrendingEntryDestinyActivity } from '../trending/trending-entry-destiny-activity';
import { TrendingEntryDestinyRitual } from '../trending/trending-entry-destiny-ritual';
import { TrendingEntryCommunityCreation } from '../trending/trending-entry-community-creation';
import { TrendingEntryCommunityStream } from '../trending/trending-entry-community-stream';
export interface TrendingDetail {
  identifier?: string;
  entityType?: TrendingEntryType;
  news?: TrendingEntryNews;
  support?: TrendingEntrySupportArticle;
  destinyItem?: TrendingEntryDestinyItem;
  destinyActivity?: TrendingEntryDestinyActivity;
  destinyRitual?: TrendingEntryDestinyRitual;
  creation?: TrendingEntryCommunityCreation;
  stream?: TrendingEntryCommunityStream;
}
