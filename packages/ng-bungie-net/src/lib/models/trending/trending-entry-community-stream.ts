/* tslint:disable */
import { PartnershipType } from '../partnerships/partnership-type';
export interface TrendingEntryCommunityStream {
  image?: string;
  title?: string;
  partnershipIdentifier?: string;
  partnershipType?: PartnershipType;
}
