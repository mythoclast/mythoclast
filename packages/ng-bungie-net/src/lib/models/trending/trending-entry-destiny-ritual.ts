/* tslint:disable */
export interface TrendingEntryDestinyRitual {
  image?: string;
  icon?: string;
  title?: string;
  subtitle?: string;
  dateStart?: string;
  dateEnd?: string;

  /**
   * A destiny event does not necessarily have a related Milestone, but if it does the details will be returned here.
   */
  milestoneDetails?: {};

  /**
   * A destiny event will not necessarily have milestone "custom content", but if it does the details will be here.
   */
  eventContent?: {};
}
