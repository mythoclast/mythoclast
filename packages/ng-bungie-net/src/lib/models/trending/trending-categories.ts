/* tslint:disable */
import { TrendingCategory } from '../trending/trending-category';
export interface TrendingCategories {
  categories?: Array<TrendingCategory>;
}
