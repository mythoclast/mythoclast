/* tslint:disable */
import { DestinyItemInstanceComponent } from './destiny/entities/items/destiny-item-instance-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface DictionaryComponentResponseOfint64AndDestinyItemInstanceComponent {
  data?: {[key: string]: DestinyItemInstanceComponent};
  privacy?: ComponentPrivacySetting;
}
