/* tslint:disable */
import { DictionaryComponentResponseOfint64AndDestinyItemInstanceComponent } from './dictionary-component-response-ofint-64and-destiny-item-instance-component';
import { DictionaryComponentResponseOfint64AndDestinyItemPerksComponent } from './dictionary-component-response-ofint-64and-destiny-item-perks-component';
import { DictionaryComponentResponseOfint64AndDestinyItemRenderComponent } from './dictionary-component-response-ofint-64and-destiny-item-render-component';
import { DictionaryComponentResponseOfint64AndDestinyItemStatsComponent } from './dictionary-component-response-ofint-64and-destiny-item-stats-component';
import { DictionaryComponentResponseOfint64AndDestinyItemSocketsComponent } from './dictionary-component-response-ofint-64and-destiny-item-sockets-component';
import { DictionaryComponentResponseOfint64AndDestinyItemTalentGridComponent } from './dictionary-component-response-ofint-64and-destiny-item-talent-grid-component';
import { DictionaryComponentResponseOfuint32AndDestinyItemPlugComponent } from './dictionary-component-response-ofuint-32and-destiny-item-plug-component';
import { DictionaryComponentResponseOfint64AndDestinyItemObjectivesComponent } from './dictionary-component-response-ofint-64and-destiny-item-objectives-component';
export interface DestinyItemComponentSetOfint64 {
  instances?: DictionaryComponentResponseOfint64AndDestinyItemInstanceComponent;
  perks?: DictionaryComponentResponseOfint64AndDestinyItemPerksComponent;
  renderData?: DictionaryComponentResponseOfint64AndDestinyItemRenderComponent;
  stats?: DictionaryComponentResponseOfint64AndDestinyItemStatsComponent;
  sockets?: DictionaryComponentResponseOfint64AndDestinyItemSocketsComponent;
  talentGrids?: DictionaryComponentResponseOfint64AndDestinyItemTalentGridComponent;
  plugStates?: DictionaryComponentResponseOfuint32AndDestinyItemPlugComponent;
  objectives?: DictionaryComponentResponseOfint64AndDestinyItemObjectivesComponent;
}
