/* tslint:disable */
import { DestinyCurrenciesComponent } from './destiny/components/inventory/destiny-currencies-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface SingleComponentResponseOfDestinyCurrenciesComponent {
  data?: DestinyCurrenciesComponent;
  privacy?: ComponentPrivacySetting;
}
