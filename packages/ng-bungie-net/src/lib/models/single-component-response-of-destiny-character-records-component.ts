/* tslint:disable */
import { DestinyCharacterRecordsComponent } from './destiny/components/records/destiny-character-records-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface SingleComponentResponseOfDestinyCharacterRecordsComponent {
  data?: DestinyCharacterRecordsComponent;
  privacy?: ComponentPrivacySetting;
}
