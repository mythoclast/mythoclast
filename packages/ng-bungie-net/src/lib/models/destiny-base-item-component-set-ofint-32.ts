/* tslint:disable */
import { DictionaryComponentResponseOfint32AndDestinyItemObjectivesComponent } from './dictionary-component-response-ofint-32and-destiny-item-objectives-component';
export interface DestinyBaseItemComponentSetOfint32 {
  objectives?: DictionaryComponentResponseOfint32AndDestinyItemObjectivesComponent;
}
