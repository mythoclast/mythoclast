/* tslint:disable */
import { DestinyItemRenderComponent } from './destiny/entities/items/destiny-item-render-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface DictionaryComponentResponseOfuint32AndDestinyItemRenderComponent {
  data?: {[key: string]: DestinyItemRenderComponent};
  privacy?: ComponentPrivacySetting;
}
