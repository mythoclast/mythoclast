/* tslint:disable */
import { DestinyItemTalentGridComponent } from './destiny/entities/items/destiny-item-talent-grid-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface DictionaryComponentResponseOfint64AndDestinyItemTalentGridComponent {
  data?: {[key: string]: DestinyItemTalentGridComponent};
  privacy?: ComponentPrivacySetting;
}
