/* tslint:disable */
export interface DateRange {
  start?: string;
  end?: string;
}
