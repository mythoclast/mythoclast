/* tslint:disable */
import { PollResult } from '../forum/poll-result';
export interface PollResponse {
  topicId?: number;
  results?: Array<PollResult>;
  totalVotes?: number;
}
