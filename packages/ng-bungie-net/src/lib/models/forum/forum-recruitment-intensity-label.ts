/* tslint:disable */

export enum ForumRecruitmentIntensityLabel {
  None = 0,
  Casual = 1,
  Professional = 2,
}
