/* tslint:disable */

export enum ForumRecruitmentToneLabel {
  None = 0,
  FamilyFriendly = 1,
  Rowdy = 2,
}
