/* tslint:disable */
import { ForumRecruitmentIntensityLabel } from '../forum/forum-recruitment-intensity-label';
import { ForumRecruitmentToneLabel } from '../forum/forum-recruitment-tone-label';
import { GeneralUser } from '../user/general-user';
export interface ForumRecruitmentDetail {
  topicId?: number;
  microphoneRequired?: boolean;
  intensity?: ForumRecruitmentIntensityLabel;
  tone?: ForumRecruitmentToneLabel;
  approved?: boolean;
  conversationId?: number;
  playerSlotsTotal?: number;
  playerSlotsRemaining?: number;
  Fireteam?: Array<GeneralUser>;
  kickedPlayerIds?: Array<number>;
}
