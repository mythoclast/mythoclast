/* tslint:disable */
import { ForumMediaType } from '../forum/forum-media-type';
import { ForumPostPopularity } from '../forum/forum-post-popularity';
import { IgnoreResponse } from '../ignores/ignore-response';
export interface PostResponse {
  userRating?: number;
  lastReplyTimestamp?: string;
  urlMediaType?: ForumMediaType;
  thumbnail?: string;
  popularity?: ForumPostPopularity;
  isActive?: boolean;
  isAnnouncement?: boolean;
  IsPinned?: boolean;
  userHasRated?: boolean;
  userHasMutedPost?: boolean;
  latestReplyPostId?: number;
  latestReplyAuthorId?: number;
  ignoreStatus?: IgnoreResponse;
  locale?: string;
}
