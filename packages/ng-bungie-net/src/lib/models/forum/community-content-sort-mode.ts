/* tslint:disable */

export enum CommunityContentSortMode {
  Trending = 0,
  Latest = 1,
  HighestRated = 2,
}
