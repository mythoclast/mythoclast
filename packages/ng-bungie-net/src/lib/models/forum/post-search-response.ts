/* tslint:disable */
import { PostResponse } from '../forum/post-response';
import { GroupResponse } from '../groups-v2/group-response';
import { TagResponse } from '../tags/models/contracts/tag-response';
import { PollResponse } from '../forum/poll-response';
import { ForumRecruitmentDetail } from '../forum/forum-recruitment-detail';
import { GeneralUser } from '../user/general-user';
import { PagedQuery } from '../queries/paged-query';
export interface PostSearchResponse {
  availablePages?: number;
  relatedPosts?: Array<PostResponse>;
  groups?: Array<GroupResponse>;
  searchedTags?: Array<TagResponse>;
  polls?: Array<PollResponse>;
  recruitmentDetails?: Array<ForumRecruitmentDetail>;
  authors?: Array<GeneralUser>;
  results?: Array<PostResponse>;
  totalResults?: number;
  hasMore?: boolean;
  query?: PagedQuery;
  replacementContinuationToken?: string;

  /**
   * If useTotalResults is true, then totalResults represents an accurate count.
   * If False, it does not, and may be estimated/only the size of the current page.
   * Either way, you should probably always only trust hasMore.
   * This is a long-held historical throwback to when we used to do paging with known total results. Those queries toasted our database, and we were left to hastily alter our endpoints and create backward- compatible shims, of which useTotalResults is one.
   */
  useTotalResults?: boolean;
}
