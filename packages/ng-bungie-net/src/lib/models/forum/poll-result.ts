/* tslint:disable */
export interface PollResult {
  answerText?: string;
  answerSlot?: number;
  lastVoteDate?: string;
  votes?: number;
  requestingUserVoted?: boolean;
}
