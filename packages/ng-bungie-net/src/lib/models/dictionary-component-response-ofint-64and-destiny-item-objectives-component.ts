/* tslint:disable */
import { DestinyItemObjectivesComponent } from './destiny/entities/items/destiny-item-objectives-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface DictionaryComponentResponseOfint64AndDestinyItemObjectivesComponent {
  data?: {[key: string]: DestinyItemObjectivesComponent};
  privacy?: ComponentPrivacySetting;
}
