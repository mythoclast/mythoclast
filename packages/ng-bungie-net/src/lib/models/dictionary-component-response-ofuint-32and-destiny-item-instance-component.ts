/* tslint:disable */
import { DestinyItemInstanceComponent } from './destiny/entities/items/destiny-item-instance-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface DictionaryComponentResponseOfuint32AndDestinyItemInstanceComponent {
  data?: {[key: string]: DestinyItemInstanceComponent};
  privacy?: ComponentPrivacySetting;
}
