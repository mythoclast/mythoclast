/* tslint:disable */
import { DestinyItemObjectivesComponent } from './destiny/entities/items/destiny-item-objectives-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface DictionaryComponentResponseOfuint32AndDestinyItemObjectivesComponent {
  data?: {[key: string]: DestinyItemObjectivesComponent};
  privacy?: ComponentPrivacySetting;
}
