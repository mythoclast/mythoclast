/* tslint:disable */
import { DestinyPlugSetsComponent } from './destiny/components/plug-sets/destiny-plug-sets-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface SingleComponentResponseOfDestinyPlugSetsComponent {
  data?: DestinyPlugSetsComponent;
  privacy?: ComponentPrivacySetting;
}
