/* tslint:disable */
export interface HyperlinkReference {
  title?: string;
  url?: string;
}
