/* tslint:disable */
export interface InterpolationPointFloat {
  value?: number;
  weight?: number;
}
