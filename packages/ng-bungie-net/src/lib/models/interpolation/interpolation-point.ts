/* tslint:disable */
export interface InterpolationPoint {
  value?: number;
  weight?: number;
}
