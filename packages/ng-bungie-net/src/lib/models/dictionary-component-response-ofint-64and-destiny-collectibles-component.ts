/* tslint:disable */
import { DestinyCollectiblesComponent } from './destiny/components/collectibles/destiny-collectibles-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface DictionaryComponentResponseOfint64AndDestinyCollectiblesComponent {
  data?: {[key: string]: DestinyCollectiblesComponent};
  privacy?: ComponentPrivacySetting;
}
