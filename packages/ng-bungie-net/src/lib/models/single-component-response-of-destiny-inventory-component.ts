/* tslint:disable */
import { DestinyInventoryComponent } from './destiny/entities/inventory/destiny-inventory-component';
import { ComponentPrivacySetting } from './components/component-privacy-setting';
export interface SingleComponentResponseOfDestinyInventoryComponent {
  data?: DestinyInventoryComponent;
  privacy?: ComponentPrivacySetting;
}
