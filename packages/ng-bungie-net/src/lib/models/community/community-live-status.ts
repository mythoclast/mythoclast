/* tslint:disable */
import { PartnershipType } from '../partnerships/partnership-type';
import { UserInfoCard } from '../user/user-info-card';
export interface CommunityLiveStatus {
  dateStreamStarted?: string;
  dateStatusUpdated?: string;
  partnershipIdentifier?: string;
  partnershipType?: PartnershipType;
  thumbnail?: string;
  thumbnailSmall?: string;
  thumbnailLarge?: string;
  destinyCharacterId?: number;
  userInfo?: UserInfoCard;
  currentActivityHash?: number;
  dateLastPlayed?: string;
  url?: string;
  locale?: string;
  currentViewers?: number;
  followers?: number;
  overallViewers?: number;
  isFeatured?: boolean;
  title?: string;
  activityModeHash?: number;
  dateFeatured?: string;
  trendingValue?: number;
  isSubscribable?: boolean;
}
