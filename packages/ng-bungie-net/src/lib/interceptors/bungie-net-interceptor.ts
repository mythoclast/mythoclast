import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable, of, forkJoin, race, interval } from 'rxjs';
import { tap, switchMap, filter, delay, take } from 'rxjs/operators';
import { BungieNetAuthenticatorService } from '../services';
import { BungieNetConfiguration } from '../bungie-net-configuration';


@Injectable()
export class BungieNetInterceptor implements HttpInterceptor {
  constructor(
    private authService: BungieNetAuthenticatorService,
    private config: BungieNetConfiguration
  ) { }
  intercept(request: HttpRequest<any>, nextHandler: HttpHandler): Observable<HttpEvent<any>> {
    let tokenObservable: Observable<string>;
    if (request.url.startsWith('https://www.bungie.net/platform/app/oauth/token/')) {
      tokenObservable = of(null);
    } else {
      tokenObservable = this.authService.getActiveToken().pipe(
        switchMap(it => !!it ? of(it) : interval(1000).pipe(switchMap(() => this.authService.getActiveToken()))),
        switchMap(it => !!it ? of(it) : interval(1000).pipe(switchMap(() => this.authService.getActiveToken()))),
        switchMap(it => !!it ? of(it) : interval(1000).pipe(switchMap(() => this.authService.getActiveToken()))),
        switchMap(it => !!it ? of(it) : interval(1000).pipe(switchMap(() => this.authService.getActiveToken()))),
        switchMap(it => !!it ? of(it) : interval(1000).pipe(switchMap(() => this.authService.getActiveToken())))
      );
    }

    return forkJoin({
      req: of(request),
      next: of(nextHandler),
      token: tokenObservable,
      key: this.config.apiKey
    }).pipe(
      switchMap(({ req, next, token, key }) => {
        if (req.url.startsWith('https://www.bungie.net/') && !req.url.startsWith('https://www.bungie.net/common/destiny2_content/json')) {
          const headers: { [index: string]: string } = {};
          headers['X-API-Key'] = key;
          if (!!token && req.url !== 'https://www.bungie.net/platform/app/oauth/token/') {
            headers.Authorization = `Bearer ${token}`;
          }
          req = req.clone({
            withCredentials: true,
            setHeaders: headers
          });
        }

        return next.handle(req).pipe(
          tap(x => x, err => {
            console.error(err);
          })
        );
      })
    );

  }
}
