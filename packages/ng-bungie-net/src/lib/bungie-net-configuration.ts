/* tslint:disable */
import { Injectable } from '@angular/core';
import { ReplaySubject, Observable } from 'rxjs';
import { take } from 'rxjs/operators';

/**
 * A container for Bungie.net API keys
 */
export class BungieKeys {
  /**
   * Your API key
   */
  apiKey: string;

  /**
   * Your OAuth client_id
   */
  oauthClientId: string;

  /**
   * Your OAuth client_secret
   */
  oauthClientSecret: string;
}

/**
 * Global configuration for BungieNet services
 */
@Injectable({
  providedIn: 'root',
})
export class BungieNetConfiguration {
  private apiKeySubject: ReplaySubject<string>;
  private oauthClientIdSubject: ReplaySubject<string>;
  private oauthClientSecretSubject: ReplaySubject<string>;

  constructor(keys: BungieKeys = null) {
    this.apiKeySubject = new ReplaySubject<string>(1);
    this.oauthClientIdSubject = new ReplaySubject<string>(1);
    this.oauthClientSecretSubject = new ReplaySubject<string>(1);
    if (keys) {
      this.acceptKeys(keys);
    }
  }

  /**
   * An Observable of the API key. New subscribers always get the current value.
   */
  get apiKey(): Observable<string> {
    return this.apiKeySubject.pipe(take(1));
  }

  /**
   * An Observable of the OAuth client_id. New subscribers always get the current value.
   */
  get oauthClientId(): Observable<string> {
    return this.oauthClientIdSubject.pipe(take(1));
  }

  /**
   * An Observable of the OAuth client_secret. New subscribers always get the current value.
   */
  get oauthClientSecret(): Observable<string> {
    return this.oauthClientSecretSubject.pipe(take(1));
  }

  /**
   * Calling this function registers the provided keys with the service.
   * This is required for API calls to work.
   * Otherwise the above Observables never emit and no request can ever begin.
   * @param keys A BungieKeys object containing the keys.
   */
  private acceptKeys(keys: BungieKeys): BungieNetConfiguration {
    this.apiKeySubject.next(keys.apiKey);
    this.oauthClientIdSubject.next(keys.oauthClientId);
    this.oauthClientSecretSubject.next(keys.oauthClientSecret);
    return this;
  }

  rootUrl: string = 'https://www.bungie.net/Platform';
}

export interface BungieNetConfigurationInterface {
  apiKey: Observable<string>;
  oauthClientId: Observable<string>;
  oauthClientSecret: Observable<string>;
  rootUrl?: string;
}


