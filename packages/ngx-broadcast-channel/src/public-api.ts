/*
 * Public API Surface of ngx-broadcast-channel
 */

export * from './lib/ngx-broadcast-channel.service';
export * from './lib/ngx-broadcast-channel.module';
