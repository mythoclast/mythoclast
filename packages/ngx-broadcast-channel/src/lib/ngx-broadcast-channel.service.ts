import { Injectable } from '@angular/core';
import BroadcastChannel from 'broadcast-channel';
import { Observable, Subscriber, TeardownLogic, Subscription, PartialObserver, from } from 'rxjs';

interface ChannelInfo<T> {
  channel: BroadcastChannel<T>;
  observable: Observable<T>;
}

@Injectable({
  providedIn: 'root'
})
export class NgxBroadcastChannelService {

  private getChannelInfo<T>(name: string): ChannelInfo<T> {
    const channel = new BroadcastChannel<T>(name);
    const channelObserver = function(this: Observable<T>, subscriber: Subscriber<T>): TeardownLogic {
      channel.onmessage = message => subscriber.next(message);
      return () => from(channel.close());
    };
    const observableChannel = new Observable<T>(channelObserver);
    const info = {
      channel,
      observable: observableChannel
    };

    return info;
  }

  getObservableChannel<T>(name: string): Observable<T> {
    return this.getChannelInfo<T>(name).observable;
  }

  sendTo<T>(name: string, value: T): Observable<void> {
    return from(this.getChannelInfo<T>(name).channel.postMessage(value));
  }

  subscribeTo<T>(name: string, partialObserver: PartialObserver<T>): Subscription {
    return this.getChannelInfo<T>(name).observable.subscribe(partialObserver);
  }
}
