import { NgModule } from '@angular/core';
import { NgxBroadcastChannelService } from './ngx-broadcast-channel.service';

@NgModule({
  declarations: [],
  imports: [],
  providers: [NgxBroadcastChannelService],
  exports: []
})
export class NgxBroadcastChannelModule { }
